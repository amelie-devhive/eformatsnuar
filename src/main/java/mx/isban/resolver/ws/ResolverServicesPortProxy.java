package mx.isban.resolver.ws;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;
import javax.xml.ws.Action;

import mx.isban.api.eformats.service.FormatBusiness;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ResolverServicesPortProxy{

    protected Descriptor _descriptor;

    private static final Logger LOGGER = LogManager.getLogger(ResolverServicesPortProxy.class);
    public class Descriptor {
    	
    	
        private mx.isban.resolver.ws.ResolverServices _service = null;
        private mx.isban.resolver.ws.ResolverConsultaServiceWS _proxy = null;
        private Dispatch<Source> _dispatch = null;
        private boolean _useJNDIOnly = false;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new mx.isban.resolver.ws.ResolverServices(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            try
            {
                InitialContext ctx = new InitialContext();
                _service = (mx.isban.resolver.ws.ResolverServices)ctx.lookup("java:comp/env/service/ResolverServices");
            }
            catch (NamingException e)
            {
                if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
                	LOGGER.error("JNDI lookup failure: javax.naming.NamingException: " + e.getMessage(),e);
                }
            }

            if (_service == null && !_useJNDIOnly)
                _service = new mx.isban.resolver.ws.ResolverServices();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getResolverServicesPort();
        }

        public mx.isban.resolver.ws.ResolverConsultaServiceWS getProxy() {
            return _proxy;
        }

        public void useJNDIOnly(boolean useJNDIOnly) {
            _useJNDIOnly = useJNDIOnly;
            init();
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("", "ResolverServicesPort");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

        public void setMTOMEnabled(boolean enable) {
            SOAPBinding binding = (SOAPBinding) ((BindingProvider) _proxy).getBinding();
            binding.setMTOMEnabled(enable);
        }
    }

    public ResolverServicesPortProxy() {
        _descriptor = new Descriptor();
        _descriptor.setMTOMEnabled(false);
    }

    public ResolverServicesPortProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
        _descriptor.setMTOMEnabled(false);
    }

    public Descriptor getDescriptor() {
        return _descriptor;
    }

    public ResponseConsultaUsrLDAP getLocalUserFromCorpID(RequestCorporativo reqUsrLDAP) {
        return getDescriptor().getProxy().getLocalUserFromCorpID(reqUsrLDAP);
    }

    public ResponseConsultaUsrLDAP getCorporateUserFromLocalID(RequestLocal arg0) {
        return getDescriptor().getProxy().getCorporateUserFromLocalID(arg0);
    }

    public ResponseConsultaUsrLDAP getPositionFromCorpID(RequestCorporativo arg0) {
        return getDescriptor().getProxy().getPositionFromCorpID(arg0);
    }

    public ResponseConsultaUsrLDAP getEmailFromCorpID(RequestCorporativo arg0) {
        return getDescriptor().getProxy().getEmailFromCorpID(arg0);
    }

    public ResponseConsultaUsrLDAP getLocationFromCorpID(RequestCorporativo arg0) {
        return getDescriptor().getProxy().getLocationFromCorpID(arg0);
    }

    public ResponseConsultaUsrLDAP getCommonNameFromCorpID(RequestCorporativo arg0) {
        return getDescriptor().getProxy().getCommonNameFromCorpID(arg0);
    }

}