
package mx.isban.resolver.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para requestCorporativo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="requestCorporativo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.resolver.isban.mx/}requestLDAP">
 *       &lt;sequence>
 *         &lt;element name="identificadorCorporativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestCorporativo", propOrder = {
    "identificadorCorporativo"
})
public class RequestCorporativo
    extends RequestLDAP
{

    protected String identificadorCorporativo;

    /**
     * Obtiene el valor de la propiedad identificadorCorporativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificadorCorporativo() {
        return identificadorCorporativo;
    }

    /**
     * Define el valor de la propiedad identificadorCorporativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificadorCorporativo(String value) {
        this.identificadorCorporativo = value;
    }

}
