
package mx.isban.resolver.ws;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "ResolverServices", targetNamespace = "http://ws.resolver.isban.mx/", wsdlLocation = "WEB-INF/wsdlDev/ResolverServices.wsdl")
public class ResolverServices
    extends Service
{

    private final static URL RESOLVERSERVICES_WSDL_LOCATION;
    private final static WebServiceException RESOLVERSERVICES_EXCEPTION;
    private final static QName RESOLVERSERVICES_QNAME = new QName("http://ws.resolver.isban.mx/", "ResolverServices");

    static {
            RESOLVERSERVICES_WSDL_LOCATION = mx.isban.resolver.ws.ResolverServices.class.getResource("/WEB-INF/wsdlDev/ResolverServices.wsdl");
        WebServiceException e = null;
        if (RESOLVERSERVICES_WSDL_LOCATION == null) {
            e = new WebServiceException("Cannot find 'WEB-INF/wsdlDev/ResolverServices.wsdl' wsdl. Place the resource correctly in the classpath.");
        }
        RESOLVERSERVICES_EXCEPTION = e;
    }

    public ResolverServices() {
        super(getWsdlLocation(), RESOLVERSERVICES_QNAME);
    }

    public ResolverServices(WebServiceFeature... features) {
        super(getWsdlLocation(), RESOLVERSERVICES_QNAME, features);
    }

    public ResolverServices(URL wsdlLocation) {
        super(wsdlLocation, RESOLVERSERVICES_QNAME);
    }

    public ResolverServices(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, RESOLVERSERVICES_QNAME, features);
    }

    public ResolverServices(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ResolverServices(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns ResolverConsultaServiceWS
     */
    @WebEndpoint(name = "ResolverServicesPort")
    public ResolverConsultaServiceWS getResolverServicesPort() {
        return super.getPort(new QName("http://ws.resolver.isban.mx/", "ResolverServicesPort"), ResolverConsultaServiceWS.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ResolverConsultaServiceWS
     */
    @WebEndpoint(name = "ResolverServicesPort")
    public ResolverConsultaServiceWS getResolverServicesPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://ws.resolver.isban.mx/", "ResolverServicesPort"), ResolverConsultaServiceWS.class, features);
    }

    private static URL getWsdlLocation() {
        if (RESOLVERSERVICES_EXCEPTION!= null) {
            throw RESOLVERSERVICES_EXCEPTION;
        }
        return RESOLVERSERVICES_WSDL_LOCATION;
    }

}
