
package mx.isban.resolver.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.isban.resolver.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactoryParent extends ObjectFactoryGrandParent {

    private final static QName _GetLocalUserFromCorpIDResponse_QNAME = new QName("http://ws.resolver.isban.mx/", "getLocalUserFromCorpIDResponse");
    private final static QName _GetLocationFromCorpIDResponse_QNAME = new QName("http://ws.resolver.isban.mx/", "getLocationFromCorpIDResponse");

    /**
     * Create an instance of {@link GetLocalUserFromCorpID }
     * 
     */
    public GetLocalUserFromCorpID createGetLocalUserFromCorpID() {
        return new GetLocalUserFromCorpID();
    }

    /**
     * Create an instance of {@link GetCorporateUserFromLocalID }
     * 
     */
    public GetCorporateUserFromLocalID createGetCorporateUserFromLocalID() {
        return new GetCorporateUserFromLocalID();
    }

    /**
     * Create an instance of {@link GetPositionFromCorpID }
     * 
     */
    public GetPositionFromCorpID createGetPositionFromCorpID() {
        return new GetPositionFromCorpID();
    }

    /**
     * Create an instance of {@link GetLocationFromCorpIDResponse }
     * 
     */
    public GetLocationFromCorpIDResponse createGetLocationFromCorpIDResponse() {
        return new GetLocationFromCorpIDResponse();
    }

    /**
     * Create an instance of {@link ResponseConsultaUsrLDAP }
     * 
     */
    public ResponseConsultaUsrLDAP createResponseConsultaUsrLDAP() {
        return new ResponseConsultaUsrLDAP();
    }

    /**
     * Create an instance of {@link RequestCorporativo }
     * 
     */
    public RequestCorporativo createRequestCorporativo() {
        return new RequestCorporativo();
    }

    /**
     * Create an instance of {@link RequestLocal }
     * 
     */
    public RequestLocal createRequestLocal() {
        return new RequestLocal();
    }

    /**
     * Create an instance of {@link RequestLDAP }
     * 
     */
    public RequestLDAP createRequestLDAP() {
        return new RequestLDAP();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLocalUserFromCorpIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getLocalUserFromCorpIDResponse")
    public JAXBElement<GetLocalUserFromCorpIDResponse> createGetLocalUserFromCorpIDResponse(GetLocalUserFromCorpIDResponse value) {
        return new JAXBElement<GetLocalUserFromCorpIDResponse>(_GetLocalUserFromCorpIDResponse_QNAME, GetLocalUserFromCorpIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLocationFromCorpIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getLocationFromCorpIDResponse")
    public JAXBElement<GetLocationFromCorpIDResponse> createGetLocationFromCorpIDResponse(GetLocationFromCorpIDResponse value) {
        return new JAXBElement<GetLocationFromCorpIDResponse>(_GetLocationFromCorpIDResponse_QNAME, GetLocationFromCorpIDResponse.class, null, value);
    }
}
