
package mx.isban.resolver.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para requestLDAP complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="requestLDAP">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claveAplicativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usuarioAplicativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestLDAP", propOrder = {
    "claveAplicativo",
    "usuarioAplicativo"
})
@XmlSeeAlso({
    RequestCorporativo.class,
    RequestLocal.class
})
public class RequestLDAP {

    protected String claveAplicativo;
    protected String usuarioAplicativo;

    /**
     * Obtiene el valor de la propiedad claveAplicativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveAplicativo() {
        return claveAplicativo;
    }

    /**
     * Define el valor de la propiedad claveAplicativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveAplicativo(String value) {
        this.claveAplicativo = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioAplicativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioAplicativo() {
        return usuarioAplicativo;
    }

    /**
     * Define el valor de la propiedad usuarioAplicativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioAplicativo(String value) {
        this.usuarioAplicativo = value;
    }

}
