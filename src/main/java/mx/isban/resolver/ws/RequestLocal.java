
package mx.isban.resolver.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para requestLocal complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="requestLocal">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.resolver.isban.mx/}requestLDAP">
 *       &lt;sequence>
 *         &lt;element name="identificadorLocal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestLocal", propOrder = {
    "identificadorLocal"
})
public class RequestLocal
    extends RequestLDAP
{

    protected String identificadorLocal;

    /**
     * Obtiene el valor de la propiedad identificadorLocal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificadorLocal() {
        return identificadorLocal;
    }

    /**
     * Define el valor de la propiedad identificadorLocal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificadorLocal(String value) {
        this.identificadorLocal = value;
    }

}
