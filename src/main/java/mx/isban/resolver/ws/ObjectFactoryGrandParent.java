
package mx.isban.resolver.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.isban.resolver.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactoryGrandParent {

    private final static QName _GetEmailFromCorpID_QNAME = new QName("http://ws.resolver.isban.mx/", "getEmailFromCorpID");
    private final static QName _GetCommonNameFromCorpID_QNAME = new QName("http://ws.resolver.isban.mx/", "getCommonNameFromCorpID");
    private final static QName _GetCorporateUserFromLocalID_QNAME = new QName("http://ws.resolver.isban.mx/", "getCorporateUserFromLocalID");
    private final static QName _GetEmailFromCorpIDResponse_QNAME = new QName("http://ws.resolver.isban.mx/", "getEmailFromCorpIDResponse");
    private final static QName _GetPositionFromCorpID_QNAME = new QName("http://ws.resolver.isban.mx/", "getPositionFromCorpID");
    private final static QName _GetPositionFromCorpIDResponse_QNAME = new QName("http://ws.resolver.isban.mx/", "getPositionFromCorpIDResponse");
    private final static QName _GetLocalUserFromCorpID_QNAME = new QName("http://ws.resolver.isban.mx/", "getLocalUserFromCorpID");
    private final static QName _GetCorporateUserFromLocalIDResponse_QNAME = new QName("http://ws.resolver.isban.mx/", "getCorporateUserFromLocalIDResponse");
    private final static QName _GetCommonNameFromCorpIDResponse_QNAME = new QName("http://ws.resolver.isban.mx/", "getCommonNameFromCorpIDResponse");
    private final static QName _GetLocationFromCorpID_QNAME = new QName("http://ws.resolver.isban.mx/", "getLocationFromCorpID");

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEmailFromCorpID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getEmailFromCorpID")
    public JAXBElement<GetEmailFromCorpID> createGetEmailFromCorpID(GetEmailFromCorpID value) {
        return new JAXBElement<GetEmailFromCorpID>(_GetEmailFromCorpID_QNAME, GetEmailFromCorpID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommonNameFromCorpID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getCommonNameFromCorpID")
    public JAXBElement<GetCommonNameFromCorpID> createGetCommonNameFromCorpID(GetCommonNameFromCorpID value) {
        return new JAXBElement<GetCommonNameFromCorpID>(_GetCommonNameFromCorpID_QNAME, GetCommonNameFromCorpID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCorporateUserFromLocalID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getCorporateUserFromLocalID")
    public JAXBElement<GetCorporateUserFromLocalID> createGetCorporateUserFromLocalID(GetCorporateUserFromLocalID value) {
        return new JAXBElement<GetCorporateUserFromLocalID>(_GetCorporateUserFromLocalID_QNAME, GetCorporateUserFromLocalID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEmailFromCorpIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getEmailFromCorpIDResponse")
    public JAXBElement<GetEmailFromCorpIDResponse> createGetEmailFromCorpIDResponse(GetEmailFromCorpIDResponse value) {
        return new JAXBElement<GetEmailFromCorpIDResponse>(_GetEmailFromCorpIDResponse_QNAME, GetEmailFromCorpIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPositionFromCorpID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getPositionFromCorpID")
    public JAXBElement<GetPositionFromCorpID> createGetPositionFromCorpID(GetPositionFromCorpID value) {
        return new JAXBElement<GetPositionFromCorpID>(_GetPositionFromCorpID_QNAME, GetPositionFromCorpID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPositionFromCorpIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getPositionFromCorpIDResponse")
    public JAXBElement<GetPositionFromCorpIDResponse> createGetPositionFromCorpIDResponse(GetPositionFromCorpIDResponse value) {
        return new JAXBElement<GetPositionFromCorpIDResponse>(_GetPositionFromCorpIDResponse_QNAME, GetPositionFromCorpIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLocalUserFromCorpID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getLocalUserFromCorpID")
    public JAXBElement<GetLocalUserFromCorpID> createGetLocalUserFromCorpID(GetLocalUserFromCorpID value) {
        return new JAXBElement<GetLocalUserFromCorpID>(_GetLocalUserFromCorpID_QNAME, GetLocalUserFromCorpID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCorporateUserFromLocalIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getCorporateUserFromLocalIDResponse")
    public JAXBElement<GetCorporateUserFromLocalIDResponse> createGetCorporateUserFromLocalIDResponse(GetCorporateUserFromLocalIDResponse value) {
        return new JAXBElement<GetCorporateUserFromLocalIDResponse>(_GetCorporateUserFromLocalIDResponse_QNAME, GetCorporateUserFromLocalIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommonNameFromCorpIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getCommonNameFromCorpIDResponse")
    public JAXBElement<GetCommonNameFromCorpIDResponse> createGetCommonNameFromCorpIDResponse(GetCommonNameFromCorpIDResponse value) {
        return new JAXBElement<GetCommonNameFromCorpIDResponse>(_GetCommonNameFromCorpIDResponse_QNAME, GetCommonNameFromCorpIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLocationFromCorpID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.resolver.isban.mx/", name = "getLocationFromCorpID")
    public JAXBElement<GetLocationFromCorpID> createGetLocationFromCorpID(GetLocationFromCorpID value) {
        return new JAXBElement<GetLocationFromCorpID>(_GetLocationFromCorpID_QNAME, GetLocationFromCorpID.class, null, value);
    }

}
