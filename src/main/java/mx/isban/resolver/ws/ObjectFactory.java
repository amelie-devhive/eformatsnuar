
package mx.isban.resolver.ws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.isban.resolver.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory extends ObjectFactoryParent {


	/**
     * Create an instance of {@link GetPositionFromCorpIDResponse }
     * 
     */
    public GetPositionFromCorpIDResponse createGetPositionFromCorpIDResponse() {
        return new GetPositionFromCorpIDResponse();
    }

    /**
     * Create an instance of {@link GetLocalUserFromCorpIDResponse }
     * 
     */
    public GetLocalUserFromCorpIDResponse createGetLocalUserFromCorpIDResponse() {
        return new GetLocalUserFromCorpIDResponse();
    }

    /**
     * Create an instance of {@link GetCommonNameFromCorpID }
     * 
     */
    public GetCommonNameFromCorpID createGetCommonNameFromCorpID() {
        return new GetCommonNameFromCorpID();
    }

    /**
     * Create an instance of {@link GetEmailFromCorpIDResponse }
     * 
     */
    public GetEmailFromCorpIDResponse createGetEmailFromCorpIDResponse() {
        return new GetEmailFromCorpIDResponse();
    }

    /**
     * Create an instance of {@link GetCorporateUserFromLocalIDResponse }
     * 
     */
    public GetCorporateUserFromLocalIDResponse createGetCorporateUserFromLocalIDResponse() {
        return new GetCorporateUserFromLocalIDResponse();
    }

    /**
     * Create an instance of {@link GetCommonNameFromCorpIDResponse }
     * 
     */
    public GetCommonNameFromCorpIDResponse createGetCommonNameFromCorpIDResponse() {
        return new GetCommonNameFromCorpIDResponse();
    }

    /**
     * Create an instance of {@link GetEmailFromCorpID }
     * 
     */
    public GetEmailFromCorpID createGetEmailFromCorpID() {
        return new GetEmailFromCorpID();
    }

    /**
     * Create an instance of {@link GetLocationFromCorpID }
     * 
     */
    public GetLocationFromCorpID createGetLocationFromCorpID() {
        return new GetLocationFromCorpID();
    }
}
