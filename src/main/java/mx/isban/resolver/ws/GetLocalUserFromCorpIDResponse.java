
package mx.isban.resolver.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getLocalUserFromCorpIDResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getLocalUserFromCorpIDResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.resolver.isban.mx/}responseConsultaUsrLDAP" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getLocalUserFromCorpIDResponse", propOrder = {
    "_return"
})
public class GetLocalUserFromCorpIDResponse {

    @XmlElement(name = "return")
    protected ResponseConsultaUsrLDAP _return;

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link ResponseConsultaUsrLDAP }
     *     
     */
    public ResponseConsultaUsrLDAP getReturn() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseConsultaUsrLDAP }
     *     
     */
    public void setReturn(ResponseConsultaUsrLDAP value) {
        this._return = value;
    }

}
