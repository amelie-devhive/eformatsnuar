
package mx.isban.resolver.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(name = "ResolverConsultaServiceWS", targetNamespace = "http://ws.resolver.isban.mx/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ResolverConsultaServiceWS {


    /**
     * 
     * @param reqUsrLDAP
     * @return
     *     returns mx.isban.resolver.ws.ResponseConsultaUsrLDAP
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getLocalUserFromCorpID", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetLocalUserFromCorpID")
    @ResponseWrapper(localName = "getLocalUserFromCorpIDResponse", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetLocalUserFromCorpIDResponse")
    @Action(input = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getLocalUserFromCorpIDRequest", output = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getLocalUserFromCorpIDResponse")
    public ResponseConsultaUsrLDAP getLocalUserFromCorpID(
        @WebParam(name = "reqUsrLDAP", targetNamespace = "")
        RequestCorporativo reqUsrLDAP);

    /**
     * 
     * @param arg0
     * @return
     *     returns mx.isban.resolver.ws.ResponseConsultaUsrLDAP
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getCorporateUserFromLocalID", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetCorporateUserFromLocalID")
    @ResponseWrapper(localName = "getCorporateUserFromLocalIDResponse", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetCorporateUserFromLocalIDResponse")
    @Action(input = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getCorporateUserFromLocalIDRequest", output = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getCorporateUserFromLocalIDResponse")
    public ResponseConsultaUsrLDAP getCorporateUserFromLocalID(
        @WebParam(name = "arg0", targetNamespace = "")
        RequestLocal arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns mx.isban.resolver.ws.ResponseConsultaUsrLDAP
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getPositionFromCorpID", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetPositionFromCorpID")
    @ResponseWrapper(localName = "getPositionFromCorpIDResponse", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetPositionFromCorpIDResponse")
    @Action(input = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getPositionFromCorpIDRequest", output = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getPositionFromCorpIDResponse")
    public ResponseConsultaUsrLDAP getPositionFromCorpID(
        @WebParam(name = "arg0", targetNamespace = "")
        RequestCorporativo arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns mx.isban.resolver.ws.ResponseConsultaUsrLDAP
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getEmailFromCorpID", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetEmailFromCorpID")
    @ResponseWrapper(localName = "getEmailFromCorpIDResponse", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetEmailFromCorpIDResponse")
    @Action(input = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getEmailFromCorpIDRequest", output = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getEmailFromCorpIDResponse")
    public ResponseConsultaUsrLDAP getEmailFromCorpID(
        @WebParam(name = "arg0", targetNamespace = "")
        RequestCorporativo arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns mx.isban.resolver.ws.ResponseConsultaUsrLDAP
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getLocationFromCorpID", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetLocationFromCorpID")
    @ResponseWrapper(localName = "getLocationFromCorpIDResponse", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetLocationFromCorpIDResponse")
    @Action(input = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getLocationFromCorpIDRequest", output = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getLocationFromCorpIDResponse")
    public ResponseConsultaUsrLDAP getLocationFromCorpID(
        @WebParam(name = "arg0", targetNamespace = "")
        RequestCorporativo arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns mx.isban.resolver.ws.ResponseConsultaUsrLDAP
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getCommonNameFromCorpID", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetCommonNameFromCorpID")
    @ResponseWrapper(localName = "getCommonNameFromCorpIDResponse", targetNamespace = "http://ws.resolver.isban.mx/", className = "mx.isban.resolver.ws.GetCommonNameFromCorpIDResponse")
    @Action(input = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getCommonNameFromCorpIDRequest", output = "http://ws.resolver.isban.mx/ResolverConsultaServiceWS/getCommonNameFromCorpIDResponse")
    public ResponseConsultaUsrLDAP getCommonNameFromCorpID(
        @WebParam(name = "arg0", targetNamespace = "")
        RequestCorporativo arg0);

}
