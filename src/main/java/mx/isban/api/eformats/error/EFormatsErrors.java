package mx.isban.api.eformats.error;

import org.springframework.http.HttpStatus;

import mx.isban.api.eformats.dto.EFormatsError;

public enum EFormatsErrors {
	CAT_DB_UNKNOWN_ERR("err#001", "facultad.db.unknownError", "Problemas con la consulta del catalogo en la Base de Datos", HttpStatus.CONFLICT),
	CAT_DATOS_NO_CONTENT("err#002", "cat.datos.noContent", "No se encontraron datos en el catálogo", HttpStatus.NOT_FOUND),
	CAT_DATOS_NULL_CVE_CAT("err#003", "cat.datos.nullCveCat", "Se requiere la clave del catálogo a consultar", HttpStatus.BAD_REQUEST),
	CAT_FACULTAD_NULL_NUM_FAC("err#004", "cat.facultad.nullumFac", "Para consultar por clave de facultad se requiere el número de facultad", HttpStatus.BAD_REQUEST),
	
	FAVORITE_DB_UNKNOWN_ERR("err#005", "favorite.db.unknownError", "Problemas con la consulta de favoritos en la Base de Datos", HttpStatus.CONFLICT),
	FAVORITE_NOT_FOUND("err#006", "favorite.notFound.byId", "No se encontró el formato favorito buscado", HttpStatus.NOT_FOUND),
	FAVORITE_NO_CONTENT("err#007", "favorite.notFound.favorites", "No se encontraron registros en la base de datos de formatos favoritos", HttpStatus.NO_CONTENT),
	FAVORITE_SAVE_INVALID("err#008", "favorite.save.invalid", "Valores no válidos en el objeto Formato Favorito", HttpStatus.BAD_REQUEST),
	FAVORITE_SAVE_ERROR("err#009", "favorite.save.error", "No se pudo guardar el Formato Favorito", HttpStatus.CONFLICT),
	FAVORITE_UPDATE_INVALID("err#010", "favorite.update.invalid", "Valores no válidos en el objeto Formato Favorito", HttpStatus.BAD_REQUEST),
	FAVORITE_UPDATE_ERROR("err#011", "favorite.update.error", "No se pudo actualizar el Formato Favorito", HttpStatus.CONFLICT),
	FAVORITE_DELETE_INVALID("err#012", "favorite.delete.invalid", "Se requiere el id del Formato Favorito", HttpStatus.BAD_REQUEST),
	FAVORITE_DELETE_ERROR("err#013", "favorite.delete.error", "No se pudo borrar el Formato Favorito", HttpStatus.CONFLICT),

	FORMAT_DB_UNKNOWN_ERR("err#014", "format.db.unknownError", "Problemas con la consulta de formato en la Base de Datos", HttpStatus.CONFLICT),
	FORMAT_NOT_FOUND("err#015", "format.notFound.format", "No se encontraron registros en la base de datos para formatos", HttpStatus.NO_CONTENT),
	FORMAT_SAVE_ERROR("err#016", "format.save.error", "Hubo un problema al guardar la información", HttpStatus.INTERNAL_SERVER_ERROR),
	FORMAT_UPDATE_INVALID("err#017", "format.update.invalid", "No hay datos qué actualizar", HttpStatus.NOT_MODIFIED),
	FORMAT_UPDATE_INEXISTENT("err#018", "format.update.inexistent", "No existe el registro en la base de datos", HttpStatus.NOT_FOUND),
	FORMAT_UPDATE_ERROR("err#019", "format.update.error", "Hubo un problema al actualiza la información", HttpStatus.INTERNAL_SERVER_ERROR),
	FORMAT_DELETE_INVALID("err#020", "format.delete.invalid", "El id proporcionado no es válido", HttpStatus.BAD_REQUEST),
	FORMAT_DELETE_ERROR("err#021", "format.delete.error", "No fue posible eliminar el registro", HttpStatus.CONFLICT),

	BITACORA_DB_UNKNOWN_ERR("err#022", "bitacora.db.unknownError", "Problemas con la consulta de bitacora en la Base de Datos", HttpStatus.CONFLICT),
	BITACORA_NULL_NUM_EJECUTIVO("err#023", "bitacora.null.numEjecutivo", "Se requiere el número del Ejecutivo", HttpStatus.BAD_REQUEST),
	BITACORA_NOT_FOUND_TOP("err#024", "bitacora.notFound.topBitacora", "No se encontró registros para el ejecutivo proporcionado", HttpStatus.NOT_FOUND),
	BITACORA_INVALID_DATA_SAVING("err#025", "bitacora.save.invalidData", "Valores no válidos en el objeto Bitacora", HttpStatus.BAD_REQUEST),

	CLIENTE_DB_UNKNOWN_ERR("err#026", "cliente.db.unknownError", "Problemas con la consulta de cliente en la Base de Datos", HttpStatus.CONFLICT),
	CLIENTE_NULL_NUM_CLIENTE("err#027", "cliente.null.numCte", "Se requiere el número de cliente", HttpStatus.BAD_REQUEST),
	CLIENTE_NOT_FOUND_NUM_CTE("err#028", "cliente.notFound.numCte", "No se pudo encontrar el cliente", HttpStatus.NOT_FOUND),

	CUENTA_DB_UNKNOWN_ERR("err#029", "cuenta.db.unknownError", "Problemas con la consulta de cuenta en la Base de Datos", HttpStatus.CONFLICT),
	CUENTA_NULL_NUM_CTA("err#030", "cuenta.null.numCta", "Es necesario ingresar la cuenta a buscar", HttpStatus.BAD_REQUEST),
	CUENTA_NOT_FOUND_NUM_CTA("err#031", "cuenta.notFound.byNumCta", "No se encontró la cuenta", HttpStatus.NOT_FOUND),
	CUENTA_NOT_FOUND_MOVIL("err#032", "cuenta.notFound.byMovil", "No se encontró una cuenta asociada al móvil proporcionado", HttpStatus.NOT_FOUND),
	
	TIPO_PERFIL_DB_UNKNOWN_ERR("err#033", "tipoperfil.db.unknownError", "Problemas con la consulta del tipo de perfil en la Base de Datos", HttpStatus.CONFLICT),
	TIPO_PERFIL_NO_CONTENT("err#034", "tipoperfil.noContent", "No se encontraron datos en el tipo de perfil", HttpStatus.NOT_FOUND),
	
	PERFIL_DB_UNKNOWN_ERR("err#035", "perfil.db.unknownError", "Problemas con la consulta de perfiles en la Base de Datos", HttpStatus.CONFLICT),
	PERFIL_NO_CONTENT("err#036", "perfil.noContent", "No se encontraron datos en el perfil", HttpStatus.NOT_FOUND),

	CAT_FORMAT_DB_UNKNOWN_ERR("err#037", "catformat.db.unknownError", "Problemas con la consulta del catalogo de formatos en la Base de Datos", HttpStatus.CONFLICT),
	CAT_FORMAT_NO_CONTENT("err#038", "catformat.noContent", "No se encontraron datos en el catalogo de formatos", HttpStatus.NOT_FOUND),
	
	FILE_INVALID_NAME("err#033", "file.invalidName", "Se requiere un nombre de archivo válido", HttpStatus.BAD_REQUEST),
	FILE_NOT_FOUND("err#034", "file.notFound", "No se encontró el archivo solicitado", HttpStatus.NOT_FOUND),
	FILE_UNKNOWN_ERROR("err#035", "file.unknownError", "Hubo un problema al intentar leer el archivo", HttpStatus.NOT_FOUND),
	WSDL_SERVICE_ERROR("err#036", "wsdl.unknownService", "Hubo un problema al consultar el servicio web", HttpStatus.SERVICE_UNAVAILABLE),
	WS_RESOLVER_ERROR("err#wsdlresolver", "wsdl.wsdlresolver", null, HttpStatus.NOT_FOUND)
	;
	private EFormatsError error;
	
	private EFormatsErrors(String hashErr, String codigo, String errMsg, HttpStatus httpStatus) {
		error = new EFormatsError(hashErr, codigo, errMsg, httpStatus);
	}

	public EFormatsError getValue() {
		return error;
	}
	
}
