package mx.isban.api.eformats.repository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mx.isban.api.eformats.model.FeiMxPrcCatFact;

public interface CatFacultadesRepository extends JpaRepository<FeiMxPrcCatFact, Long> {

	/**
	 * Query consultar el catálogo de facultades por numero de facultad
	 * @param numFac numero de facultad
	 * @return Lista de registros del catálogo 
	 */
	@Query(name = FeiMxPrcCatFact.FIND_BY_NUM_FAC, 
			value = "select datos from FeiMxPrcCatFact datos " + 
			" where trim(datos.numFact) = trim(:numFac)")
	List<FeiMxPrcCatFact> findByNumFac(@Param("numFac") String numFac);
	
	/**
	 * Query consultar el catálogo de facultades por numero de facultad y clave de facultad
	 * @param numFac numero de facultad
	 * @param cveFac clave de facultad
	 * @return Lista de registros del catálogo 
	 */
	@Query(name = FeiMxPrcCatFact.FIND_BY_NUM_FAC_CVE_FAC, 
			value = "select datos from FeiMxPrcCatFact datos " + 
			" where trim(datos.numFact) = trim(:numFac) " + 
			" and trim(datos.valClaveFact) = trim(:cveFac) ")
	List<FeiMxPrcCatFact> findByNumFacCveFac(@Param("numFac") String numFac, @Param("cveFac") String cveFac);
	
	/**
	 * Query Obtener lista de facultades de un perfil en particular
	 * @return Lista de registros del catálogo 
	 */
//	@Query(name = FeiMxPrcCatFact.FIND_BY_ID_PERFIL, 
//			value = "select fac from FeiMxPrcCatFact fac "
//					+ " inner join FeiMxRelFactPerfi facperfil " 
//					+ " on fac.idFact = facperfil.feiMxPrcCatPerfi.feiMxPrcCatFact.idFact"
//					+ " where fac.dscEstatFact = 'A' and facperfil.feiMxPrcCatPerfi.idPerfi = :idPerfil")
	@Query(name = FeiMxPrcCatFact.FIND_BY_ID_PERFIL, 
	value = "select fac from FeiMxPrcCatFact fac, FeiMxRelFactPerfi facperfil " 
			+ " where fac.idFact = facperfil.feiMxPrcCatFact.idFact"
			+ " and fac.dscEstatFact = 'A' and facperfil.feiMxPrcCatPerfi.idPerfi = :idPerfil")
	List<FeiMxPrcCatFact> findByIdPerfil(@Valid @Param("idPerfil")long idPerfil);
	

	/**
	 * Query Obtener lista de facultades de un perfil en particular
	 * @return Lista de registros del catálogo 
	 */
	@Query(name = FeiMxPrcCatFact.FIND_EXTRAORDINARIAS, 
			value = "select fac from FeiMxPrcCatFact fac, FeiMxRelFactTipoPerfi tipoperfil " 
					+ " where fac.idFact = tipoperfil.feiMxPrcCatFact.idFact"
					+ " and fac.dscEstatFact = 'A' and tipoperfil.feiMxPrcCatTipoPerfi.idTipoPerfi = :idTipoPerfil")
	List<FeiMxPrcCatFact> findExtraordinarias(@Valid @Param("idTipoPerfil")long idTipoPerfil);
}
