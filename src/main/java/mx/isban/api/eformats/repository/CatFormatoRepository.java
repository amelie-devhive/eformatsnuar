package mx.isban.api.eformats.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mx.isban.api.eformats.model.FeiMxMaeCatFmt;

public interface CatFormatoRepository extends JpaRepository<FeiMxMaeCatFmt, Long> {
	/**
	 * Query para los formatos relacionados a la palabra clave
	 * @param keyword palabra clave para la busqueda el formato
	 * @return Lista de formatos que conincidan con la palabra clave
	 */
	@Query(name = FeiMxMaeCatFmt.FIND_LIKE_KEYWORD, 
			value = "select catFormato from FeiMxMaeCatFmt catFormato " + 
			" where upper(catFormato.valClaveFmt) like '%' || upper(:keyword) || '%' " +
			" or upper(catFormato.txtPlbraClave) like '%' || upper(:keyword) || '%' ")
	List<FeiMxMaeCatFmt> findLikeKeyword(@Param("keyword") String keyword);
}
