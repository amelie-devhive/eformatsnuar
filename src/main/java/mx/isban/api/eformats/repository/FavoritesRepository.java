package mx.isban.api.eformats.repository;

import org.springframework.data.repository.CrudRepository;

import mx.isban.api.eformats.model.FeiMxRelFmtFav;

public interface FavoritesRepository extends CrudRepository<FeiMxRelFmtFav, Long> {

}
