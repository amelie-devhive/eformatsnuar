package mx.isban.api.eformats.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mx.isban.api.eformats.model.FeiMxPrcCatPerfi;

public interface PerfilRepository extends JpaRepository<FeiMxPrcCatPerfi, Long> {

	/**
	 * Query consultar el catálogo de perfiles por tipo de perfil
	 * @return Lista de registros del catálogo 
	 */
	@Query(name = FeiMxPrcCatPerfi.FIND_BY_TIPO_PERFIL, 
			value = "select datos from FeiMxPrcCatPerfi datos " + 
			" where datos.dscEstatPerfi = 'A' and datos.feiMxPrcCatTipoPerfi.idTipoPerfi = :idTipoPerfil")
	List<FeiMxPrcCatPerfi> findByTipoPerfil(@Param("idTipoPerfil")long idTipoPerfil);
	
}
