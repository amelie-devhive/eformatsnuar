package mx.isban.api.eformats.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mx.isban.api.eformats.model.FeiMxRelDatCat;

public interface CatalogoRepository extends JpaRepository<FeiMxRelDatCat, Long> {

	/**
	 * Query para buscar los datos de un catálogo por su clave de catálogo
	 * @param claveCat clave del catalogo a buscar, ej: CAT_002
	 * @return Lista de datos del catálogo con la clave buscada
	 */
//	@Query(name = FeiMxRelDatosCat.FIND_BY_CVE_CATALOGO, 
//			value = "select datos from FeiMxRelCatSi cat inner join FeiMxRelDatCat datos" + 
//			" on cat.idCat=datos.idCat" + 
//			" where trim(cat.claveCat) = trim(:cveCat)")
	@Query(name = FeiMxRelDatCat.FIND_BY_CVE_CATALOGO, 
			value = "select datos from FeiMxRelCatSi cat, FeiMxRelDatCat datos" + 
			" where cat.idCat=datos.feiMxRelCatSi.idCat" + 
			" and trim(cat.valClaveCat) = trim(:cveCat)")
	List<FeiMxRelDatCat> findByClaveCatalogo(@Param("cveCat") String claveCat);
}
