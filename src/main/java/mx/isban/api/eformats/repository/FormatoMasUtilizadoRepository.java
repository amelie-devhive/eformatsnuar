package mx.isban.api.eformats.repository;

import org.springframework.data.repository.CrudRepository;

import mx.isban.api.eformats.model.FeiMxRelFormatoMasUtilizado;

public interface FormatoMasUtilizadoRepository extends CrudRepository<FeiMxRelFormatoMasUtilizado, Long> {

}
