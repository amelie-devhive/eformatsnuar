package mx.isban.api.eformats.repository;

import org.springframework.data.repository.CrudRepository;

import mx.isban.api.eformats.model.Format;

public interface FormatRepository extends CrudRepository<Format, Long> {

}
