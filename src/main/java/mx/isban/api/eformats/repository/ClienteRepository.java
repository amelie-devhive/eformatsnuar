package mx.isban.api.eformats.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import mx.isban.api.eformats.dto.ClienteDto;

@Repository
public class ClienteRepository {

	public ClienteDto buscarClienteBuc(String numCliente) {
		
		final Map<String, ClienteDto> clientes = new HashMap<>();
		clientes.put("12345678", new ClienteDto("12345678", "Juán Pérez Pérez"));
		clientes.put("56426700", new ClienteDto("56426700", "Karmen Ortíz Ramirez"));
		clientes.put("34598701", new ClienteDto("34598701", "Alma Torres Pineda"));
		clientes.put("38976544", new ClienteDto("38976544", "Roberto Montreal Herrera"));
		
		ClienteDto cliente = clientes.get(numCliente);
		return cliente;
	}
	
}
