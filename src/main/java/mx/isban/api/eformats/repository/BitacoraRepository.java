package mx.isban.api.eformats.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import mx.isban.api.eformats.model.FeiMxLogBta;

public interface BitacoraRepository extends JpaRepository<FeiMxLogBta, Long> {

	/**
	 * Query para buscar el top de bitacoras (formatos) de un ejecutivo
	 * @param numEjecutivo numero de ejecutivo que consulta
	 * @param topCount numero maximo en el top
	 * @return Lista de datos del catálogo con la clave buscada
	 */
	List findTop(
			@Param("numEjecutivo") 
			String numEjecutivo, 
			@Param("topCount") 
			int topCount);
}
