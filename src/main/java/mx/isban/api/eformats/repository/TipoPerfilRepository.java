package mx.isban.api.eformats.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.isban.api.eformats.model.FeiMxPrcCatTipoPerfi;

public interface TipoPerfilRepository extends JpaRepository<FeiMxPrcCatTipoPerfi, Long> {

	/**
	 * Query consultar el catálogo de tipos de perfil
	 * @return Lista de registros del catálogo 
	 */
	@Query(name = FeiMxPrcCatTipoPerfi.FIND_ACTIVE, 
			value = "select datos from FeiMxPrcCatTipoPerfi datos " + 
			" where datos.dscEstatPerfi = 'A'")
	List<FeiMxPrcCatTipoPerfi> findActive();
	
}
