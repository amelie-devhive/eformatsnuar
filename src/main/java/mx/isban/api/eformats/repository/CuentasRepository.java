package mx.isban.api.eformats.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import mx.isban.api.eformats.dto.CuentaDto;

@Repository
public class CuentasRepository {
	
	public CuentaDto buscarCuentaConMovil(String numMovil) {
		final Map<String, CuentaDto> cuentasByMovil = new HashMap<>();
		cuentasByMovil.put("5510205567", new CuentaDto("12345678901", "12345678", "Juán Pérez Pérez", "MXN"));
		cuentasByMovil.put("5589871013", new CuentaDto("99999999992", "12345678", "Juán Pérez Pérez", "MXN"));
		cuentasByMovil.put("5534546778", new CuentaDto("55524398763", "56426700", "Karmen Ortíz Ramirez", "USD"));
		cuentasByMovil.put("5501989001", new CuentaDto("19564269904", "34598701", "Alma Torres Pineda", "MXN"));
		cuentasByMovil.put("5567666561", new CuentaDto("86319765245", "38976544", "Roberto Montreal Herrera", "USD"));
		
		CuentaDto cuenta = cuentasByMovil.get(numMovil);
		return cuenta;
	}
	
	public CuentaDto buscarCuentaCheques(String numCuenta) {
		Map<String, CuentaDto> cuentas = new HashMap<>();
		cuentas.put("12345678901", new CuentaDto("12345678901", "12345678", "Juán Pérez Pérez", "MXN"));
		cuentas.put("99999999992", new CuentaDto("99999999992", "12345678", "Juán Pérez Pérez", "MXN"));
		cuentas.put("55524398763", new CuentaDto("55524398763", "56426700", "Karmen Ortíz Ramirez", "USD"));
		cuentas.put("19564269904", new CuentaDto("19564269904", "34598701", "Alma Torres Pineda", "MXN"));
		cuentas.put("86319765245", new CuentaDto("86319765245", "38976544", "Roberto Montreal Herrera", "USD"));
		CuentaDto cuenta = cuentas.get(numCuenta);
		return cuenta;
	}
	
}
