package mx.isban.api.eformats;


import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Start point of the spring boot application
 * @author Amelie
 */
@SpringBootApplication
@EnableSwagger2
@EntityScan("mx.isban.api.eformats.model")
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages="mx.isban.api.eformats.repository")
@ComponentScan({"mx.isban.api.eformats"})
@EnableTransactionManagement
@EnableWebMvc
public class Application extends SpringBootServletInitializer{

	/**
	 * start app spring boot
	 * @param args recibe argumentos para inicialización de proyecto
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * data source bean config
	 * @return
	 */
	@Bean("datasource")
	@ConfigurationProperties(prefix="datasource")
	public DataSource dataSource(){
		return DataSourceBuilder.create().build();
	}
	
	/**
	 * jdbcTemplate bean
	 * @param dataSource
	 * @return
	 */
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource){
		return new JdbcTemplate(dataSource);
	}
	
	/**
	 * Configure of spring boot basis
	 */
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(Application.class);
    }
 
	/**
	 * bean of cors configuration in order to cross domain client calling
	 * @return
	 */
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
					.allowedOrigins( "http://ec2-13-56-168-103.us-west-1.compute.amazonaws.com",
							"http://localhost:9000", "http://localhost:8990", "http://localhost:5000",
							"https://mxfe-ms-formatos-mxformatelectronicos-dev.appls.cto1.paas.gsnetcloud.corp",
							"https://mxfe-ms-transacciones-mxformatelectronicos-dev.appls.cto1.paas.gsnetcloud.corp",
							"https://mxfe-ng-formatos-mxformatelectronicos-dev.appls.cto1.paas.gsnetcloud.corp"
					).allowedHeaders("*")
					.allowedMethods( "GET", "POST", "PUT", "DELETE", "OPTIONS" );
			}
		};
	}

	/**
	 * rest template bean
	 * @return
	 */
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
	/**
	 * default sampler bean
	 * @return
	 */
	@Bean 
	public AlwaysSampler defaultSampler() {
	  return new AlwaysSampler(); 
	}
}
