package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_MAE_ACT_RFC database table.
 * 
 */
@Entity
@Table(name="FEI_MX_MAE_ACT_RFC")
@NamedQuery(name="FeiMxMaeActRfc.findAll", query="SELECT f FROM FeiMxMaeActRfc f")
public class FeiMxMaeActRfc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_MAE_ACT_RFC_IDACTRFC_GENERATOR", sequenceName="FEI_MX_MAE_ACT_RFC_ID_ACT_RFC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_MAE_ACT_RFC_IDACTRFC_GENERATOR")
	@Column(name="ID_ACT_RFC", unique=true, nullable=false, precision=38)
	private long idActRfc;

	@Column(name="DSC_NOM_ACT", length=255)
	private String dscNomAct;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxRelAct
	@OneToMany(mappedBy="feiMxMaeActRfc")
	private List<FeiMxRelAct> feiMxRelActs;

	public FeiMxMaeActRfc() {
	}

	public long getIdActRfc() {
		return this.idActRfc;
	}

	public void setIdActRfc(long idActRfc) {
		this.idActRfc = idActRfc;
	}

	public String getDscNomAct() {
		return this.dscNomAct;
	}

	public void setDscNomAct(String dscNomAct) {
		this.dscNomAct = dscNomAct;
	}

	public Timestamp getFchHorUltAct() {
		if(fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null) {
			this.fchHorUltAct = null;
		}else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public List<FeiMxRelAct> getFeiMxRelActs() {
		return new ArrayList<FeiMxRelAct>(feiMxRelActs);
	}

	public void setFeiMxRelActs(List<FeiMxRelAct> feiMxRelActs) {
		this.feiMxRelActs = new ArrayList<FeiMxRelAct>(feiMxRelActs);
	}

	public FeiMxRelAct addFeiMxRelAct(FeiMxRelAct feiMxRelAct) {
		getFeiMxRelActs().add(feiMxRelAct);
		feiMxRelAct.setFeiMxMaeActRfc(this);

		return feiMxRelAct;
	}

	public FeiMxRelAct removeFeiMxRelAct(FeiMxRelAct feiMxRelAct) {
		getFeiMxRelActs().remove(feiMxRelAct);
		feiMxRelAct.setFeiMxMaeActRfc(null);

		return feiMxRelAct;
	}

}