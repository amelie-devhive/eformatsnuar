package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the FEI_MX_REL_CAT_TIPO_FMT database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_CAT_TIPO_FMT")
@NamedQuery(name="FeiMxRelCatTipoFmt.findAll", query="SELECT f FROM FeiMxRelCatTipoFmt f")
public class FeiMxRelCatTipoFmt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_REL_CAT_TIPO_FMT_IDTIPOFMT_GENERATOR", sequenceName="FEI_MX_REL_CAT_TIPO_FMT_ID_TIPO_FMT")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_CAT_TIPO_FMT_IDTIPOFMT_GENERATOR")
	@Column(name="ID_TIPO_FMT", unique=true, nullable=false, precision=38)
	private long idTipoFmt;

	@Column(name="DSC_ESTAT_FMT", length=1)
	private String dscEstatFmt;

	@Column(name="DSC_TIPO_FMT", length=20)
	private String dscTipoFmt;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxMaeCatFmt
	@OneToMany(mappedBy="feiMxRelCatTipoFmt")
	private List<FeiMxMaeCatFmt> feiMxMaeCatFmts;

	public FeiMxRelCatTipoFmt() {
		// default constructor
	}

	public long getIdTipoFmt() {
		return this.idTipoFmt;
	}

	public void setIdTipoFmt(long idTipoFmt) {
		this.idTipoFmt = idTipoFmt;
	}

	public String getDscEstatFmt() {
		return this.dscEstatFmt;
	}

	public void setDscEstatFmt(String dscEstatFmt) {
		this.dscEstatFmt = dscEstatFmt;
	}

	public String getDscTipoFmt() {
		return this.dscTipoFmt;
	}

	public void setDscTipoFmt(String dscTipoFmt) {
		this.dscTipoFmt = dscTipoFmt;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public List<FeiMxMaeCatFmt> getFeiMxMaeCatFmts() {
		if(feiMxMaeCatFmts == null) {
			return Collections.emptyList();
		}
		List<FeiMxMaeCatFmt> copy = new ArrayList<>();
		Collections.copy(copy, feiMxMaeCatFmts);
		return copy;
	}

	public void setFeiMxMaeCatFmts(List<FeiMxMaeCatFmt> feiMxMaeCatFmts) {
		if(feiMxMaeCatFmts == null) {
			this.feiMxMaeCatFmts = null;
		}else {
			Collections.copy(this.feiMxMaeCatFmts, feiMxMaeCatFmts);
		}
	}
	/**
	 * Agrega un elementio a la lista de catalogos
	 * @param feiMxMaeCatFormato
	 * @return
	 */
	public FeiMxMaeCatFmt addFeiMxMaeCatFmt(FeiMxMaeCatFmt feiMxMaeCatFmt) {
		getFeiMxMaeCatFmts().add(feiMxMaeCatFmt);
		feiMxMaeCatFmt.setFeiMxRelCatTipoFmt(this);

		return feiMxMaeCatFmt;
	}

	public FeiMxMaeCatFmt removeFeiMxMaeCatFmt(FeiMxMaeCatFmt feiMxMaeCatFmt) {
		getFeiMxMaeCatFmts().remove(feiMxMaeCatFmt);
		feiMxMaeCatFmt.setFeiMxRelCatTipoFmt(null);

		return feiMxMaeCatFmt;
	}

}