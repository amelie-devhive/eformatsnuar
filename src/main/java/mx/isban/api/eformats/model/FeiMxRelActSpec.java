package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_REL_ACT_SPEC database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_ACT_SPEC")
@NamedQuery(name="FeiMxRelActSpec.findAll", query="SELECT f FROM FeiMxRelActSpec f")
public class FeiMxRelActSpec implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private FeiMxRelActSpecPk id;
	
	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxMaeActGen
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACT_GEN", insertable=false, updatable=false)
	private FeiMxMaeActGen feiMxMaeActGen;

	//bi-directional many-to-one association to FeiMxMaeActSpecRfc
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACT_SPEC", insertable=false, updatable=false)
	private FeiMxMaeActSpecRfc feiMxMaeActSpecRfc;

	public FeiMxRelActSpec() {
	}

	public Timestamp getFchHorUltAct() {
		if(fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null) {
			this.fchHorUltAct = null;
		}else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public FeiMxMaeActGen getFeiMxMaeActGen() {
		return this.feiMxMaeActGen;
	}

	public void setFeiMxMaeActGen(FeiMxMaeActGen feiMxMaeActGen) {
		this.feiMxMaeActGen = feiMxMaeActGen;
	}

	public FeiMxMaeActSpecRfc getFeiMxMaeActSpecRfc() {
		return this.feiMxMaeActSpecRfc;
	}

	public void setFeiMxMaeActSpecRfc(FeiMxMaeActSpecRfc feiMxMaeActSpecRfc) {
		this.feiMxMaeActSpecRfc = feiMxMaeActSpecRfc;
	}

	public FeiMxRelActSpecPk getId() {
		return id;
	}

	public void setId(FeiMxRelActSpecPk id) {
		this.id = id;
	}

}