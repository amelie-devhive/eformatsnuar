package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_REL_DAT_CAT database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_DAT_CAT")
@NamedQuery(name="FeiMxRelDatCat.findAll", query="SELECT f FROM FeiMxRelDatCat f")
public class FeiMxRelDatCat implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_BY_CVE_CATALOGO = "FeiMxRelDatCat.findByCveCatalogo";

	@Id
	@SequenceGenerator(name="FEI_MX_REL_DAT_CAT_IDDAT_GENERATOR", sequenceName="FEI_MX_REL_DAT_CAT_ID_DAT")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_DAT_CAT_IDDAT_GENERATOR")
	@Column(name="ID_DAT", unique=true, nullable=false, precision=38)
	private long idDat;

	@Column(name="DSC_ADIC", length=200)
	private String dscAdic;

	@Column(name="DSC_ESTAT_DAT", length=1)
	private String dscEstatDat;

	@Column(name="DSC_NOM_DAT", length=200)
	private String dscNomDat;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxRelCatSi
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CAT", insertable=false, updatable=false)
	private FeiMxRelCatSi feiMxRelCatSi;

	//bi-directional many-to-one association to FeiMxRelDatCat
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_DAT_PADRE", insertable=false, updatable=false)
	private FeiMxRelDatCat feiMxRelDatCat;

	//bi-directional many-to-one association to FeiMxRelDatCat
	@OneToMany(mappedBy="feiMxRelDatCat")
	private List<FeiMxRelDatCat> feiMxRelDatCats;

	public FeiMxRelDatCat() {
		// default constructor
	}

	public long getIdDat() {
		return this.idDat;
	}

	public void setIdDat(long idDat) {
		this.idDat = idDat;
	}

	public String getDscAdic() {
		return this.dscAdic;
	}

	public void setDscAdic(String dscAdic) {
		this.dscAdic = dscAdic;
	}

	public String getDscEstatDat() {
		return this.dscEstatDat;
	}

	public void setDscEstatDat(String dscEstatDat) {
		this.dscEstatDat = dscEstatDat;
	}

	public String getDscNomDat() {
		return this.dscNomDat;
	}

	public void setDscNomDat(String dscNomDat) {
		this.dscNomDat = dscNomDat;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public FeiMxRelCatSi getFeiMxRelCatSi() {
		return this.feiMxRelCatSi;
	}

	public void setFeiMxRelCatSi(FeiMxRelCatSi feiMxRelCatSi) {
		this.feiMxRelCatSi = feiMxRelCatSi;
	}

	public FeiMxRelDatCat getFeiMxRelDatCat() {
		return this.feiMxRelDatCat;
	}

	public void setFeiMxRelDatCat(FeiMxRelDatCat feiMxRelDatCat) {
		this.feiMxRelDatCat = feiMxRelDatCat;
	}

	public List<FeiMxRelDatCat> getFeiMxRelDatCats() {
		return new ArrayList<FeiMxRelDatCat>(feiMxRelDatCats);
	}

	public void setFeiMxRelDatCats(List<FeiMxRelDatCat> feiMxRelDatCats) {
		this.feiMxRelDatCats = new ArrayList<FeiMxRelDatCat>(feiMxRelDatCats);
	}

	public FeiMxRelDatCat addFeiMxRelDatCat(FeiMxRelDatCat feiMxRelDatCat) {
		getFeiMxRelDatCats().add(feiMxRelDatCat);
		feiMxRelDatCat.setFeiMxRelDatCat(this);

		return feiMxRelDatCat;
	}

	public FeiMxRelDatCat removeFeiMxRelDatCat(FeiMxRelDatCat feiMxRelDatCat) {
		getFeiMxRelDatCats().remove(feiMxRelDatCat);
		feiMxRelDatCat.setFeiMxRelDatCat(null);

		return feiMxRelDatCat;
	}
}