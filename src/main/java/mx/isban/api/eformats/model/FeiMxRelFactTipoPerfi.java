package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the FEI_MX_REL_FACT_TIPO_PERFI database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_FACT_TIPO_PERFI")
@NamedQuery(name="FeiMxRelFactTipoPerfi.findAll", query="SELECT f FROM FeiMxRelFactTipoPerfi f")
public class FeiMxRelFactTipoPerfi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_REL_FACT_TIPO_PERFI_IDFACTTIPO_GENERATOR", sequenceName="FEI_MX_REL_FACT_TIPO_PERFI_ID_FACT_TIPO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_FACT_TIPO_PERFI_IDFACTTIPO_GENERATOR")
	@Column(name="ID_FACT_TIPO", unique=true, nullable=false, length=10)
	private String idFactTipo;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxPrcCatFact
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FACT", insertable=false, updatable=false)
	private FeiMxPrcCatFact feiMxPrcCatFact;

	//bi-directional many-to-one association to FeiMxPrcCatTipoPerfi
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_PERFI", insertable=false, updatable=false)
	private FeiMxPrcCatTipoPerfi feiMxPrcCatTipoPerfi;

	public FeiMxRelFactTipoPerfi() {
		// default constructor
	}

	public String getIdFactTipo() {
		return this.idFactTipo;
	}

	public void setIdFactTipo(String idFactTipo) {
		this.idFactTipo = idFactTipo;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public FeiMxPrcCatFact getFeiMxPrcCatFact() {
		return this.feiMxPrcCatFact;
	}

	public void setFeiMxPrcCatFact(FeiMxPrcCatFact feiMxPrcCatFact) {
		this.feiMxPrcCatFact = feiMxPrcCatFact;
	}

	public FeiMxPrcCatTipoPerfi getFeiMxPrcCatTipoPerfi() {
		return this.feiMxPrcCatTipoPerfi;
	}

	public void setFeiMxPrcCatTipoPerfi(FeiMxPrcCatTipoPerfi feiMxPrcCatTipoPerfi) {
		this.feiMxPrcCatTipoPerfi = feiMxPrcCatTipoPerfi;
	}

}