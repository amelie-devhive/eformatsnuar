package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SEG_MX_PRC_PEFTO database table.
 * 
 */
@Entity
@Table(name="SEG_MX_PRC_PEFTO")
@NamedQuery(name="SegMxPrcPefto.findAll", query="SELECT s FROM SegMxPrcPefto s")
public class SegMxPrcPefto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SEG_MX_PRC_PEFTO_IDPEFTOPK_GENERATOR", sequenceName="SEG_MX_PRC_PEFTO_ID_PEFTO_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEG_MX_PRC_PEFTO_IDPEFTOPK_GENERATOR")
	@Column(name="ID_PEFTO_PK", unique=true, nullable=false, precision=15)
	private long idPeftoPk;

	@Column(name="COD_CNTR_COST", nullable=false, length=10)
	private String codCntrCost;

	@Column(name="COD_PSTO", nullable=false, length=10)
	private String codPsto;

	@Column(name="FCH_ULT_MOD", nullable=false)
	private Timestamp fchUltMod;

	@Column(name="FLG_ACT", nullable=false, precision=1)
	private BigDecimal flgAct;

	//bi-directional many-to-one association to SegMxPrcApp
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_APP_FK", nullable=false, insertable=false, updatable=false)
	private SegMxPrcApp segMxPrcApp;

	//bi-directional many-to-one association to SegMxPrcMod
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOD_FK", insertable=false, updatable=false)
	private SegMxPrcMod segMxPrcMod;

	//bi-directional many-to-one association to SegMxPrcOper
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_OPER_FK", insertable=false, updatable=false)
	private SegMxPrcOper segMxPrcOper;

	//bi-directional many-to-one association to SegMxPrcPant
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PANT_FK", insertable=false, updatable=false)
	private SegMxPrcPant segMxPrcPant;

	//bi-directional many-to-one association to SegMxPrcPerf
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PERF_FK", nullable=false, insertable=false, updatable=false)
	private SegMxPrcPerf segMxPrcPerf;

	public SegMxPrcPefto() {
	}

	public long getIdPeftoPk() {
		return this.idPeftoPk;
	}

	public void setIdPeftoPk(long idPeftoPk) {
		this.idPeftoPk = idPeftoPk;
	}

	public String getCodCntrCost() {
		return this.codCntrCost;
	}

	public void setCodCntrCost(String codCntrCost) {
		this.codCntrCost = codCntrCost;
	}

	public String getCodPsto() {
		return this.codPsto;
	}

	public void setCodPsto(String codPsto) {
		this.codPsto = codPsto;
	}

	public Timestamp getFchUltMod() {
		if(this.fchUltMod == null) {
			return null;
		}
		return (Timestamp) this.fchUltMod.clone();
	}

	public void setFchUltMod(Timestamp fchUltMod) {
		if(fchUltMod == null){
			this.fchUltMod = null;
		} else {
			this.fchUltMod = (Timestamp) fchUltMod.clone();
		}
	}

	public BigDecimal getFlgAct() {
		return this.flgAct;
	}

	public void setFlgAct(BigDecimal flgAct) {
		this.flgAct = flgAct;
	}

	public SegMxPrcApp getSegMxPrcApp() {
		return this.segMxPrcApp;
	}

	public void setSegMxPrcApp(SegMxPrcApp segMxPrcApp) {
		this.segMxPrcApp = segMxPrcApp;
	}

	public SegMxPrcMod getSegMxPrcMod() {
		return this.segMxPrcMod;
	}

	public void setSegMxPrcMod(SegMxPrcMod segMxPrcMod) {
		this.segMxPrcMod = segMxPrcMod;
	}

	public SegMxPrcOper getSegMxPrcOper() {
		return this.segMxPrcOper;
	}

	public void setSegMxPrcOper(SegMxPrcOper segMxPrcOper) {
		this.segMxPrcOper = segMxPrcOper;
	}

	public SegMxPrcPant getSegMxPrcPant() {
		return this.segMxPrcPant;
	}

	public void setSegMxPrcPant(SegMxPrcPant segMxPrcPant) {
		this.segMxPrcPant = segMxPrcPant;
	}

	public SegMxPrcPerf getSegMxPrcPerf() {
		return this.segMxPrcPerf;
	}

	public void setSegMxPrcPerf(SegMxPrcPerf segMxPrcPerf) {
		this.segMxPrcPerf = segMxPrcPerf;
	}

}