package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.isban.api.eformats.utils.EntityDtoConverter;


/**
 * The persistent class for the CUSTOMER database table.
 * 
 */
@Entity
@Table(name="CUSTOMER")
@NamedQuery(name="Customer.findAll", query="SELECT c FROM Customer c")
public class Customer implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CUSTOMER_ID_GENERATOR", sequenceName="SEQ_FEI_MX_CUS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CUSTOMER_ID_GENERATOR")
	@Column(unique=true, nullable=false, precision=19)
	private long id;

	@Column(name="CREATED_DATE")
	private Date date;

	@Column(length=255)
	private String email;

	@Column(length=255)
	private String name;

	public Customer() {
		//default constructor
	}
	
	public Customer(String name, String email, Date date) {
        this.name = name;
        this.email = email;
        this.date = (Date) date.clone();
    }
	
	@Override
	protected Customer clone() throws CloneNotSupportedException {
		return EntityDtoConverter.convertEntityToDto(this, Customer.class);
	}
	
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		if(this.date == null) {
			return null;
		}
		return (Timestamp) this.date.clone();
	}

	public void setDate(Date createdDate) {
		if(createdDate == null){
			this.date = null;
		} else {
			this.date = (Timestamp) createdDate.clone();
		}
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}