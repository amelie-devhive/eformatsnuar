package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_PRC_CAT_TIPO_PERFI database table.
 * 
 */
@Entity
@Table(name="FEI_MX_PRC_CAT_TIPO_PERFI")
@NamedQuery(name="FeiMxPrcCatTipoPerfi.findAll", query="SELECT f FROM FeiMxPrcCatTipoPerfi f")
public class FeiMxPrcCatTipoPerfi implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String TABLE_NAME = "FEI_MX_PRC_CAT_TIPO_PERFIL";
	public static final String FIND_ACTIVE = "FeiMxPrcCatTipoPerfi.findActive";

	@Id
	@SequenceGenerator(name="FEI_MX_PRC_CAT_TIPO_PERFI_IDTIPOPERFI_GENERATOR", sequenceName="FEI_MX_PRC_CAT_TIPO_PERFI_ID_TIPO_PERFI")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_PRC_CAT_TIPO_PERFI_IDTIPOPERFI_GENERATOR")
	@Column(name="ID_TIPO_PERFI", unique=true, nullable=false, precision=38)
	private long idTipoPerfi;

	@Column(name="DSC_ESTAT_PERFI", length=1)
	private String dscEstatPerfi;

	@Column(name="DSC_NOM_PERFI", length=30)
	private String dscNomPerfi;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxPrcCatPerfi
	@OneToMany(mappedBy="feiMxPrcCatTipoPerfi")
	private List<FeiMxPrcCatPerfi> feiMxPrcCatPerfis;

	//bi-directional many-to-one association to FeiMxRelFactTipoPerfi
	@OneToMany(mappedBy="feiMxPrcCatTipoPerfi")
	private List<FeiMxRelFactTipoPerfi> feiMxRelFactTipoPerfis;

	public FeiMxPrcCatTipoPerfi() {
		// default constructor
	}

	public long getIdTipoPerfi() {
		return this.idTipoPerfi;
	}

	public void setIdTipoPerfi(long idTipoPerfi) {
		this.idTipoPerfi = idTipoPerfi;
	}

	public String getDscEstatPerfi() {
		return this.dscEstatPerfi;
	}

	public void setDscEstatPerfi(String dscEstatPerfi) {
		this.dscEstatPerfi = dscEstatPerfi;
	}

	public String getDscNomPerfi() {
		return this.dscNomPerfi;
	}

	public void setDscNomPerfi(String dscNomPerfi) {
		this.dscNomPerfi = dscNomPerfi;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public List<FeiMxPrcCatPerfi> getFeiMxPrcCatPerfis() {
		if(feiMxPrcCatPerfis == null) {
			return Collections.emptyList();
		}
		List<FeiMxPrcCatPerfi> copy = new ArrayList<>();
		Collections.copy(copy, feiMxPrcCatPerfis);
		return copy;
	}

	public void setFeiMxPrcCatPerfis(List<FeiMxPrcCatPerfi> feiMxPrcCatPerfis) {
		if(feiMxPrcCatPerfis == null) {
			this.feiMxPrcCatPerfis = null;
		}else {
			Collections.copy(this.feiMxPrcCatPerfis, feiMxPrcCatPerfis);
		}
	}
	
	public FeiMxPrcCatPerfi addFeiMxPrcCatPerfi(FeiMxPrcCatPerfi feiMxPrcCatPerfi) {
		getFeiMxPrcCatPerfis().add(feiMxPrcCatPerfi);
		feiMxPrcCatPerfi.setFeiMxPrcCatTipoPerfi(this);

		return feiMxPrcCatPerfi;
	}

	public FeiMxPrcCatPerfi removeFeiMxPrcCatPerfi(FeiMxPrcCatPerfi feiMxPrcCatPerfi) {
		getFeiMxPrcCatPerfis().remove(feiMxPrcCatPerfi);
		feiMxPrcCatPerfi.setFeiMxPrcCatTipoPerfi(null);

		return feiMxPrcCatPerfi;
	}

	public List<FeiMxRelFactTipoPerfi> getFeiMxRelFactTipoPerfis() {
		if(feiMxRelFactTipoPerfis == null) {
			return Collections.emptyList();
		}
		List<FeiMxRelFactTipoPerfi> copy = new ArrayList<>();
		Collections.copy(copy, feiMxRelFactTipoPerfis);
		return copy;
	}

	public void setFeiMxRelFactTipoPerfis(List<FeiMxRelFactTipoPerfi> feiMxRelFactTipoPerfis) {
		if(feiMxRelFactTipoPerfis == null) {
			this.feiMxRelFactTipoPerfis = null;
		}else {
			Collections.copy(this.feiMxRelFactTipoPerfis, feiMxRelFactTipoPerfis);
		}
	}
	
	public FeiMxRelFactTipoPerfi addFeiMxRelFactTipoPerfi(FeiMxRelFactTipoPerfi feiMxRelFactTipoPerfi) {
		getFeiMxRelFactTipoPerfis().add(feiMxRelFactTipoPerfi);
		feiMxRelFactTipoPerfi.setFeiMxPrcCatTipoPerfi(this);

		return feiMxRelFactTipoPerfi;
	}

	public FeiMxRelFactTipoPerfi removeFeiMxRelFactTipoPerfi(FeiMxRelFactTipoPerfi feiMxRelFactTipoPerfi) {
		getFeiMxRelFactTipoPerfis().remove(feiMxRelFactTipoPerfi);
		feiMxRelFactTipoPerfi.setFeiMxPrcCatTipoPerfi(null);

		return feiMxRelFactTipoPerfi;
	}

}