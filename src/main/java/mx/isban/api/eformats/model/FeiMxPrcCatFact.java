package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_PRC_CAT_FACT database table.
 * 
 */
@Entity
@Table(name="FEI_MX_PRC_CAT_FACT")
@NamedQuery(name="FeiMxPrcCatFact.findAll", query="SELECT f FROM FeiMxPrcCatFact f")
public class FeiMxPrcCatFact implements Serializable, Cloneable {
	private static final long serialVersionUID = 5017768919514158386L;

	public static final String FIND_BY_NUM_FAC = "FeiMxPrcCatFact.findByNumFac";
	public static final String FIND_BY_NUM_FAC_CVE_FAC = "FeiMxPrcCatFact.findByNumFacCveFac";
	public static final String FIND_BY_ID_PERFIL = "FeiMxPrcCatFact.findByIdPerfil";
	public static final String FIND_EXTRAORDINARIAS = "FeiMxPrcCatFact.findExtraordinarias";

	@Id
	@SequenceGenerator(name="FEI_MX_PRC_CAT_FACT_IDFACT_GENERATOR", sequenceName="FEI_MX_PRC_CAT_FACT_ID_FACT")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_PRC_CAT_FACT_IDFACT_GENERATOR")
	@Column(name="ID_FACT", unique=true, nullable=false, precision=38)
	private long idFact;

	@Column(name="DSC_ESTAT_FACT", length=1)
	private String dscEstatFact;

	@Column(name="DSC_FACT", length=250)
	private String dscFact;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="NUM_FACT", length=4)
	private String numFact;

	@Column(name="USR_ID", length=10)
	private String usrId;

	@Column(name="VAL_CLAVE_FACT", length=25)
	private String valClaveFact;

	//bi-directional many-to-one association to FeiMxRelFactPerfi
	@OneToMany(mappedBy="feiMxPrcCatFact")
	private List<FeiMxRelFactPerfi> feiMxRelFactPerfis;

	//bi-directional many-to-one association to FeiMxRelFactTipoPerfi
	@OneToMany(mappedBy="feiMxPrcCatFact")
	private List<FeiMxRelFactTipoPerfi> feiMxRelFactTipoPerfis;

	public FeiMxPrcCatFact() {
		// default constructor
	}

	public long getIdFact() {
		return this.idFact;
	}

	public void setIdFact(long idFact) {
		this.idFact = idFact;
	}

	public String getDscEstatFact() {
		return this.dscEstatFact;
	}

	public void setDscEstatFact(String dscEstatFact) {
		this.dscEstatFact = dscEstatFact;
	}

	public String getDscFact() {
		return this.dscFact;
	}

	public void setDscFact(String dscFact) {
		this.dscFact = dscFact;
	}

	public Timestamp getFchHorUltAct() {
		if(fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null) {
			this.fchHorUltAct = null;
		}else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getNumFact() {
		return this.numFact;
	}

	public void setNumFact(String numFact) {
		this.numFact = numFact;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getValClaveFact() {
		return this.valClaveFact;
	}

	public void setValClaveFact(String valClaveFact) {
		this.valClaveFact = valClaveFact;
	}

	public List<FeiMxRelFactPerfi> getFeiMxRelFactPerfis() {
		return new ArrayList<FeiMxRelFactPerfi>(feiMxRelFactPerfis);
	}

	public void setFeiMxRelFactPerfis(List<FeiMxRelFactPerfi> feiMxRelFactPerfis) {
		this.feiMxRelFactPerfis = new ArrayList<FeiMxRelFactPerfi>(feiMxRelFactPerfis);
	}

	public FeiMxRelFactPerfi addFeiMxRelFactPerfi(FeiMxRelFactPerfi feiMxRelFactPerfi) {
		getFeiMxRelFactPerfis().add(feiMxRelFactPerfi);
		feiMxRelFactPerfi.setFeiMxPrcCatFact(this);

		return feiMxRelFactPerfi;
	}

	public FeiMxRelFactPerfi removeFeiMxRelFactPerfi(FeiMxRelFactPerfi feiMxRelFactPerfi) {
		getFeiMxRelFactPerfis().remove(feiMxRelFactPerfi);
		feiMxRelFactPerfi.setFeiMxPrcCatFact(null);

		return feiMxRelFactPerfi;
	}

	public List<FeiMxRelFactTipoPerfi> getFeiMxRelFactTipoPerfis() {
		return new ArrayList<FeiMxRelFactTipoPerfi>(feiMxRelFactTipoPerfis);
	}

	public void setFeiMxRelFactTipoPerfis(List<FeiMxRelFactTipoPerfi> feiMxRelFactTipoPerfis) {
		this.feiMxRelFactTipoPerfis = new ArrayList<FeiMxRelFactTipoPerfi>(feiMxRelFactTipoPerfis);
	}

	public FeiMxRelFactTipoPerfi addFeiMxRelFactTipoPerfi(FeiMxRelFactTipoPerfi feiMxRelFactTipoPerfi) {
		getFeiMxRelFactTipoPerfis().add(feiMxRelFactTipoPerfi);
		feiMxRelFactTipoPerfi.setFeiMxPrcCatFact(this);

		return feiMxRelFactTipoPerfi;
	}

	public FeiMxRelFactTipoPerfi removeFeiMxRelFactTipoPerfi(FeiMxRelFactTipoPerfi feiMxRelFactTipoPerfi) {
		getFeiMxRelFactTipoPerfis().remove(feiMxRelFactTipoPerfi);
		feiMxRelFactTipoPerfi.setFeiMxPrcCatFact(null);

		return feiMxRelFactTipoPerfi;
	}
	
}