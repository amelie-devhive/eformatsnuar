package mx.isban.api.eformats.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FeiMxRelActPk implements Serializable{ 
	private static final long serialVersionUID = -4050645714055914418L;
	
	@Column(name = "ID_ACT_RFC")
	private long idActRfc;
	@Column(name = "ID_ACT_SPEC")
	private long idActSpec;
	
	public long getIdActRfc() {
		return this.idActRfc;
	}

	public void setIdActRfc(long idActRfc) {
		this.idActRfc = idActRfc;
	}

	public long getIdActSpec() {
		return idActSpec;
	}

	public void setIdActSpec(long idActSpec) {
		this.idActSpec = idActSpec;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idActRfc ^ (idActRfc >>> 32));
		result = prime * result + (int) (idActSpec ^ (idActSpec >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeiMxRelActPk other = (FeiMxRelActPk) obj;
		if (idActRfc != other.idActRfc)
			return false;
		if (idActSpec != other.idActSpec)
			return false;
		return true;
	}
	
	
}
