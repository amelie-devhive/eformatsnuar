package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_REL_FMT_MAS_UTIL database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_FMT_MAS_UTIL")
@NamedQuery(name="FeiMxRelFmtMasUtil.findAll", query="SELECT f FROM FeiMxRelFmtMasUtil f")
public class FeiMxRelFmtMasUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_REL_FMT_MAS_UTIL_IDMASUTIL_GENERATOR", sequenceName="SEQ_FEI_MX_MAS_UTIL")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_FMT_MAS_UTIL_IDMASUTIL_GENERATOR")
	@Column(name="ID_MAS_UTIL", unique=true, nullable=false, precision=10)
	private long idMasUtil;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="ID_FMT", precision=38)
	private BigDecimal idFmt;

	@Column(name="USR_ID", length=10)
	private String usrId;

	@Column(name="VAL_CONT", precision=38)
	private BigDecimal valCont;

	@Column(name="VAL_NUM_EJE", length=10)
	private String valNumEje;

	public FeiMxRelFmtMasUtil() {
	}

	public long getIdMasUtil() {
		return this.idMasUtil;
	}

	public void setIdMasUtil(long idMasUtil) {
		this.idMasUtil = idMasUtil;
	}

	public Timestamp getFchHorUltAct() {
		if(fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null) {
			this.fchHorUltAct = null;
		}else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public BigDecimal getIdFmt() {
		return this.idFmt;
	}

	public void setIdFmt(BigDecimal idFmt) {
		this.idFmt = idFmt;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public BigDecimal getValCont() {
		return this.valCont;
	}

	public void setValCont(BigDecimal valCont) {
		this.valCont = valCont;
	}

	public String getValNumEje() {
		return this.valNumEje;
	}

	public void setValNumEje(String valNumEje) {
		this.valNumEje = valNumEje;
	}

}