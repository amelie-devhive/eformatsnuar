package mx.isban.api.eformats.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SEG_MX_AUX_STS_APR database table.
 * 
 */
@Entity
@Table(name="SEG_MX_AUX_STS_APR")
@NamedQuery(name="SegMxAuxStsApr.findAll", query="SELECT s FROM SegMxAuxStsApr s")
public class SegMxAuxStsApr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SEG_MX_AUX_STS_APR_IDESTATPK_GENERATOR", sequenceName="SEG_MX_AUX_STS_APR_ID_ESTAT_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEG_MX_AUX_STS_APR_IDESTATPK_GENERATOR")
	@Column(name="ID_ESTAT_PK", unique=true, nullable=false, precision=1)
	private long idEstatPk;

	@Column(name="DSC_ESTAT", nullable=false, length=32)
	private String dscEstat;

	public SegMxAuxStsApr() {
	}

	public long getIdEstatPk() {
		return this.idEstatPk;
	}

	public void setIdEstatPk(long idEstatPk) {
		this.idEstatPk = idEstatPk;
	}

	public String getDscEstat() {
		return this.dscEstat;
	}

	public void setDscEstat(String dscEstat) {
		this.dscEstat = dscEstat;
	}

}