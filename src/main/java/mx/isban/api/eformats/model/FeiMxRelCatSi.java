package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_REL_CAT_SISTEMA database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_CAT_SIS")
@NamedQuery(name="FeiMxRelCatSi.findAll", query="SELECT f FROM FeiMxRelCatSi f")
public class FeiMxRelCatSi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_REL_CAT_SIS_IDCAT_GENERATOR", sequenceName="FEI_MX_REL_CAT_SIS_ID_CAT")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_CAT_SIS_IDCAT_GENERATOR")
	@Column(name="ID_CAT", unique=true, nullable=false, precision=38)
	private long idCat;

	@Column(name="DSC_CAT", length=50)
	private String dscCat;

	@Column(name="DSC_ESTAT_CAT", length=1)
	private String dscEstatCat;

	@Column(name="DSC_NOM_CAT", length=100)
	private String dscNomCat;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	@Column(name="VAL_CLAVE_CAT", length=50)
	private String valClaveCat;

	//bi-directional many-to-one association to FeiMxRelDatCat
	@OneToMany(mappedBy="feiMxRelCatSi")
	private List<FeiMxRelDatCat> feiMxRelDatCats;

	public FeiMxRelCatSi() {
		// default constructor
	}

	public long getIdCat() {
		return this.idCat;
	}

	public void setIdCat(long idCat) {
		this.idCat = idCat;
	}

	public String getDscCat() {
		return this.dscCat;
	}

	public void setDscCat(String dscCat) {
		this.dscCat = dscCat;
	}

	public String getDscEstatCat() {
		return this.dscEstatCat;
	}

	public void setDscEstatCat(String dscEstatCat) {
		this.dscEstatCat = dscEstatCat;
	}

	public String getDscNomCat() {
		return this.dscNomCat;
	}

	public void setDscNomCat(String dscNomCat) {
		this.dscNomCat = dscNomCat;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getValClaveCat() {
		return this.valClaveCat;
	}

	public void setValClaveCat(String valClaveCat) {
		this.valClaveCat = valClaveCat;
	}

	public List<FeiMxRelDatCat> getFeiMxRelDatCats() {
		return new ArrayList<FeiMxRelDatCat>(feiMxRelDatCats);
	}

	public void setFeiMxRelDatCats(List<FeiMxRelDatCat> feiMxRelDatCats) {
		this.feiMxRelDatCats = new ArrayList<FeiMxRelDatCat>(feiMxRelDatCats);
	}

	public FeiMxRelDatCat addFeiMxRelDatCat(FeiMxRelDatCat feiMxRelDatCat) {
		getFeiMxRelDatCats().add(feiMxRelDatCat);
		feiMxRelDatCat.setFeiMxRelCatSi(this);

		return feiMxRelDatCat;
	}

	public FeiMxRelDatCat removeFeiMxRelDatCat(FeiMxRelDatCat feiMxRelDatCat) {
		getFeiMxRelDatCats().remove(feiMxRelDatCat);
		feiMxRelDatCat.setFeiMxRelCatSi(null);

		return feiMxRelDatCat;
	}

}