package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the FEI_MX_REL_CAT_PAQ database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_CAT_PAQ")
@NamedQuery(name="FeiMxRelCatPaq.findAll", query="SELECT f FROM FeiMxRelCatPaq f")
public class FeiMxRelCatPaq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_REL_CAT_PAQ_IDPAQ_GENERATOR", sequenceName="FEI_MX_REL_CAT_PAQ_ID_PAQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_CAT_PAQ_IDPAQ_GENERATOR")
	@Column(name="ID_PAQ", unique=true, nullable=false, precision=38)
	private long idPaq;

	@Column(name="DSC_ESTAT_PAQ", length=1)
	private String dscEstatPaq;

	@Column(name="DSC_NOM_PAQ", length=50)
	private String dscNomPaq;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxRelFmtPaq
	@OneToMany(mappedBy="feiMxRelCatPaq")
	private List<FeiMxRelFmtPaq> feiMxRelFmtPaqs;

	public FeiMxRelCatPaq() {
		// default constructor
	}

	public long getIdPaq() {
		return this.idPaq;
	}

	public void setIdPaq(long idPaq) {
		this.idPaq = idPaq;
	}

	public String getDscEstatPaq() {
		return this.dscEstatPaq;
	}

	public void setDscEstatPaq(String dscEstatPaq) {
		this.dscEstatPaq = dscEstatPaq;
	}

	public String getDscNomPaq() {
		return this.dscNomPaq;
	}

	public void setDscNomPaq(String dscNomPaq) {
		this.dscNomPaq = dscNomPaq;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public List<FeiMxRelFmtPaq> getFeiMxRelFmtPaqs() {
		if(feiMxRelFmtPaqs == null) {
			return Collections.emptyList();
		}
		List<FeiMxRelFmtPaq> copy = new ArrayList<>();
		Collections.copy(copy, feiMxRelFmtPaqs);
		return copy;
	}

	public void setFeiMxRelFmtPaqs(List<FeiMxRelFmtPaq> feiMxRelFmtPaqs) {
		if(feiMxRelFmtPaqs == null) {
			this.feiMxRelFmtPaqs = null;
		}else {
			Collections.copy(this.feiMxRelFmtPaqs, feiMxRelFmtPaqs);
		}
	}

	public FeiMxRelFmtPaq addFeiMxRelFmtPaq(FeiMxRelFmtPaq feiMxRelFmtPaq) {
		getFeiMxRelFmtPaqs().add(feiMxRelFmtPaq);
		feiMxRelFmtPaq.setFeiMxRelCatPaq(this);

		return feiMxRelFmtPaq;
	}

	public FeiMxRelFmtPaq removeFeiMxRelFmtPaq(FeiMxRelFmtPaq feiMxRelFmtPaq) {
		getFeiMxRelFmtPaqs().remove(feiMxRelFmtPaq);
		feiMxRelFmtPaq.setFeiMxRelCatPaq(null);

		return feiMxRelFmtPaq;
	}

}