package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the FEI_MX_REL_FMT_PEN database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_FMT_PEN")
@NamedQuery(name="FeiMxRelFmtPen.findAll", query="SELECT f FROM FeiMxRelFmtPen f")
public class FeiMxRelFmtPen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_REL_FMT_PEN_IDPEN_GENERATOR", sequenceName="FEI_MX_REL_FMT_PEN_ID_PEN")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_FMT_PEN_IDPEN_GENERATOR")
	@Column(name="ID_PEN", unique=true, nullable=false, precision=10)
	private long idPen;

	@Lob
	@Column(name="DSC_OBJ_DAT_CAP")
	private String dscObjDatCap;

	@Column(name="FCH_HOR_REGIS")
	private Timestamp fchHorRegis;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="NUM_ASO", precision=38)
	private BigDecimal numAso;

	@Column(name="NUM_EJE", length=10)
	private String numEje;

	@Column(name="NUM_SUC", length=10)
	private String numSuc;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxMaeCatFmt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FMT", insertable=false, updatable=false)
	private FeiMxMaeCatFmt feiMxMaeCatFmt;

	public FeiMxRelFmtPen() {
		// default constructor
	}

	public long getIdPen() {
		return this.idPen;
	}

	public void setIdPen(long idPen) {
		this.idPen = idPen;
	}

	public String getDscObjDatCap() {
		return this.dscObjDatCap;
	}

	public void setDscObjDatCap(String dscObjDatCap) {
		this.dscObjDatCap = dscObjDatCap;
	}

	public Timestamp getFchHorRegis() {
		if(fchHorRegis == null) {
			return null;
		}
		return (Timestamp) this.fchHorRegis.clone();
	}

	public void setFchHorRegis(Timestamp fchHorRegis) {
		if(fchHorRegis == null) {
			this.fchHorRegis = null;
		}else {
			this.fchHorRegis = (Timestamp) fchHorRegis.clone();
		}
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public BigDecimal getNumAso() {
		return this.numAso;
	}

	public void setNumAso(BigDecimal numAso) {
		this.numAso = numAso;
	}

	public String getNumEje() {
		return this.numEje;
	}

	public void setNumEje(String numEje) {
		this.numEje = numEje;
	}

	public String getNumSuc() {
		return this.numSuc;
	}

	public void setNumSuc(String numSuc) {
		this.numSuc = numSuc;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public FeiMxMaeCatFmt getFeiMxMaeCatFmt() {
		return this.feiMxMaeCatFmt;
	}

	public void setFeiMxMaeCatFmt(FeiMxMaeCatFmt feiMxMaeCatFmt) {
		this.feiMxMaeCatFmt = feiMxMaeCatFmt;
	}

}