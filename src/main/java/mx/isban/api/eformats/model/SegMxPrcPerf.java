package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SEG_MX_PRC_PERF database table.
 * 
 */
@Entity
@Table(name="SEG_MX_PRC_PERF")
@NamedQuery(name="SegMxPrcPerf.findAll", query="SELECT s FROM SegMxPrcPerf s")
public class SegMxPrcPerf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SEG_MX_PRC_PERF_IDPERFPK_GENERATOR", sequenceName="SEG_MX_PRC_PERF_ID_PERF_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEG_MX_PRC_PERF_IDPERFPK_GENERATOR")
	@Column(name="ID_PERF_PK", unique=true, nullable=false, precision=4)
	private long idPerfPk;

	@Column(name="DSC_DESC", length=500)
	private String dscDesc;

	@Column(name="FCH_ULT_MOD", nullable=false)
	private Timestamp fchUltMod;

	@Column(name="FLG_ACT", nullable=false, precision=1)
	private BigDecimal flgAct;

	@Column(name="TXT_CVE", nullable=false, length=60)
	private String txtCve;

	@Column(name="TXT_NOMB", nullable=false, length=250)
	private String txtNomb;

	//bi-directional many-to-one association to SegMxPrcPefto
	@OneToMany(mappedBy="segMxPrcPerf")
	private List<SegMxPrcPefto> segMxPrcPeftos;

	public SegMxPrcPerf() {
	}

	public long getIdPerfPk() {
		return this.idPerfPk;
	}

	public void setIdPerfPk(long idPerfPk) {
		this.idPerfPk = idPerfPk;
	}

	public String getDscDesc() {
		return this.dscDesc;
	}

	public void setDscDesc(String dscDesc) {
		this.dscDesc = dscDesc;
	}

	public Timestamp getFchUltMod() {
		if(this.fchUltMod == null) {
			return null;
		}
		return (Timestamp) this.fchUltMod.clone();
	}

	public void setFchUltMod(Timestamp fchUltMod) {
		if(fchUltMod == null){
			this.fchUltMod = null;
		} else {
			this.fchUltMod = (Timestamp) fchUltMod.clone();
		}
	}

	public BigDecimal getFlgAct() {
		return this.flgAct;
	}

	public void setFlgAct(BigDecimal flgAct) {
		this.flgAct = flgAct;
	}

	public String getTxtCve() {
		return this.txtCve;
	}

	public void setTxtCve(String txtCve) {
		this.txtCve = txtCve;
	}

	public String getTxtNomb() {
		return this.txtNomb;
	}

	public void setTxtNomb(String txtNomb) {
		this.txtNomb = txtNomb;
	}

	public List<SegMxPrcPefto> getSegMxPrcPeftos() {
		return new ArrayList<SegMxPrcPefto>(segMxPrcPeftos);
	}

	public void setSegMxPrcPeftos(List<SegMxPrcPefto> segMxPrcPeftos) {
		this.segMxPrcPeftos = new ArrayList<SegMxPrcPefto>(segMxPrcPeftos);
	}

	public SegMxPrcPefto addSegMxPrcPefto(SegMxPrcPefto segMxPrcPefto) {
		getSegMxPrcPeftos().add(segMxPrcPefto);
		segMxPrcPefto.setSegMxPrcPerf(this);

		return segMxPrcPefto;
	}

	public SegMxPrcPefto removeSegMxPrcPefto(SegMxPrcPefto segMxPrcPefto) {
		getSegMxPrcPeftos().remove(segMxPrcPefto);
		segMxPrcPefto.setSegMxPrcPerf(null);

		return segMxPrcPefto;
	}

}