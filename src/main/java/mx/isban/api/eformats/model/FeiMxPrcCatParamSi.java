package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_PRC_CAT_PARAM_SIS database table.
 * 
 */
@Entity
@Table(name="FEI_MX_PRC_CAT_PARAM_SIS")
@NamedQuery(name="FeiMxPrcCatParamSi.findAll", query="SELECT f FROM FeiMxPrcCatParamSi f")
public class FeiMxPrcCatParamSi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_PRC_CAT_PARAM_SIS_IDPARAM_GENERATOR", sequenceName="FEI_MX_PRC_CAT_PARAM_SIS_ID_PARAM")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_PRC_CAT_PARAM_SIS_IDPARAM_GENERATOR")
	@Column(name="ID_PARAM", unique=true, nullable=false, precision=38)
	private long idParam;

	@Column(name="DSC_PARAM", length=20)
	private String dscParam;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	@Column(name="VAL_PARAM", length=10)
	private String valParam;

	public FeiMxPrcCatParamSi() {
		// default constructor
	}

	public long getIdParam() {
		return this.idParam;
	}

	public void setIdParam(long idParam) {
		this.idParam = idParam;
	}

	public String getDscParam() {
		return this.dscParam;
	}

	public void setDscParam(String dscParam) {
		this.dscParam = dscParam;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getValParam() {
		return this.valParam;
	}

	public void setValParam(String valParam) {
		this.valParam = valParam;
	}

}