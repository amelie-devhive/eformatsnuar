package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_PRC_CAT_PERFI database table.
 * 
 */
@Entity
@Table(name="FEI_MX_PRC_CAT_PERFI")
@NamedQuery(name="FeiMxPrcCatPerfi.findAll", query="SELECT f FROM FeiMxPrcCatPerfi f")
public class FeiMxPrcCatPerfi implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String TABLE_NAME = "FEI_MX_PRC_CAT_PERFIL";
	public static final String FIND_BY_TIPO_PERFIL = "FeiMxPrcCatPerfi.findByTipoPerfil";

	@Id
	@SequenceGenerator(name="FEI_MX_PRC_CAT_PERFI_IDPERFI_GENERATOR", sequenceName="FEI_MX_PRC_CAT_PERFI_ID_PERFI")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_PRC_CAT_PERFI_IDPERFI_GENERATOR")
	@Column(name="ID_PERFI", unique=true, nullable=false, precision=38)
	private long idPerfi;

	@Column(name="DSC_ESTAT_PERFI", length=1)
	private String dscEstatPerfi;

	@Column(name="DSC_PERFI", length=250)
	private String dscPerfi;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	@Column(name="VAL_CLAVE_PERFI", length=15)
	private String valClavePerfi;

	//bi-directional many-to-one association to FeiMxPrcCatTipoPerfi
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_PERFI", insertable=false, updatable=false)
	private FeiMxPrcCatTipoPerfi feiMxPrcCatTipoPerfi;

	//bi-directional many-to-one association to FeiMxRelFactPerfi
	@OneToMany(mappedBy="feiMxPrcCatPerfi")
	private List<FeiMxRelFactPerfi> feiMxRelFactPerfis;

	public FeiMxPrcCatPerfi() {
		// default constructor
	}

	public long getIdPerfi() {
		return this.idPerfi;
	}

	public void setIdPerfi(long idPerfi) {
		this.idPerfi = idPerfi;
	}

	public String getDscEstatPerfi() {
		return this.dscEstatPerfi;
	}

	public void setDscEstatPerfi(String dscEstatPerfi) {
		this.dscEstatPerfi = dscEstatPerfi;
	}

	public String getDscPerfi() {
		return this.dscPerfi;
	}

	public void setDscPerfi(String dscPerfi) {
		this.dscPerfi = dscPerfi;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getValClavePerfi() {
		return this.valClavePerfi;
	}

	public void setValClavePerfi(String valClavePerfi) {
		this.valClavePerfi = valClavePerfi;
	}

	public FeiMxPrcCatTipoPerfi getFeiMxPrcCatTipoPerfi() {
		return this.feiMxPrcCatTipoPerfi;
	}

	public void setFeiMxPrcCatTipoPerfi(FeiMxPrcCatTipoPerfi feiMxPrcCatTipoPerfi) {
		this.feiMxPrcCatTipoPerfi = feiMxPrcCatTipoPerfi;
	}

	public List<FeiMxRelFactPerfi> getFeiMxRelFactPerfis() {
		if(feiMxRelFactPerfis == null) {
			return Collections.emptyList();
		}
		List<FeiMxRelFactPerfi> copy = new ArrayList<>();
		Collections.copy(copy, feiMxRelFactPerfis);
		return copy;
	}

	public void setFeiMxRelFactPerfis(List<FeiMxRelFactPerfi> feiMxRelFactPerfis) {
		if(feiMxRelFactPerfis == null) {
			this.feiMxRelFactPerfis = null;
		}else {
			Collections.copy(this.feiMxRelFactPerfis, feiMxRelFactPerfis);
		}
	}

	public FeiMxRelFactPerfi addFeiMxRelFactPerfi(FeiMxRelFactPerfi feiMxRelFactPerfi) {
		getFeiMxRelFactPerfis().add(feiMxRelFactPerfi);
		feiMxRelFactPerfi.setFeiMxPrcCatPerfi(this);

		return feiMxRelFactPerfi;
	}

	public FeiMxRelFactPerfi removeFeiMxRelFactPerfi(FeiMxRelFactPerfi feiMxRelFactPerfi) {
		getFeiMxRelFactPerfis().remove(feiMxRelFactPerfi);
		feiMxRelFactPerfi.setFeiMxPrcCatPerfi(null);

		return feiMxRelFactPerfi;
	}
}