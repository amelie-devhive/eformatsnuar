package mx.isban.api.eformats.model;


import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



/**
 * The persistent class for the FEI_MX_REL_ACT database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_ACT")
@NamedQuery(name="FeiMxRelAct.findAll", query="SELECT f FROM FeiMxRelAct f")
public class FeiMxRelAct implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private FeiMxRelActPk id;
	
	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxMaeActRfc
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACT_RFC", insertable=false, updatable=false)
	private FeiMxMaeActRfc feiMxMaeActRfc;

	//bi-directional many-to-one association to FeiMxMaeActSpecRfc
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACT_SPEC", insertable=false, updatable=false)
	private FeiMxMaeActSpecRfc feiMxMaeActSpecRfc;

	public FeiMxRelAct() {
		// default constructor
	}

	public Timestamp getFchHorUltAct() {
		if(fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null) {
			this.fchHorUltAct = null;
		}else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public FeiMxMaeActRfc getFeiMxMaeActRfc() {
		return this.feiMxMaeActRfc;
	}

	public void setFeiMxMaeActRfc(FeiMxMaeActRfc feiMxMaeActRfc) {
		this.feiMxMaeActRfc = feiMxMaeActRfc;
	}

	public FeiMxMaeActSpecRfc getFeiMxMaeActSpecRfc() {
		return this.feiMxMaeActSpecRfc;
	}

	public void setFeiMxMaeActSpecRfc(FeiMxMaeActSpecRfc feiMxMaeActSpecRfc) {
		this.feiMxMaeActSpecRfc = feiMxMaeActSpecRfc;
	}

	public FeiMxRelActPk getId() {
		return id;
	}

	public void setId(FeiMxRelActPk id) {
		this.id = id;
	}
}
