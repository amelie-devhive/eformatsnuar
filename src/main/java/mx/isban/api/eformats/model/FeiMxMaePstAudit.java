package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the FEI_MX_MAE_PST_AUDIT database table.
 * 
 */
@Entity
@Table(name="FEI_MX_MAE_PST_AUDIT")
@NamedQuery(name="FeiMxMaePstAudit.findAll", query="SELECT f FROM FeiMxMaePstAudit f")
public class FeiMxMaePstAudit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_MAE_PST_AUDIT_IDTRANS_GENERATOR", sequenceName="FEI_MX_MAE_PST_AUDIT_ID_TRANS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_MAE_PST_AUDIT_IDTRANS_GENERATOR")
	@Column(name="ID_TRANS", unique=true, nullable=false, length=10)
	private String idTrans;

	@Column(name="DSC_DESTI_OPE", length=10)
	private String dscDestiOpe;

	@Column(name="DSC_ESTAT_OPE", length=10)
	private String dscEstatOpe;

	@Column(name="DSC_HOST_NAME_SER_WEB", length=10)
	private String dscHostNameSerWeb;

	@Column(name="DSC_NOM_APLIC", length=10)
	private String dscNomAplic;

	@Column(name="DSC_NOM_ARCH", length=10)
	private String dscNomArch;

	@Column(name="DSC_NOM_TRANS", length=10)
	private String dscNomTrans;

	@Temporal(TemporalType.DATE)
	@Column(name="FCH_APLIC")
	private Date fchAplic;

	@Temporal(TemporalType.DATE)
	@Column(name="FCH_AUDIT")
	private Date fchAudit;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Temporal(TemporalType.DATE)
	@Column(name="FCH_PROG")
	private Date fchProg;

	@Column(name="HOR_AUDIT")
	private Timestamp horAudit;

	@Column(name="ID_INSTA_WEB", length=10)
	private String idInstaWeb;

	@Column(name="ID_SESION", length=10)
	private String idSesion;

	@Column(name="ID_USR", length=10)
	private String idUsr;

	@Column(name="ID_USR_AFECT", length=10)
	private String idUsrAfect;

	@Column(name="NUM_CONTR", length=10)
	private String numContr;

	@Column(name="NUM_CTA_DESTI", length=10)
	private String numCtaDesti;

	@Column(name="NUM_CTA_ORI", length=10)
	private String numCtaOri;

	@Column(name="NUM_TIPO_CAM", length=10)
	private String numTipoCam;

	@Column(name="USR_ID", length=10)
	private String usrId;

	@Column(name="VAL_COD_OPE", length=10)
	private String valCodOpe;

	@Column(name="VAL_DIR_IP", length=10)
	private String valDirIp;

	@Column(name="VAL_ID_OPE", length=10)
	private String valIdOpe;

	@Column(name="VAL_MONTO_TOTAL", length=10)
	private String valMontoTotal;

	public FeiMxMaePstAudit() {
		//default constructor
	}

	public String getIdTrans() {
		return this.idTrans;
	}

	public void setIdTrans(String idTrans) {
		this.idTrans = idTrans;
	}

	public String getDscDestiOpe() {
		return this.dscDestiOpe;
	}

	public void setDscDestiOpe(String dscDestiOpe) {
		this.dscDestiOpe = dscDestiOpe;
	}

	public String getDscEstatOpe() {
		return this.dscEstatOpe;
	}

	public void setDscEstatOpe(String dscEstatOpe) {
		this.dscEstatOpe = dscEstatOpe;
	}

	public String getDscHostNameSerWeb() {
		return this.dscHostNameSerWeb;
	}

	public void setDscHostNameSerWeb(String dscHostNameSerWeb) {
		this.dscHostNameSerWeb = dscHostNameSerWeb;
	}

	public String getDscNomAplic() {
		return this.dscNomAplic;
	}

	public void setDscNomAplic(String dscNomAplic) {
		this.dscNomAplic = dscNomAplic;
	}

	public String getDscNomArch() {
		return this.dscNomArch;
	}

	public void setDscNomArch(String dscNomArch) {
		this.dscNomArch = dscNomArch;
	}

	public String getDscNomTrans() {
		return this.dscNomTrans;
	}

	public void setDscNomTrans(String dscNomTrans) {
		this.dscNomTrans = dscNomTrans;
	}

	public Date getFchAplic() {
		if(this.fchAplic == null) {
			return null;
		}
		return (Date) this.fchAplic.clone();
	}

	public void setFchAplic(Date fchAplic) {
		if(fchAplic == null) {
			this.fchAplic = null;
		}else {
			this.fchAplic = (Date) fchAplic.clone();
		}
	}

	public Date getFchAudit() {
		if(this.fchAudit == null) {
			return null;
		}
		return (Date) this.fchAudit.clone();
	}

	public void setFchAudit(Date fchAudit) {
		if(fchAudit == null) {
			this.fchAudit = null;
		}else {
			this.fchAudit = (Date) fchAudit.clone();
		}
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null) {
			this.fchHorUltAct = null;
		}else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public Date getFchProg() {
		if(this.fchProg == null) {
			return null;
		}
		return (Date) this.fchProg.clone();
	}

	public void setFchProg(Date fchProg) {
		if(fchProg == null) {
			this.fchProg = null;
		}else {
			this.fchProg = (Date) fchProg.clone();
		}
	}

	public Timestamp getHorAudit() {
		if(this.horAudit == null) {
			return null;
		}
		return (Timestamp) this.horAudit.clone();
	}

	public void setHorAudit(Timestamp horAudit) {
		if(horAudit == null) {
			this.horAudit = null;
		}else {
			this.horAudit = (Timestamp) horAudit.clone();
		}
	}

	public String getIdInstaWeb() {
		return this.idInstaWeb;
	}

	public void setIdInstaWeb(String idInstaWeb) {
		this.idInstaWeb = idInstaWeb;
	}

	public String getIdSesion() {
		return this.idSesion;
	}

	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}

	public String getIdUsr() {
		return this.idUsr;
	}

	public void setIdUsr(String idUsr) {
		this.idUsr = idUsr;
	}

	public String getIdUsrAfect() {
		return this.idUsrAfect;
	}

	public void setIdUsrAfect(String idUsrAfect) {
		this.idUsrAfect = idUsrAfect;
	}

	public String getNumContr() {
		return this.numContr;
	}

	public void setNumContr(String numContr) {
		this.numContr = numContr;
	}

	public String getNumCtaDesti() {
		return this.numCtaDesti;
	}

	public void setNumCtaDesti(String numCtaDesti) {
		this.numCtaDesti = numCtaDesti;
	}

	public String getNumCtaOri() {
		return this.numCtaOri;
	}

	public void setNumCtaOri(String numCtaOri) {
		this.numCtaOri = numCtaOri;
	}

	public String getNumTipoCam() {
		return this.numTipoCam;
	}

	public void setNumTipoCam(String numTipoCam) {
		this.numTipoCam = numTipoCam;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getValCodOpe() {
		return this.valCodOpe;
	}

	public void setValCodOpe(String valCodOpe) {
		this.valCodOpe = valCodOpe;
	}

	public String getValDirIp() {
		return this.valDirIp;
	}

	public void setValDirIp(String valDirIp) {
		this.valDirIp = valDirIp;
	}

	public String getValIdOpe() {
		return this.valIdOpe;
	}

	public void setValIdOpe(String valIdOpe) {
		this.valIdOpe = valIdOpe;
	}

	public String getValMontoTotal() {
		return this.valMontoTotal;
	}

	public void setValMontoTotal(String valMontoTotal) {
		this.valMontoTotal = valMontoTotal;
	}

}