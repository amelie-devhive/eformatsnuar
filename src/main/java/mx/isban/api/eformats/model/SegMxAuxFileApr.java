package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SEG_MX_AUX_FILE_APR database table.
 * 
 */
@Entity
@Table(name="SEG_MX_AUX_FILE_APR")
@NamedQuery(name="SegMxAuxFileApr.findAll", query="SELECT s FROM SegMxAuxFileApr s")
public class SegMxAuxFileApr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SEG_MX_AUX_FILE_APR_IDAPRTOPK_GENERATOR", sequenceName="SEG_MX_AUX_FILE_APR_ID_APRTO_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEG_MX_AUX_FILE_APR_IDAPRTOPK_GENERATOR")
	@Column(name="ID_APRTO_PK", unique=true, nullable=false, precision=15)
	private long idAprtoPk;

	@Column(name="FCH_HOR_EXEC", nullable=false)
	private Timestamp fchHorExec;

	@Column(name="ID_ESTAT_FK", nullable=false, precision=1)
	private BigDecimal idEstatFk;

	@Column(name="NUM_LINE_DONE", nullable=false, precision=5)
	private BigDecimal numLineDone;

	@Column(name="NUM_LINE_FAIL", nullable=false, precision=5)
	private BigDecimal numLineFail;

	@Column(name="NUM_LINE_TOT", nullable=false, precision=5)
	private BigDecimal numLineTot;

	@Column(name="TXT_MSG_ERR", length=4000)
	private String txtMsgErr;

	@Column(name="TXT_NOMB_ARCH", nullable=false, length=65)
	private String txtNombArch;

	@Column(name="TXT_RUTA_ARCH", nullable=false, length=200)
	private String txtRutaArch;

	@Column(name="USR_APRV", nullable=false, length=15)
	private String usrAprv;

	public SegMxAuxFileApr() {
	}

	public long getIdAprtoPk() {
		return this.idAprtoPk;
	}

	public void setIdAprtoPk(long idAprtoPk) {
		this.idAprtoPk = idAprtoPk;
	}

	public Timestamp getFchHorExec() {
		if(this.fchHorExec == null) {
			return null;
		}
		return (Timestamp) this.fchHorExec.clone();
	}

	public void setFchHorExec(Timestamp fchHorExec) {
		if(fchHorExec == null){
			this.fchHorExec = null;
		} else {
			this.fchHorExec = (Timestamp) fchHorExec.clone();
		}
	}

	public BigDecimal getIdEstatFk() {
		return this.idEstatFk;
	}

	public void setIdEstatFk(BigDecimal idEstatFk) {
		this.idEstatFk = idEstatFk;
	}

	public BigDecimal getNumLineDone() {
		return this.numLineDone;
	}

	public void setNumLineDone(BigDecimal numLineDone) {
		this.numLineDone = numLineDone;
	}

	public BigDecimal getNumLineFail() {
		return this.numLineFail;
	}

	public void setNumLineFail(BigDecimal numLineFail) {
		this.numLineFail = numLineFail;
	}

	public BigDecimal getNumLineTot() {
		return this.numLineTot;
	}

	public void setNumLineTot(BigDecimal numLineTot) {
		this.numLineTot = numLineTot;
	}

	public String getTxtMsgErr() {
		return this.txtMsgErr;
	}

	public void setTxtMsgErr(String txtMsgErr) {
		this.txtMsgErr = txtMsgErr;
	}

	public String getTxtNombArch() {
		return this.txtNombArch;
	}

	public void setTxtNombArch(String txtNombArch) {
		this.txtNombArch = txtNombArch;
	}

	public String getTxtRutaArch() {
		return this.txtRutaArch;
	}

	public void setTxtRutaArch(String txtRutaArch) {
		this.txtRutaArch = txtRutaArch;
	}

	public String getUsrAprv() {
		return this.usrAprv;
	}

	public void setUsrAprv(String usrAprv) {
		this.usrAprv = usrAprv;
	}

}