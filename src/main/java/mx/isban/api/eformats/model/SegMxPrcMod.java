package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SEG_MX_PRC_MOD database table.
 * 
 */
@Entity
@Table(name="SEG_MX_PRC_MOD")
@NamedQuery(name="SegMxPrcMod.findAll", query="SELECT s FROM SegMxPrcMod s")
public class SegMxPrcMod implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SEG_MX_PRC_MOD_IDMODPK_GENERATOR", sequenceName="SEG_MX_PRC_MOD_ID_MOD_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEG_MX_PRC_MOD_IDMODPK_GENERATOR")
	@Column(name="ID_MOD_PK", unique=true, nullable=false, precision=8)
	private long idModPk;

	@Column(name="DSC_DESC", length=500)
	private String dscDesc;

	@Column(name="FCH_ULT_MOD", nullable=false)
	private Timestamp fchUltMod;

	@Column(name="FLG_ACT", nullable=false, precision=1)
	private BigDecimal flgAct;

	@Column(name="ID_MOD_PAD_FK", precision=8)
	private BigDecimal idModPadFk;

	@Column(name="TXT_CVE", nullable=false, length=60)
	private String txtCve;

	@Column(name="TXT_NOMB", nullable=false, length=250)
	private String txtNomb;

	@Column(name="TXT_URI_FWD", length=2000)
	private String txtUriFwd;

	@Column(name="TXT_URI_HOME", length=2000)
	private String txtUriHome;

	//bi-directional many-to-one association to SegMxAuxTipoMod
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_MOD_FK", nullable=false, insertable=false, updatable=false)
	private SegMxAuxTipoMod segMxAuxTipoMod;

	//bi-directional many-to-one association to SegMxPrcPefto
	@OneToMany(mappedBy="segMxPrcMod")
	private List<SegMxPrcPefto> segMxPrcPeftos;

	public SegMxPrcMod() {
	}

	public long getIdModPk() {
		return this.idModPk;
	}

	public void setIdModPk(long idModPk) {
		this.idModPk = idModPk;
	}

	public String getDscDesc() {
		return this.dscDesc;
	}

	public void setDscDesc(String dscDesc) {
		this.dscDesc = dscDesc;
	}

	public Timestamp getFchUltMod() {
		if(this.fchUltMod == null) {
			return null;
		}
		return (Timestamp) this.fchUltMod.clone();
	}

	public void setFchUltMod(Timestamp fchUltMod) {
		if(fchUltMod == null){
			this.fchUltMod = null;
		} else {
			this.fchUltMod = (Timestamp) fchUltMod.clone();
		}
	}

	public BigDecimal getFlgAct() {
		return this.flgAct;
	}

	public void setFlgAct(BigDecimal flgAct) {
		this.flgAct = flgAct;
	}

	public BigDecimal getIdModPadFk() {
		return this.idModPadFk;
	}

	public void setIdModPadFk(BigDecimal idModPadFk) {
		this.idModPadFk = idModPadFk;
	}

	public String getTxtCve() {
		return this.txtCve;
	}

	public void setTxtCve(String txtCve) {
		this.txtCve = txtCve;
	}

	public String getTxtNomb() {
		return this.txtNomb;
	}

	public void setTxtNomb(String txtNomb) {
		this.txtNomb = txtNomb;
	}

	public String getTxtUriFwd() {
		return this.txtUriFwd;
	}

	public void setTxtUriFwd(String txtUriFwd) {
		this.txtUriFwd = txtUriFwd;
	}

	public String getTxtUriHome() {
		return this.txtUriHome;
	}

	public void setTxtUriHome(String txtUriHome) {
		this.txtUriHome = txtUriHome;
	}

	public SegMxAuxTipoMod getSegMxAuxTipoMod() {
		return this.segMxAuxTipoMod;
	}

	public void setSegMxAuxTipoMod(SegMxAuxTipoMod segMxAuxTipoMod) {
		this.segMxAuxTipoMod = segMxAuxTipoMod;
	}

	public List<SegMxPrcPefto> getSegMxPrcPeftos() {
		return new ArrayList<SegMxPrcPefto>(segMxPrcPeftos);
	}

	public void setSegMxPrcPeftos(List<SegMxPrcPefto> segMxPrcPeftos) {
		this.segMxPrcPeftos = new ArrayList<SegMxPrcPefto>(segMxPrcPeftos);
	}

	public SegMxPrcPefto addSegMxPrcPefto(SegMxPrcPefto segMxPrcPefto) {
		getSegMxPrcPeftos().add(segMxPrcPefto);
		segMxPrcPefto.setSegMxPrcMod(this);

		return segMxPrcPefto;
	}

	public SegMxPrcPefto removeSegMxPrcPefto(SegMxPrcPefto segMxPrcPefto) {
		getSegMxPrcPeftos().remove(segMxPrcPefto);
		segMxPrcPefto.setSegMxPrcMod(null);

		return segMxPrcPefto;
	}

}