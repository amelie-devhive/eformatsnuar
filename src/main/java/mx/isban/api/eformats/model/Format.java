package mx.isban.api.eformats.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FORMAT database table.
 * 
 */
@Entity
@Table(name="FORMAT")
@NamedQuery(name="Format.findAll", query="SELECT f FROM Format f")
public class Format implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FORMAT_FORMATID_GENERATOR", sequenceName="SEQ_FEI_MX_FMT")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FORMAT_FORMATID_GENERATOR")
	@Column(name="FORMAT_ID", unique=true, nullable=false, precision=19)
	private long formatId;

	@Column(name="FORMAT_NAME", length=255)
	private String formatName;

	public Format() {
		//default constructor
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}


	public long getFormatId() {
		return this.formatId;
	}

	public void setFormatId(long formatId) {
		this.formatId = formatId;
	}

	public String getFormatName() {
		return this.formatName;
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}

}