package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the FEI_MX_REL_FMT_PAQ database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_FMT_PAQ")
@NamedQuery(name="FeiMxRelFmtPaq.findAll", query="SELECT f FROM FeiMxRelFmtPaq f")
public class FeiMxRelFmtPaq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_REL_FMT_PAQ_IDFMTO_GENERATOR", sequenceName="FEI_MX_REL_FMT_PAQ_ID_FMTO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_FMT_PAQ_IDFMTO_GENERATOR")
	@Column(name="ID_FMTO", unique=true, nullable=false, precision=10)
	private long idFmto;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxMaeCatFmt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FMT", insertable=false, updatable=false)
	private FeiMxMaeCatFmt feiMxMaeCatFmt;

	//bi-directional many-to-one association to FeiMxRelCatPaq
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PAQ", insertable=false, updatable=false)
	private FeiMxRelCatPaq feiMxRelCatPaq;

	public FeiMxRelFmtPaq() {
		// default constructor
	}

	public long getIdFmto() {
		return this.idFmto;
	}

	public void setIdFmto(long idFmto) {
		this.idFmto = idFmto;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public FeiMxMaeCatFmt getFeiMxMaeCatFmt() {
		return this.feiMxMaeCatFmt;
	}

	public void setFeiMxMaeCatFmt(FeiMxMaeCatFmt feiMxMaeCatFmt) {
		this.feiMxMaeCatFmt = feiMxMaeCatFmt;
	}

	public FeiMxRelCatPaq getFeiMxRelCatPaq() {
		return this.feiMxRelCatPaq;
	}

	public void setFeiMxRelCatPaq(FeiMxRelCatPaq feiMxRelCatPaq) {
		this.feiMxRelCatPaq = feiMxRelCatPaq;
	}

}