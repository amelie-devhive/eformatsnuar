package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_MAE_CAT_FMT database table.
 * 
 */
@Entity
@Table(name="FEI_MX_MAE_CAT_FMT")
@NamedQuery(name="FeiMxMaeCatFmt.findAll", query="SELECT f FROM FeiMxMaeCatFmt f")
public class FeiMxMaeCatFmt implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_LIKE_KEYWORD = "FeiMxMaeCatFmt.findLikeKeyword";

	@Id
	@SequenceGenerator(name="FEI_MX_MAE_CAT_FMT_IDFMT_GENERATOR", sequenceName="FEI_MX_MAE_CAT_FMT_ID_FMT")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_MAE_CAT_FMT_IDFMT_GENERATOR")
	@Column(name="ID_FMT", unique=true, nullable=false, precision=38)
	private long idFmt;

	@Column(name="DES_NOM_FMT", length=100)
	private String desNomFmt;

	@Column(name="DSC_AREA_RES", length=50)
	private String dscAreaRes;

	@Column(name="DSC_NOM_NEGO", length=50)
	private String dscNomNego;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Lob
	@Column(name="GRL_IMG_FMT")
	private byte[] grlImgFmt;

	@Column(name="TXT_PLBRA_CLAVE", length=200)
	private String txtPlbraClave;

	@Column(name="USR_ID", length=10)
	private String usrId;

	@Column(name="VAL_CLAVE_FMT", length=10)
	private String valClaveFmt;

	//bi-directional many-to-one association to FeiMxLogBta
	@OneToMany(mappedBy="feiMxMaeCatFmt")
	private List<FeiMxLogBta> feiMxLogBtas;

	//bi-directional many-to-one association to FeiMxRelCatTipoFmt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_FMT", insertable=false, updatable=false)
	private FeiMxRelCatTipoFmt feiMxRelCatTipoFmt;

	//bi-directional many-to-one association to FeiMxRelFmtFav
	@OneToMany(mappedBy="feiMxMaeCatFmt")
	private List<FeiMxRelFmtFav> feiMxRelFmtFavs;

	//bi-directional many-to-one association to FeiMxRelFmtPaq
	@OneToMany(mappedBy="feiMxMaeCatFmt")
	private List<FeiMxRelFmtPaq> feiMxRelFmtPaqs;

	//bi-directional many-to-one association to FeiMxRelFmtPen
	@OneToMany(mappedBy="feiMxMaeCatFmt")
	private List<FeiMxRelFmtPen> feiMxRelFmtPens;

	public FeiMxMaeCatFmt() {
		//default constructor
	}

	public FeiMxMaeCatFmt(Long idFormato) {
		this.idFmt = idFormato;
	}

	public long getIdFmt() {
		return this.idFmt;
	}

	public void setIdFmt(long idFmt) {
		this.idFmt = idFmt;
	}

	public String getDesNomFmt() {
		return this.desNomFmt;
	}

	public void setDesNomFmt(String desNomFmt) {
		this.desNomFmt = desNomFmt;
	}

	public String getDscAreaRes() {
		return this.dscAreaRes;
	}

	public void setDscAreaRes(String dscAreaRes) {
		this.dscAreaRes = dscAreaRes;
	}

	public String getDscNomNego() {
		return this.dscNomNego;
	}

	public void setDscNomNego(String dscNomNego) {
		this.dscNomNego = dscNomNego;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public byte[] getGrlImgFmt() {
		if(grlImgFmt == null) {
			return new byte[] {};
		}
		return this.grlImgFmt.clone();
	}

	public void setGrlImgFmt(byte[] grlImgFmt) {
		if(grlImgFmt == null) {
			this.grlImgFmt = null;
		}else {
			this.grlImgFmt = grlImgFmt.clone();
		}
	}

	public String getTxtPlbraClave() {
		return this.txtPlbraClave;
	}

	public void setTxtPlbraClave(String txtPlbraClave) {
		this.txtPlbraClave = txtPlbraClave;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getValClaveFmt() {
		return this.valClaveFmt;
	}

	public void setValClaveFmt(String valClaveFmt) {
		this.valClaveFmt = valClaveFmt;
	}

	public List<FeiMxLogBta> getFeiMxLogBtas() {
		if(feiMxLogBtas == null) {
			return Collections.emptyList();
		}
		List<FeiMxLogBta> copy = new ArrayList<>();
		Collections.copy(copy, feiMxLogBtas);
		return copy;
	}

	public void setFeiMxLogBtas(List<FeiMxLogBta> feiMxLogBtas) {
		if(feiMxLogBtas == null) {
			this.feiMxLogBtas = null;
		}else {
			Collections.copy(this.feiMxLogBtas, feiMxLogBtas);
		}
	}
	
	public FeiMxLogBta addFeiMxLogBta(FeiMxLogBta feiMxLogBta) {
		getFeiMxLogBtas().add(feiMxLogBta);
		feiMxLogBta.setFeiMxMaeCatFmt(this);

		return feiMxLogBta;
	}

	public FeiMxLogBta removeFeiMxLogBta(FeiMxLogBta feiMxLogBta) {
		getFeiMxLogBtas().remove(feiMxLogBta);
		feiMxLogBta.setFeiMxMaeCatFmt(null);

		return feiMxLogBta;
	}

	public FeiMxRelCatTipoFmt getFeiMxRelCatTipoFmt() {
		return this.feiMxRelCatTipoFmt;
	}

	public void setFeiMxRelCatTipoFmt(FeiMxRelCatTipoFmt feiMxRelCatTipoFmt) {
		this.feiMxRelCatTipoFmt = feiMxRelCatTipoFmt;
	}


	public List<FeiMxRelFmtFav> getFeiMxRelFmtFavs() {
		return new ArrayList<FeiMxRelFmtFav>(feiMxRelFmtFavs);
	}

	public void setFeiMxRelFmtFavs(List<FeiMxRelFmtFav> feiMxRelFmtFavs) {
		this.feiMxRelFmtFavs = new ArrayList<FeiMxRelFmtFav>(feiMxRelFmtFavs);
	}

	public FeiMxRelFmtFav addFeiMxRelFmtFav(FeiMxRelFmtFav feiMxRelFmtFav) {
		getFeiMxRelFmtFavs().add(feiMxRelFmtFav);
		feiMxRelFmtFav.setFeiMxMaeCatFmt(this);

		return feiMxRelFmtFav;
	}

	public FeiMxRelFmtFav removeFeiMxRelFmtFav(FeiMxRelFmtFav feiMxRelFmtFav) {
		getFeiMxRelFmtFavs().remove(feiMxRelFmtFav);
		feiMxRelFmtFav.setFeiMxMaeCatFmt(null);

		return feiMxRelFmtFav;
	}

	public List<FeiMxRelFmtPaq> getFeiMxRelFmtPaqs() {
		return new ArrayList<FeiMxRelFmtPaq>(feiMxRelFmtPaqs);
	}

	public void setFeiMxRelFmtPaqs(List<FeiMxRelFmtPaq> feiMxRelFmtPaqs) {
		this.feiMxRelFmtPaqs = new ArrayList<FeiMxRelFmtPaq>(feiMxRelFmtPaqs);
	}

	public FeiMxRelFmtPaq addFeiMxRelFmtPaq(FeiMxRelFmtPaq feiMxRelFmtPaq) {
		getFeiMxRelFmtPaqs().add(feiMxRelFmtPaq);
		feiMxRelFmtPaq.setFeiMxMaeCatFmt(this);

		return feiMxRelFmtPaq;
	}

	public FeiMxRelFmtPaq removeFeiMxRelFmtPaq(FeiMxRelFmtPaq feiMxRelFmtPaq) {
		getFeiMxRelFmtPaqs().remove(feiMxRelFmtPaq);
		feiMxRelFmtPaq.setFeiMxMaeCatFmt(null);

		return feiMxRelFmtPaq;
	}

	public List<FeiMxRelFmtPen> getFeiMxRelFmtPens() {
		if(feiMxRelFmtPens == null) {
			return Collections.emptyList();
		}
		List<FeiMxRelFmtPen> copy = new ArrayList<>();
		Collections.copy(copy, feiMxRelFmtPens);
		return copy;
	}

	public void setFeiMxRelFmtPens(List<FeiMxRelFmtPen> feiMxRelFmtPens) {
		if(feiMxRelFmtPens == null) {
			this.feiMxRelFmtPens = null;
		}else {
			Collections.copy(this.feiMxRelFmtPens, feiMxRelFmtPens);
		}
	}
	
	public FeiMxRelFmtPen addFeiMxRelFmtPen(FeiMxRelFmtPen feiMxRelFmtPen) {
		getFeiMxRelFmtPens().add(feiMxRelFmtPen);
		feiMxRelFmtPen.setFeiMxMaeCatFmt(this);

		return feiMxRelFmtPen;
	}

	public FeiMxRelFmtPen removeFeiMxRelFmtPen(FeiMxRelFmtPen feiMxRelFmtPen) {
		getFeiMxRelFmtPens().remove(feiMxRelFmtPen);
		feiMxRelFmtPen.setFeiMxMaeCatFmt(null);

		return feiMxRelFmtPen;
	}

}