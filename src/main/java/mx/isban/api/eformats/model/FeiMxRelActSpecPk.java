package mx.isban.api.eformats.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FeiMxRelActSpecPk implements Serializable{ 
	private static final long serialVersionUID = -4050645714055914418L;
	
	@Column(name = "ID_ACT_SPEC")
	private long idActSpec;
	@Column(name = "ID_ACT_GEN")
	private long idActGen;
	
	public long getIdActSpec() {
		return idActSpec;
	}
	public void setIdActSpec(long idActSpec) {
		this.idActSpec = idActSpec;
	}
	public long getIdActGen() {
		return idActGen;
	}
	public void setIdActGen(long idActGen) {
		this.idActGen = idActGen;
	}
	
}
