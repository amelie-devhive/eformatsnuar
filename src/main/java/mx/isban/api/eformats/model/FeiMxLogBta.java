package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import mx.isban.api.eformats.dto.TopFormatosBitacora;


/**
 * The persistent class for the FEI_MX_LOG_BTA database table.
 * 
 */
@Entity
@Table(name="FEI_MX_LOG_BTA")
@SqlResultSetMappings({
	@SqlResultSetMapping(name = "topBitacoraFormatoMapper", 
			classes = {
					@ConstructorResult(
							targetClass = TopFormatosBitacora.class,
							columns = {
									@ColumnResult(name = "ID_FORMATO", type = long.class),
									@ColumnResult(name = "COUNT_FORMATO", type = int.class),
									@ColumnResult(name = "NUM_EJECUTIVO", type = String.class),
									@ColumnResult(name = "CLAVE_FORMATO", type = String.class),
									@ColumnResult(name = "NOMBRE_FORMATO", type = String.class),
							})
			})
})
@NamedNativeQueries({
	@NamedNativeQuery(name=FeiMxLogBta.FIND_TOP, 
			query="select ID_FORMATO, COUNT_FORMATO, NUM_EJECUTIVO, CLAVE_FORMATO, NOMBRE_FORMATO from ( " + 
					"select bit.ID_FMT, count(bit.ID_FMT) as COUNT_FORMATO, bit.NUM_EJE, cat.VAL_CLAVE_FMT, cat.DES_NOM_FMT " + 
					"from FEI_MX_LOG_BTA bit " + 
					"    inner join FEI_MX_MAE_CAT_FMT cat " + 
					"    on cat.ID_FMT = bit.ID_FMT " + 
					"    where TRIM(bit.NUM_EJE) = TRIM(:numEjecutivo) " + 
					"    group by bit.ID_FMT, bit.NUM_EJE, cat.VAL_CLAVE_FMT, cat.DES_NOM_FMT " + 
					"    order by COUNT_FORMATO desc " + 
					") where rownum <= :topCount ", 
			resultSetMapping = "topBitacoraFormatoMapper"
			)	
})
public class FeiMxLogBta implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_TOP = "FeiMxLogBta.findTop";

	@Id
	@SequenceGenerator(name="FEI_MX_LOG_BTA_IDBTA_GENERATOR", sequenceName="SEQ_FEI_MX_BTA")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_LOG_BTA_IDBTA_GENERATOR")
	@Column(name="ID_BTA", unique=true, nullable=false, precision=10)
	private long idBta;

	@Column(name="DSC_ESTAT_FMT", length=1)
	private String dscEstatFmt;

	@Column(name="FCH_HOR_INI")
	private Timestamp fchHorIni;

	@Column(name="FCH_HOR_TERM")
	private Timestamp fchHorTerm;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="NUM_EJE", length=10)
	private String numEje;

	@Column(name="NUM_SUC", length=5)
	private String numSuc;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxMaeCatFmt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FMT", insertable=false, updatable=false)
	private FeiMxMaeCatFmt feiMxMaeCatFmt;

	public FeiMxLogBta() {
		// default constructor
	}

	public long getIdBta() {
		return this.idBta;
	}

	public void setIdBta(long idBta) {
		this.idBta = idBta;
	}

	public String getDscEstatFmt() {
		return this.dscEstatFmt;
	}

	public void setDscEstatFmt(String dscEstatFmt) {
		this.dscEstatFmt = dscEstatFmt;
	}

	public Timestamp getFchHorIni() {
		if(fchHorIni == null) {
			return null;
		}
		return (Timestamp) this.fchHorIni.clone();
	}

	public void setFchHorIni(Timestamp fchHorIni) {
		if(fchHorIni == null) {
			this.fchHorIni = null;
		}else {
			this.fchHorIni = (Timestamp) fchHorIni.clone();
		}
	}

	public Timestamp getFchHorTerm() {
		if(fchHorTerm == null) {
			return null;
		}
		return (Timestamp) this.fchHorTerm.clone();
	}

	public void setFchHorTerm(Timestamp fchHorTerm) {
		if(fchHorTerm == null) {
			this.fchHorTerm = null;
		}else {
			this.fchHorTerm = (Timestamp) fchHorTerm.clone();
		}
	}

	public Timestamp getFchHorUltAct() {
		if(fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getNumEje() {
		return this.numEje;
	}

	public void setNumEje(String numEje) {
		this.numEje = numEje;
	}

	public String getNumSuc() {
		return this.numSuc;
	}

	public void setNumSuc(String numSuc) {
		this.numSuc = numSuc;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public FeiMxMaeCatFmt getFeiMxMaeCatFmt() {
		return this.feiMxMaeCatFmt;
	}

	public void setFeiMxMaeCatFmt(FeiMxMaeCatFmt feiMxMaeCatFmt) {
		this.feiMxMaeCatFmt = feiMxMaeCatFmt;
	}

}