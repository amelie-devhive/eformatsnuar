package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_MAE_ACT_GEN database table.
 * 
 */
@Entity
@Table(name="FEI_MX_MAE_ACT_GEN")
@NamedQuery(name="FeiMxMaeActGen.findAll", query="SELECT f FROM FeiMxMaeActGen f")
public class FeiMxMaeActGen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_MAE_ACT_GEN_IDACTGEN_GENERATOR", sequenceName="FEI_MX_MAE_ACT_GEN_ID_ACT_GEN")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_MAE_ACT_GEN_IDACTGEN_GENERATOR")
	@Column(name="ID_ACT_GEN", unique=true, nullable=false, precision=38)
	private long idActGen;

	@Column(name="DSC_ACT_GEN", length=255)
	private String dscActGen;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxRelActSpec
	@OneToMany(mappedBy="feiMxMaeActGen")
	private List<FeiMxRelActSpec> feiMxRelActSpecs;

	public FeiMxMaeActGen() {
	}

	public long getIdActGen() {
		return this.idActGen;
	}

	public void setIdActGen(long idActGen) {
		this.idActGen = idActGen;
	}

	public String getDscActGen() {
		return this.dscActGen;
	}

	public void setDscActGen(String dscActGen) {
		this.dscActGen = dscActGen;
	}

	public Timestamp getFchHorUltAct() {
		if(fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null) {
			this.fchHorUltAct = null;
		}else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public List<FeiMxRelActSpec> getFeiMxRelActSpecs() {
		return new ArrayList<FeiMxRelActSpec>(feiMxRelActSpecs);
	}

	public void setFeiMxRelActSpecs(List<FeiMxRelActSpec> feiMxRelActSpecs) {
		this.feiMxRelActSpecs = new ArrayList<FeiMxRelActSpec>(feiMxRelActSpecs);
	}

	public FeiMxRelActSpec addFeiMxRelActSpec(FeiMxRelActSpec feiMxRelActSpec) {
		getFeiMxRelActSpecs().add(feiMxRelActSpec);
		feiMxRelActSpec.setFeiMxMaeActGen(this);

		return feiMxRelActSpec;
	}

	public FeiMxRelActSpec removeFeiMxRelActSpec(FeiMxRelActSpec feiMxRelActSpec) {
		getFeiMxRelActSpecs().remove(feiMxRelActSpec);
		feiMxRelActSpec.setFeiMxMaeActGen(null);

		return feiMxRelActSpec;
	}

}