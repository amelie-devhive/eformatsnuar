package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SEG_MX_PRC_OPER database table.
 * 
 */
@Entity
@Table(name="SEG_MX_PRC_OPER")
@NamedQuery(name="SegMxPrcOper.findAll", query="SELECT s FROM SegMxPrcOper s")
public class SegMxPrcOper implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SEG_MX_PRC_OPER_IDOPERPK_GENERATOR", sequenceName="SEG_MX_PRC_OPER_ID_OPER_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEG_MX_PRC_OPER_IDOPERPK_GENERATOR")
	@Column(name="ID_OPER_PK", unique=true, nullable=false)
	private long idOperPk;

	@Column(name="DSC_DESC", length=500)
	private String dscDesc;

	@Column(name="FCH_ULT_MOD", nullable=false)
	private Timestamp fchUltMod;

	@Column(name="FLG_ACT", nullable=false, precision=1)
	private BigDecimal flgAct;

	@Column(name="FLG_ENAB", nullable=false, precision=1)
	private BigDecimal flgEnab;

	@Column(name="FLG_VIS", nullable=false, precision=1)
	private BigDecimal flgVis;

	@Column(name="TXT_CVE", nullable=false, length=60)
	private String txtCve;

	@Column(name="TXT_NOMB", nullable=false, length=250)
	private String txtNomb;

	@Column(name="TXT_URI", length=2000)
	private String txtUri;

	//bi-directional many-to-one association to SegMxPrcPefto
	@OneToMany(mappedBy="segMxPrcOper")
	private List<SegMxPrcPefto> segMxPrcPeftos;

	public SegMxPrcOper() {
	}

	public long getIdOperPk() {
		return this.idOperPk;
	}

	public void setIdOperPk(long idOperPk) {
		this.idOperPk = idOperPk;
	}

	public String getDscDesc() {
		return this.dscDesc;
	}

	public void setDscDesc(String dscDesc) {
		this.dscDesc = dscDesc;
	}

	public Timestamp getFchUltMod() {
		if(this.fchUltMod == null) {
			return null;
		}
		return (Timestamp) this.fchUltMod.clone();
	}

	public void setFchUltMod(Timestamp fchUltMod) {
		if(fchUltMod == null){
			this.fchUltMod = null;
		} else {
			this.fchUltMod = (Timestamp) fchUltMod.clone();
		}
	}

	public BigDecimal getFlgAct() {
		return this.flgAct;
	}

	public void setFlgAct(BigDecimal flgAct) {
		this.flgAct = flgAct;
	}

	public BigDecimal getFlgEnab() {
		return this.flgEnab;
	}

	public void setFlgEnab(BigDecimal flgEnab) {
		this.flgEnab = flgEnab;
	}

	public BigDecimal getFlgVis() {
		return this.flgVis;
	}

	public void setFlgVis(BigDecimal flgVis) {
		this.flgVis = flgVis;
	}

	public String getTxtCve() {
		return this.txtCve;
	}

	public void setTxtCve(String txtCve) {
		this.txtCve = txtCve;
	}

	public String getTxtNomb() {
		return this.txtNomb;
	}

	public void setTxtNomb(String txtNomb) {
		this.txtNomb = txtNomb;
	}

	public String getTxtUri() {
		return this.txtUri;
	}

	public void setTxtUri(String txtUri) {
		this.txtUri = txtUri;
	}

	public List<SegMxPrcPefto> getSegMxPrcPeftos() {
		return new ArrayList<SegMxPrcPefto>(segMxPrcPeftos);
	}

	public void setSegMxPrcPeftos(List<SegMxPrcPefto> segMxPrcPeftos) {
		this.segMxPrcPeftos = new ArrayList<SegMxPrcPefto>(segMxPrcPeftos);
	}

	public SegMxPrcPefto addSegMxPrcPefto(SegMxPrcPefto segMxPrcPefto) {
		getSegMxPrcPeftos().add(segMxPrcPefto);
		segMxPrcPefto.setSegMxPrcOper(this);

		return segMxPrcPefto;
	}

	public SegMxPrcPefto removeSegMxPrcPefto(SegMxPrcPefto segMxPrcPefto) {
		getSegMxPrcPeftos().remove(segMxPrcPefto);
		segMxPrcPefto.setSegMxPrcOper(null);

		return segMxPrcPefto;
	}

}