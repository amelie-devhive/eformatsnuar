package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SEG_MX_PRC_APP database table.
 * 
 */
@Entity
@Table(name="SEG_MX_PRC_APP")
@NamedQuery(name="SegMxPrcApp.findAll", query="SELECT s FROM SegMxPrcApp s")
public class SegMxPrcApp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SEG_MX_PRC_APP_IDAPPPK_GENERATOR", sequenceName="SEG_MX_PRC_APP_ID_APP_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEG_MX_PRC_APP_IDAPPPK_GENERATOR")
	@Column(name="ID_APP_PK", unique=true, nullable=false, precision=7)
	private long idAppPk;

	@Column(name="DSC_DESC", length=500)
	private String dscDesc;

	@Column(name="FCH_ULT_MOD", nullable=false)
	private Timestamp fchUltMod;

	@Column(name="FLG_ACT", nullable=false, precision=1)
	private BigDecimal flgAct;

	@Column(name="TXT_CVE", nullable=false, length=60)
	private String txtCve;

	@Column(name="TXT_NOMB", nullable=false, length=250)
	private String txtNomb;

	@Column(name="TXT_URI_CTXT", length=2000)
	private String txtUriCtxt;

	@Column(name="TXT_URI_HOME", length=2000)
	private String txtUriHome;

	//bi-directional many-to-one association to SegMxPrcPefto
	@OneToMany(mappedBy="segMxPrcApp")
	private List<SegMxPrcPefto> segMxPrcPeftos;

	public SegMxPrcApp() {
	}

	public long getIdAppPk() {
		return this.idAppPk;
	}

	public void setIdAppPk(long idAppPk) {
		this.idAppPk = idAppPk;
	}

	public String getDscDesc() {
		return this.dscDesc;
	}

	public void setDscDesc(String dscDesc) {
		this.dscDesc = dscDesc;
	}

	public Timestamp getFchUltMod() {
		if(this.fchUltMod == null) {
			return null;
		}
		return (Timestamp) this.fchUltMod.clone();
	}

	public void setFchUltMod(Timestamp fchUltMod) {
		if(fchUltMod == null){
			this.fchUltMod = null;
		} else {
			this.fchUltMod = (Timestamp) fchUltMod.clone();
		}
	}

	public BigDecimal getFlgAct() {
		return this.flgAct;
	}

	public void setFlgAct(BigDecimal flgAct) {
		this.flgAct = flgAct;
	}

	public String getTxtCve() {
		return this.txtCve;
	}

	public void setTxtCve(String txtCve) {
		this.txtCve = txtCve;
	}

	public String getTxtNomb() {
		return this.txtNomb;
	}

	public void setTxtNomb(String txtNomb) {
		this.txtNomb = txtNomb;
	}

	public String getTxtUriCtxt() {
		return this.txtUriCtxt;
	}

	public void setTxtUriCtxt(String txtUriCtxt) {
		this.txtUriCtxt = txtUriCtxt;
	}

	public String getTxtUriHome() {
		return this.txtUriHome;
	}

	public void setTxtUriHome(String txtUriHome) {
		this.txtUriHome = txtUriHome;
	}

	public List<SegMxPrcPefto> getSegMxPrcPeftos() {
		return new ArrayList<SegMxPrcPefto>(segMxPrcPeftos);
	}

	public void setSegMxPrcPeftos(List<SegMxPrcPefto> segMxPrcPeftos) {
		this.segMxPrcPeftos = new ArrayList<SegMxPrcPefto>(segMxPrcPeftos);
	}

	public SegMxPrcPefto addSegMxPrcPefto(SegMxPrcPefto segMxPrcPefto) {
		getSegMxPrcPeftos().add(segMxPrcPefto);
		segMxPrcPefto.setSegMxPrcApp(this);

		return segMxPrcPefto;
	}

	public SegMxPrcPefto removeSegMxPrcPefto(SegMxPrcPefto segMxPrcPefto) {
		getSegMxPrcPeftos().remove(segMxPrcPefto);
		segMxPrcPefto.setSegMxPrcApp(null);

		return segMxPrcPefto;
	}

}