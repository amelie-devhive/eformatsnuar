package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_REL_FMT_MAS_UTIL database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_FMT_MAS_UTIL")
@NamedQuery(name="FeiMxRelFormatoMasUtilizado.findAll", query="SELECT f FROM FeiMxRelFormatoMasUtilizado f")
public class FeiMxRelFormatoMasUtilizado implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_REL_FMT_MAS_UTIL_ID_MAS_UTIL_GENERATOR", sequenceName="MAS_UTIL_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_FMT_MAS_UTIL_ID_MAS_UTIL_GENERATOR")
	@Column(name="ID_MAS_UTIL", unique=true, nullable=false, precision=10)
	private long idMasUtil;
	
	@Column(name="ID_FORMATO")
	private long idFormato;
	
	@Column(name="VAL_NUM_EJE", length=10)
	private String numEjecutivo;

	@Column(name="VAL_CONT")
	private long contador;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltimaActualizacion;

	@Column(name="USR_ID", length=10)
	private String usrId;

	public FeiMxRelFormatoMasUtilizado() {
		// default constructor
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	
	public long getIdMasUtil() {
		return idMasUtil;
	}

	public void setIdMasUtil(long idMasUtil) {
		this.idMasUtil = idMasUtil;
	}

	public long getIdFormato() {
		return idFormato;
	}

	public void setIdFormato(long idFormato) {
		this.idFormato = idFormato;
	}

	public String getNumEjecutivo() {
		return numEjecutivo;
	}

	public void setNumEjecutivo(String numEjecutivo) {
		this.numEjecutivo = numEjecutivo;
	}

	public long getContador() {
		return contador;
	}

	public void setContador(long contador) {
		this.contador = contador;
	}

	public Timestamp getFchHorUltimaActualizacion() {
		if(this.fchHorUltimaActualizacion == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltimaActualizacion.clone();
	}

	public void setFchHorUltimaActualizacion(Timestamp fchHorUltimaActualizacion) {
		if(fchHorUltimaActualizacion == null){
			this.fchHorUltimaActualizacion = null;
		} else {
			this.fchHorUltimaActualizacion = (Timestamp) fchHorUltimaActualizacion.clone();
		}
	}

	public String getUsrId() {
		return usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	
}