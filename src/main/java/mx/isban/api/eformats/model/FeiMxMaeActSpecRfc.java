package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the FEI_MX_MAE_ACT_SPEC_RFC database table.
 * 
 */
@Entity
@Table(name="FEI_MX_MAE_ACT_SPEC_RFC")
@NamedQuery(name="FeiMxMaeActSpecRfc.findAll", query="SELECT f FROM FeiMxMaeActSpecRfc f")
public class FeiMxMaeActSpecRfc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_MAE_ACT_SPEC_RFC_IDACTSPEC_GENERATOR", sequenceName="FEI_MX_MAE_ACT_SPEC_RFC_ID_ACT_SPEC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_MAE_ACT_SPEC_RFC_IDACTSPEC_GENERATOR")
	@Column(name="ID_ACT_SPEC", unique=true, nullable=false, precision=38)
	private long idActSpec;

	@Column(name="DSC_ACT", length=255)
	private String dscAct;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxRelAct
	@OneToMany(mappedBy="feiMxMaeActSpecRfc")
	private List<FeiMxRelAct> feiMxRelActs;

	//bi-directional many-to-one association to FeiMxRelActSpec
	@OneToMany(mappedBy="feiMxMaeActSpecRfc")
	private List<FeiMxRelActSpec> feiMxRelActSpecs;

	public FeiMxMaeActSpecRfc() {
	}

	public long getIdActSpec() {
		return this.idActSpec;
	}

	public void setIdActSpec(long idActSpec) {
		this.idActSpec = idActSpec;
	}

	public String getDscAct() {
		return this.dscAct;
	}

	public void setDscAct(String dscAct) {
		this.dscAct = dscAct;
	}

	public Timestamp getFchHorUltAct() {
		if(fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null) {
			this.fchHorUltAct = null;
		}else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public List<FeiMxRelAct> getFeiMxRelActs() {
		return new ArrayList<FeiMxRelAct>(feiMxRelActs);
	}

	public void setFeiMxRelActs(List<FeiMxRelAct> feiMxRelActs) {
		this.feiMxRelActs = new ArrayList<FeiMxRelAct>(feiMxRelActs);
	}

	public FeiMxRelAct addFeiMxRelAct(FeiMxRelAct feiMxRelAct) {
		getFeiMxRelActs().add(feiMxRelAct);
		feiMxRelAct.setFeiMxMaeActSpecRfc(this);

		return feiMxRelAct;
	}

	public FeiMxRelAct removeFeiMxRelAct(FeiMxRelAct feiMxRelAct) {
		getFeiMxRelActs().remove(feiMxRelAct);
		feiMxRelAct.setFeiMxMaeActSpecRfc(null);

		return feiMxRelAct;
	}

	public List<FeiMxRelActSpec> getFeiMxRelActSpecs() {
		return new ArrayList<FeiMxRelActSpec>(feiMxRelActSpecs);
	}

	public void setFeiMxRelActSpecs(List<FeiMxRelActSpec> feiMxRelActSpecs) {
		this.feiMxRelActSpecs = new ArrayList<FeiMxRelActSpec>(feiMxRelActSpecs);
	}

	public FeiMxRelActSpec addFeiMxRelActSpec(FeiMxRelActSpec feiMxRelActSpec) {
		getFeiMxRelActSpecs().add(feiMxRelActSpec);
		feiMxRelActSpec.setFeiMxMaeActSpecRfc(this);

		return feiMxRelActSpec;
	}

	public FeiMxRelActSpec removeFeiMxRelActSpec(FeiMxRelActSpec feiMxRelActSpec) {
		getFeiMxRelActSpecs().remove(feiMxRelActSpec);
		feiMxRelActSpec.setFeiMxMaeActSpecRfc(null);

		return feiMxRelActSpec;
	}

}