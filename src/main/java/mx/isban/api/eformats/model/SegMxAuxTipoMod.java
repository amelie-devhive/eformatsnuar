package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the SEG_MX_AUX_TIPO_MOD database table.
 * 
 */
@Entity
@Table(name="SEG_MX_AUX_TIPO_MOD")
@NamedQuery(name="SegMxAuxTipoMod.findAll", query="SELECT s FROM SegMxAuxTipoMod s")
public class SegMxAuxTipoMod implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SEG_MX_AUX_TIPO_MOD_IDTIPOMODPK_GENERATOR", sequenceName="SEG_MX_AUX_TIPO_MOD_ID_TIPO_MOD_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEG_MX_AUX_TIPO_MOD_IDTIPOMODPK_GENERATOR")
	@Column(name="ID_TIPO_MOD_PK", unique=true, nullable=false, precision=2)
	private long idTipoModPk;

	@Column(name="TXT_NOMB", nullable=false, length=250)
	private String txtNomb;

	//bi-directional many-to-one association to SegMxPrcMod
	@OneToMany(mappedBy="segMxAuxTipoMod")
	private List<SegMxPrcMod> segMxPrcMods;

	public SegMxAuxTipoMod() {
	}

	public long getIdTipoModPk() {
		return this.idTipoModPk;
	}

	public void setIdTipoModPk(long idTipoModPk) {
		this.idTipoModPk = idTipoModPk;
	}

	public String getTxtNomb() {
		return this.txtNomb;
	}

	public void setTxtNomb(String txtNomb) {
		this.txtNomb = txtNomb;
	}

	public List<SegMxPrcMod> getSegMxPrcMods() {
		return new ArrayList<SegMxPrcMod>(segMxPrcMods);
	}

	public void setSegMxPrcMods(List<SegMxPrcMod> segMxPrcMods) {
		this.segMxPrcMods = new ArrayList<SegMxPrcMod>(segMxPrcMods);
	}

	public SegMxPrcMod addSegMxPrcMod(SegMxPrcMod segMxPrcMod) {
		getSegMxPrcMods().add(segMxPrcMod);
		segMxPrcMod.setSegMxAuxTipoMod(this);

		return segMxPrcMod;
	}

	public SegMxPrcMod removeSegMxPrcMod(SegMxPrcMod segMxPrcMod) {
		getSegMxPrcMods().remove(segMxPrcMod);
		segMxPrcMod.setSegMxAuxTipoMod(null);

		return segMxPrcMod;
	}

}