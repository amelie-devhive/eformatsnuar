package mx.isban.api.eformats.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the FEI_MX_REL_FMT_FAV database table.
 * 
 */
@Entity
@Table(name="FEI_MX_REL_FMT_FAV")
@NamedQuery(name="FeiMxRelFmtFav.findAll", query="SELECT f FROM FeiMxRelFmtFav f")
public class FeiMxRelFmtFav implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FEI_MX_REL_FMT_FAV_IDFAV_GENERATOR", sequenceName="SEQ_FEI_MX_FAV")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FEI_MX_REL_FMT_FAV_IDFAV_GENERATOR")
	@Column(name="ID_FAV", unique=true, nullable=false, precision=10)
	private long idFav;

	@Column(name="FCH_HOR_INI")
	private Timestamp fchHorIni;

	@Column(name="FCH_HOR_ULT_ACT")
	private Timestamp fchHorUltAct;

	@Column(name="NUM_EJE", length=10)
	private String numEje;

	@Column(name="USR_ID", length=10)
	private String usrId;

	//bi-directional many-to-one association to FeiMxMaeCatFmt
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FMT", insertable=false, updatable=false)
	private FeiMxMaeCatFmt feiMxMaeCatFmt;

	public FeiMxRelFmtFav() {
		// default constructor
	}

	public long getIdFav() {
		return this.idFav;
	}

	public void setIdFav(long idFav) {
		this.idFav = idFav;
	}

	public Timestamp getFchHorIni() {
		if(fchHorIni == null) {
			return null;
		}
		return (Timestamp) this.fchHorIni.clone();
	}

	public void setFchHorIni(Timestamp fchHorIni) {
		if(fchHorIni == null) {
			this.fchHorIni = null;
		}else {
			this.fchHorIni = (Timestamp) fchHorIni.clone();
		}
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public String getNumEje() {
		return this.numEje;
	}

	public void setNumEje(String numEje) {
		this.numEje = numEje;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public FeiMxMaeCatFmt getFeiMxMaeCatFmt() {
		return this.feiMxMaeCatFmt;
	}

	public void setFeiMxMaeCatFmt(FeiMxMaeCatFmt feiMxMaeCatFmt) {
		this.feiMxMaeCatFmt = feiMxMaeCatFmt;
	}

}