package mx.isban.api.eformats.utils;

public class ValidateDataTypeUtils {

	public static boolean isLongNullOrZero( Long value ){
		if( value == null || value == 0L){
			return true;
		}
		return false;
	}

	public static boolean isStringNullOrEmpty(String value) {
		if( value == null || value.isEmpty()){
			return true;
		}
		return false;
	}
}
