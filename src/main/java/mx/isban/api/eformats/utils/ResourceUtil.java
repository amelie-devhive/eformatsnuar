package mx.isban.api.eformats.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

public final class ResourceUtil {
	
	private static final Logger LOGGER = LogManager.getLogger(ResourceUtil.class);

	private ResourceUtil() {
	}

	public static ClassLoader getClassLoader() {
		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		if (classLoader == null) {
			classLoader = ResourceUtil.class.getClassLoader();
		}
		return classLoader;
	}

	public static List<URL> getResources(String fileName) {
		ClassLoader classLoader = getClassLoader();
		List<URL> resources = new ArrayList<URL>();
		try {
			Enumeration<URL> urls = classLoader.getResources(fileName);
			while (urls.hasMoreElements()) {
				resources.add(urls.nextElement());
			}
		} catch (IOException err) {
			resources.add(classLoader.getResource(fileName));
		}
		return resources;
	}

	public static Properties loadProperties(String fileName,
			String... fileNames) {
		Properties properties = new Properties();
		try {
			loadProperties(properties, fileName);
		} catch (RuntimeException err) {
			properties.clear();
		}
		if (fileNames != null) {
			for (String file : fileNames) {
				loadProperties(properties, file);
			}
		}
		return properties;
	}

	private static void loadProperties(Properties properties, String fileName) {
		try {
			List<URL> resources = getResources(fileName);
			for (URL resource : resources) {
				properties.load(resource.openStream());
			}
		} catch (IOException err) {
			properties.clear();
		}
	}

	public static String extractProperty(String propertyName,
			String propertiesFileName) {
		String extractedProperty = null;
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(propertiesFileName);
		try {
			properties.load(is);
			extractedProperty = properties.getProperty(propertyName);
		} catch (IOException e) {
			LOGGER.error("Cannot load property: " + propertyName, e);
		}
		return extractedProperty;
	}
	public static  <T> T convert(Object source, Class<T> targetType) throws InstantiationException, IllegalAccessException {
	    if (source == null)
	        return null;
	    T target = targetType.newInstance();
	    BeanUtils.copyProperties(source, target);
	    return target;
	    
	}
}