package mx.isban.api.eformats.utils;

import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;

import mx.isban.api.eformats.dto.ClienteDto;

/*
 * Make Sure this dependency is set in parent pom
 * 		<!-- https://jsoniter.com/ -->
		<dependency>
		    <groupId>com.jsoniter</groupId>
		    <artifactId>jsoniter</artifactId>
		    <version>0.9.19</version>
		</dependency>
 */
/**
 * Esta clase permite convertir un json string en formato 
 * provisto por 390 Santander en una lista de Clientes
 * @author Amelie
 *
 */
public class Parser390 {

	public static ClienteDto getClients(final String json, final String numCliente){
		Any any = JsonIterator.deserialize(json);
		Any personName = any.get("partyRec", 0, "partyInfo", "personPartyInfo", "personName");
		String givenName = personName.get("givenName").toString();
		String paternalName = personName.get("paternalName").toString();
		String maternalName = personName.get("maternalName").toString();
		
		String nombreCliente = givenName + " " + paternalName + " " + maternalName;
		ClienteDto clientes = new ClienteDto(numCliente, nombreCliente);
		
		return clientes;
	}
}
