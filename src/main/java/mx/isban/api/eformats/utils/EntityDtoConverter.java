package mx.isban.api.eformats.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.jsoniter.annotation.JsonProperty;

public class EntityDtoConverter {
	
	private static final Logger LOGGER = LogManager.getLogger(EntityDtoConverter.class);
	
	private EntityDtoConverter() {}

	public static <D, T> D convertEntityToDto(T entity, Class<D> dtoClass) {
		D dto = null;
		try {
			dto = dtoClass.newInstance();
			Field[] entityFields = entity.getClass().getDeclaredFields();
			for(Field entityField : entityFields) {
				if(entityField.isSynthetic() 
						|| Modifier.isStatic(entityField.getModifiers())
						|| Modifier.isFinal(entityField.getModifiers())
						|| Modifier.isTransient(entityField.getModifiers())) {
					continue;
				}
				Field[] dtoFields = dto.getClass().getDeclaredFields();
				setFields( dtoFields, entityField, dto, entity);
			}
		} catch (InstantiationException e) {
			LOGGER.error("instantiation exception convertToDto", e);
		} catch (IllegalAccessException e) {
			LOGGER.error("illegal access exception convertToDto", e);
		} catch (NoSuchFieldException e) {
			LOGGER.error("No such field exception convertToDto", e);
		} catch (SecurityException e) {
			LOGGER.error("security exception convertToDto", e);
		} catch (IllegalArgumentException e) {
			LOGGER.error("illegal argument exception convertToDto", e);
		}
		return dto;
	}
	
	private static void setFields( Field[] dtoFields, Field entityField, Object dto, Object entity ) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
		for(Field dtoField : dtoFields) {
			if(entityField.isSynthetic() 
					|| Modifier.isStatic(entityField.getModifiers())
					|| Modifier.isFinal(entityField.getModifiers())
					|| Modifier.isTransient(entityField.getModifiers())) {
				continue;
			}
			boolean isSameField = false;
			Object currEntity = entity;
			Field currEntityField = entityField;
			JsonProperty jsonProperty = dtoField.getAnnotation(JsonProperty.class);
			if(jsonProperty == null || jsonProperty.from() == null || jsonProperty.from().length == 0) {
				isSameField = dtoField.getName().equals(currEntityField.getName());
			}else if(isSameField = (jsonProperty.from()[jsonProperty.from().length-1]
					.equals(currEntityField.getName()))){
				for(int index = 0; index < jsonProperty.from().length - 1; index++) {
					boolean entityAccessible = currEntityField.isAccessible();
					currEntityField = currEntity.getClass()
							.getDeclaredField(jsonProperty.from()[index]); 
					currEntityField.setAccessible(true);
					currEntity = currEntityField.get(currEntity);
					currEntityField.setAccessible(entityAccessible);
				}
				currEntityField = currEntity.getClass()
						.getDeclaredField(jsonProperty.from()[jsonProperty.from().length - 1]);
			}
			if(isSameField) {
				boolean entityAccessible = currEntityField.isAccessible();
				boolean dtoAccessible = dtoField.isAccessible();
				
				currEntityField.setAccessible(true);
				dtoField.setAccessible(true);
				
				try {
					dtoField.set(dto, currEntityField.get(currEntity));
				} catch (IllegalAccessException e) {
					LOGGER.error("illegal access exception convertToDto",e);
				}

				currEntityField.setAccessible(entityAccessible);
				dtoField.setAccessible(dtoAccessible);
			}
		}
	}
}
