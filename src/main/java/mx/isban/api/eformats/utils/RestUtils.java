package mx.isban.api.eformats.utils;

import java.io.Serializable;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.error.EFormatsErrors;

public class RestUtils {
	
	private RestUtils() {}

	public static <S extends Serializable> ResponseEntity<Either<S, EFormatsError>> getResponse(S success, EFormatsErrors error){
		return getResponse(success, error, null);
	}

	public static <S extends Serializable> ResponseEntity<Either<S, EFormatsError>> getResponse(S success, EFormatsErrors error,
			HttpHeaders headers) {
		
		return error == null ? 
				(headers == null ?
						new ResponseEntity<Either<S, EFormatsError>>(Either.success(success, new EFormatsError()), HttpStatus.OK)
					:	new ResponseEntity<Either<S, EFormatsError>>(Either.success(success, new EFormatsError()), headers, HttpStatus.OK))
				: new ResponseEntity<Either<S, EFormatsError>>(Either.error(success, error.getValue()), error.getValue().getHttpStatus());
				
	}
	
	public static ResponseEntity<byte[]> getResponseFile(byte[] success, EFormatsErrors error,
			HttpHeaders headers) {
		return error == null ? 
				(headers == null ?
						new ResponseEntity<byte[]>(success, HttpStatus.OK)
					:	new ResponseEntity<byte[]>(success, headers, HttpStatus.OK))
				: new ResponseEntity(error.getValue().getHttpStatus());
				
	}

}
