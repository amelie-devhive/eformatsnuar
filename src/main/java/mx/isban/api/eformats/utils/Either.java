package mx.isban.api.eformats.utils;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class Either<S extends Serializable, E extends Serializable> implements Serializable{
	private static final long serialVersionUID = -5768433722854883920L;
	
	private S success;
	private E error;
	
	private Either() {
	}
	
	public static <S extends Serializable, E extends Serializable> Either<S, E> success(S success, E error) {
		Either<S, E> either = new Either<S, E>();
		either.success = success;
		either.error = error;
		either.error = null;
		return either;
	}
	
	public static <S extends Serializable, E extends Serializable> Either<S, E> error(S success, E error) {
		Either<S, E> either = new Either<S, E>();
		either.success = success;
		either.success = null;
		either.error = error;
		return either;
	}

	@Override
	public String toString() {
		return "Either [success=" + success + ", error=" + error + "]";
	}

	public S getSuccess() {
		return success;
	}

	public E getError() {
		return error;
	}
}
