package mx.isban.api.eformats.service.impl;

import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.TipoPerfilDto;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.model.FeiMxPrcCatTipoPerfi;
import mx.isban.api.eformats.repository.TipoPerfilRepository;
import mx.isban.api.eformats.service.DatosCatalogoBusiness;
import mx.isban.api.eformats.service.TipoPerfilBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.EntityDtoConverter;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class TipoPerfilBusinessImpl implements TipoPerfilBusiness {


	private static final Logger LOGGER = LogManager.getLogger(DatosCatalogoBusiness.class);

	@Autowired
	private TipoPerfilRepository tipoPerfilRepository;

	@Override
	public ResponseEntity<Either<ArrayList<TipoPerfilDto>, EFormatsError>> getTiposPerfil() {
		ArrayList<TipoPerfilDto> response = new ArrayList<>();
		EFormatsErrors error = null;
		try {
			Iterable<FeiMxPrcCatTipoPerfi> result = tipoPerfilRepository.findActive();
			if(result.iterator().hasNext()) {
				result.forEach(format -> 
				response.add(EntityDtoConverter.convertEntityToDto(format, TipoPerfilDto.class)));
			}else {
				error = EFormatsErrors.TIPO_PERFIL_NO_CONTENT;
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.TIPO_PERFIL_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		return RestUtils.getResponse(response, error);
	}

	
}
