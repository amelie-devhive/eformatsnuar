package mx.isban.api.eformats.service.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.ClienteDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.repository.ClienteRepository;
import mx.isban.api.eformats.service.ClienteBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class ClienteBusinessImpl implements ClienteBusiness {

//	@Autowired
	private static ClienteRepository clienteRepository = new ClienteRepository();

	@Override
	public ResponseEntity<Either<ClienteDto, EFormatsError>> getClientesBuc(
			String numClientePath, String numCliente) {
		if(numClientePath != null && !numClientePath.isEmpty()) {
			numCliente = numClientePath;
		}
		
		EFormatsErrors error = null;
		ClienteDto clienteDto = null;
		
		if(numCliente == null || numCliente.isEmpty()) {
			error = EFormatsErrors.CLIENTE_NULL_NUM_CLIENTE;
		}else {
			clienteDto = clienteRepository.buscarClienteBuc(numCliente);
			if(clienteDto == null || clienteDto.getNumCliente() == null || clienteDto.getNumCliente().isEmpty()) {
				error = EFormatsErrors.CLIENTE_NOT_FOUND_NUM_CTE;
			}
		}
		
		return RestUtils.getResponse(clienteDto, error);
	}

}
