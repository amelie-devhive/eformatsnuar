package mx.isban.api.eformats.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FormatDto;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.model.Format;
import mx.isban.api.eformats.repository.FormatRepository;
import mx.isban.api.eformats.service.FormatBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.EntityDtoConverter;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class FormatBusinessImpl implements FormatBusiness {

	private static final Logger LOGGER = LogManager.getLogger(FormatBusiness.class);

	@Autowired
	private FormatRepository formatRepository;

	@Override
	public ResponseEntity<Either<ArrayList<FormatDto>, EFormatsError>> getFormats(List<Long> request) {

		EFormatsErrors error = null;
		ArrayList<FormatDto> formatosDto = new ArrayList<>();
		Iterable<Format> result = null;
		try {
			if(request == null || request.isEmpty()) {
				result = formatRepository.findAll();
			}else {
				result = formatRepository.findAll(request);
			}
			if(result == null || !result.iterator().hasNext()) {
				error = EFormatsErrors.FORMAT_NOT_FOUND;
			}			
		}catch (RuntimeException e) {
			error = EFormatsErrors.FORMAT_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		if(result != null) {
			result.forEach(format -> formatosDto.add(EntityDtoConverter.convertEntityToDto(format, FormatDto.class)));
		}
		return RestUtils.getResponse(formatosDto, error);
	}


	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> saveFormat(
			FormatDto formatDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			error = saveOrUpdateFormat(formatDto);
			if( error == null ){
				response.setId("" + formatDto.getFormatId());
				response.setCodigo("001");
				response.setMensaje("Se guardó con éxito");
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.FORMAT_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(response, error);
	}

	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> updateFormat(
			FormatDto formatDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			if(formatDto == null 
					|| formatDto.getFormatId() == null || formatDto.getFormatId() == 0L
					|| formatDto.getFormatName() == null || formatDto.getFormatName().trim().isEmpty()) {
				error = EFormatsErrors.FORMAT_UPDATE_INVALID;
			}else {
				error = saveOrUpdateFormat(formatDto);
				if( error == null ){
					response.setId("" + formatDto.getFormatId());
					response.setCodigo("002");
					response.setMensaje("Se actualizó con éxito");
				}
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.FORMAT_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(response, error);
	}
	private EFormatsErrors saveOrUpdateFormat( FormatDto formatDto){
		Format format = formatRepository.findOne(formatDto.getFormatId());
		EFormatsErrors error = null;
		if(format == null) {
			error = EFormatsErrors.FORMAT_UPDATE_INEXISTENT;
		}else {
			format.setFormatName(formatDto.getFormatName().trim());
			Format saved = formatRepository.save(format);
			if(saved == null || saved.getFormatId() == 0) {
				error = EFormatsErrors.FORMAT_UPDATE_ERROR;
			}
		}
		return error;
	}
	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> deleteFormat(
			FormatDto formatDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			if(formatDto == null 
					|| formatDto.getFormatId() == null || formatDto.getFormatId() == 0L) {
				error = EFormatsErrors.FORMAT_DELETE_INVALID;
			}else {
				Format format = formatRepository.findOne(formatDto.getFormatId());
				formatRepository.delete(format);

				response.setId("" + formatDto.getFormatId());
				response.setCodigo("003");
				response.setMensaje("Se eliminó con éxito");					
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.FORMAT_DELETE_ERROR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(response, error);
	}
	
}
