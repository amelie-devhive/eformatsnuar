package mx.isban.api.eformats.service.impl;

import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.DatosCatalogosDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.model.FeiMxRelDatCat;
import mx.isban.api.eformats.repository.CatalogoRepository;
import mx.isban.api.eformats.service.DatosCatalogoBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.EntityDtoConverter;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class DatosCatalogoBusinessImpl implements DatosCatalogoBusiness {


	private static final Logger LOGGER = LogManager.getLogger(DatosCatalogoBusiness.class);

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Override
	public ResponseEntity<Either<ArrayList<DatosCatalogosDto>, EFormatsError>> getCatalogo(String cveCat) {
		ArrayList<DatosCatalogosDto> response = new ArrayList<>();
		EFormatsErrors error = null;
		try {
			Iterable<FeiMxRelDatCat> result = catalogoRepository.findByClaveCatalogo(cveCat);
			if(result.iterator().hasNext()) {
				result.forEach(format -> 
				response.add(EntityDtoConverter.convertEntityToDto(format, DatosCatalogosDto.class)));
			}else {
				error = EFormatsErrors.CAT_DATOS_NO_CONTENT;
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.CAT_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		return RestUtils.getResponse(response, error);
	}

	
}
