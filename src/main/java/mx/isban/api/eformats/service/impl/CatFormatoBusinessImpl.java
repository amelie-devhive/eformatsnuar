package mx.isban.api.eformats.service.impl;

import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.CatFormatoDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.model.FeiMxMaeCatFmt;
import mx.isban.api.eformats.repository.CatFormatoRepository;
import mx.isban.api.eformats.service.CatFormatoBusiness;
import mx.isban.api.eformats.service.DatosCatalogoBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.EntityDtoConverter;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class CatFormatoBusinessImpl implements CatFormatoBusiness {


	private static final Logger LOGGER = LogManager.getLogger(DatosCatalogoBusiness.class);

	@Autowired
	private CatFormatoRepository catFormatoRepository;

	@Override
	public ResponseEntity<Either<ArrayList<CatFormatoDto>, EFormatsError>> getFormatoSugerido(String keyword) {
		ArrayList<CatFormatoDto> response = new ArrayList<>();
		EFormatsErrors error = null;
		try {
			Iterable<FeiMxMaeCatFmt> result = catFormatoRepository.findLikeKeyword(keyword);
			if(result.iterator().hasNext()) {
				result.forEach(format -> 
				response.add(EntityDtoConverter.convertEntityToDto(format, CatFormatoDto.class)));
			}else {
				error = EFormatsErrors.CAT_FORMAT_NO_CONTENT;
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.CAT_FORMAT_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage(), e);
		}
		return RestUtils.getResponse(response, error);
	}

	
}
