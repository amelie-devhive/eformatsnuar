package mx.isban.api.eformats.service;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.ClienteDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.utils.Either;

public interface ClienteBusiness {

	ResponseEntity<Either<ClienteDto, EFormatsError>> getClientesBuc(
			String numClientePath, String numCliente);

}
