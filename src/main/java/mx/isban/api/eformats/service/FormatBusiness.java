package mx.isban.api.eformats.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FormatDto;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.utils.Either;

public interface FormatBusiness {

	ResponseEntity<Either<ArrayList<FormatDto>, EFormatsError>> getFormats(List<Long> request);

	ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> saveFormat(FormatDto formatDto);

	ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> updateFormat(FormatDto formatDto);

	ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> deleteFormat(FormatDto formatDto);

}
