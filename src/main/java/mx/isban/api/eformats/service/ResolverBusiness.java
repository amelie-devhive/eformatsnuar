package mx.isban.api.eformats.service;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.ResolverDto;
import mx.isban.api.eformats.utils.Either;

import org.springframework.http.ResponseEntity;

public interface ResolverBusiness {

	ResponseEntity<Either<ResolverDto, EFormatsError>> getUserInfo(
			ResolverDto resolverDto);
	
	ResponseEntity<Either<ResolverDto, EFormatsError>> getPuestoInfo(
			ResolverDto resolverDto);
	
	ResponseEntity<Either<ResolverDto, EFormatsError>> getCentroCostosInfo(
			ResolverDto resolverDto);

}
