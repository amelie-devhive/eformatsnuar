package mx.isban.api.eformats.service;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.BitacoraDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.utils.Either;

public interface BitacoraBusiness {


	public ResponseEntity<Either<ArrayList<BitacoraDto>, EFormatsError>> 
		getTop(String numEjecutivo, int topCount);
	
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
		saveBitacora(BitacoraDto bitacoraDto);
}
