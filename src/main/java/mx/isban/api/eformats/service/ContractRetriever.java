package mx.isban.api.eformats.service;

import org.springframework.http.ResponseEntity;

public interface ContractRetriever {

	ResponseEntity<byte[]> getContractFile(boolean inline);

}