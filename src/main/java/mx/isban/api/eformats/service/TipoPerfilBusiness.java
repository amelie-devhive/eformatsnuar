package mx.isban.api.eformats.service;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.TipoPerfilDto;
import mx.isban.api.eformats.utils.Either;

public interface TipoPerfilBusiness {

	ResponseEntity<Either<ArrayList<TipoPerfilDto>, EFormatsError>> getTiposPerfil();

}
