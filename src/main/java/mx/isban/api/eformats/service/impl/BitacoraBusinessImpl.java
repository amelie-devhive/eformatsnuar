package mx.isban.api.eformats.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.BitacoraDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.model.FeiMxLogBta;
import mx.isban.api.eformats.model.FeiMxMaeCatFmt;
import mx.isban.api.eformats.repository.BitacoraRepository;
import mx.isban.api.eformats.service.BitacoraBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.EntityDtoConverter;
import mx.isban.api.eformats.utils.RestUtils;
import mx.isban.api.eformats.utils.ValidateDataTypeUtils;

@Service
public class BitacoraBusinessImpl implements BitacoraBusiness {

	private static final Logger LOGGER = LogManager.getLogger(BitacoraBusiness.class);

	@Autowired
	private BitacoraRepository bitacoraRepository;
	
	@Override
	public ResponseEntity<Either<ArrayList<BitacoraDto>, EFormatsError>> getTop(String numEjecutivo, int topCount) {
		EFormatsErrors error = null;
		ArrayList<BitacoraDto> bitacorasDto = new ArrayList<>();
		try {
			if(numEjecutivo == null || numEjecutivo.isEmpty()) {
				error = EFormatsErrors.BITACORA_NULL_NUM_EJECUTIVO;
			}else {
				@SuppressWarnings("unchecked")
				List<FeiMxLogBta> result = bitacoraRepository.findTop(numEjecutivo, topCount);
				if(result == null || result.isEmpty()) {
					error = EFormatsErrors.BITACORA_NOT_FOUND_TOP;
				}else {
					result.forEach(bitacora -> bitacorasDto.add(EntityDtoConverter.convertEntityToDto(bitacora, BitacoraDto.class)));
				}
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.BITACORA_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		return RestUtils.getResponse(bitacorasDto, error);
	}

	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> saveBitacora(BitacoraDto bitacoraDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			if(bitacoraDto == null 
					||ValidateDataTypeUtils.isLongNullOrZero(bitacoraDto.getIdFormato())
					||ValidateDataTypeUtils.isStringNullOrEmpty(bitacoraDto.getEstatus())
					||ValidateDataTypeUtils.isStringNullOrEmpty( bitacoraDto.getNumEjecutivo())
					||ValidateDataTypeUtils.isStringNullOrEmpty( bitacoraDto.getUsrId())) {
				error = EFormatsErrors.BITACORA_INVALID_DATA_SAVING;
			}else {
				FeiMxLogBta bitacora = new FeiMxLogBta();
				bitacora.setFeiMxMaeCatFmt(new FeiMxMaeCatFmt(bitacoraDto.getIdFormato()));
				bitacora.setDscEstatFmt(bitacoraDto.getEstatus());
				bitacora.setFchHorIni(bitacoraDto.getFchHorInicio());
				bitacora.setFchHorTerm(bitacoraDto.getFchHorTerminado());
				bitacora.setFchHorUltAct(new Timestamp(System.currentTimeMillis()));
				bitacora.setNumEje(bitacoraDto.getNumEjecutivo());
				bitacora.setNumSuc(bitacoraDto.getNumSucursal());
				bitacora.setUsrId(bitacoraDto.getUsrId());
				
				FeiMxLogBta saved = bitacoraRepository.save(bitacora);
				response.setId("" + saved.getIdBta());
				response.setCodigo("001");
				response.setMensaje("Se guardó con éxito");
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.BITACORA_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		return RestUtils.getResponse(response, error);
	}

}
