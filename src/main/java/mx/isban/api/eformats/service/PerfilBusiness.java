package mx.isban.api.eformats.service;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.PerfilDto;
import mx.isban.api.eformats.utils.Either;

public interface PerfilBusiness {

	ResponseEntity<Either<ArrayList<PerfilDto>, EFormatsError>> getPerfilByTipoPerfil(Long idTipoPerfil);

}
