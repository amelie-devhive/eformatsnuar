package mx.isban.api.eformats.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FormatoMasUtilizadoDto;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.model.FeiMxRelFormatoMasUtilizado;
import mx.isban.api.eformats.repository.FormatoMasUtilizadoRepository;
import mx.isban.api.eformats.service.FavoritesBusiness;
import mx.isban.api.eformats.service.FormatoMasUtilizadoBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.EntityDtoConverter;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class FormatoMasUtilizadoBusinessImpl implements FormatoMasUtilizadoBusiness {

	private static final Logger LOGGER = LogManager.getLogger(FavoritesBusiness.class);

	@Autowired
	private FormatoMasUtilizadoRepository formatosMasUtilizadosRepository;

	@Override
	public ResponseEntity<Either<ArrayList<FormatoMasUtilizadoDto>, EFormatsError>> 
			getFormatoMasUtilizado(List<Long> request) {
		EFormatsErrors error = null;
		ArrayList<FormatoMasUtilizadoDto> formatoMasUtilizadoDto = new ArrayList<>();
		Iterable<FeiMxRelFormatoMasUtilizado> result = null;
		try {
			if(request != null && request.isEmpty()) {
				result = formatosMasUtilizadosRepository.findAll();
				if(result == null || !result.iterator().hasNext()) {
					error = EFormatsErrors.FAVORITE_NO_CONTENT;
				}
			}else {
				result = formatosMasUtilizadosRepository.findAll(request);
				if(result == null || !result.iterator().hasNext()) {
					error = EFormatsErrors.FAVORITE_NOT_FOUND;
				}
			}
			if(result != null) {
				result.forEach((formatoMasUtilizado) -> formatoMasUtilizadoDto.add(EntityDtoConverter.convertEntityToDto(formatoMasUtilizado, FormatoMasUtilizadoDto.class)));
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.FAVORITE_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(formatoMasUtilizadoDto, error);
	}


	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			saveFormatoMasUtilizado(FormatoMasUtilizadoDto formatoMasUtilizadoDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			if(formatoMasUtilizadoDto == null 
					|| formatoMasUtilizadoDto.getIdMasUtil() == 0L
					|| formatoMasUtilizadoDto.getNumEjecutivo() == null || formatoMasUtilizadoDto.getNumEjecutivo().isEmpty()
					|| formatoMasUtilizadoDto.getUsrId() == null || formatoMasUtilizadoDto.getUsrId().isEmpty()) {
				error = EFormatsErrors.FAVORITE_SAVE_INVALID;
			}else {
				FeiMxRelFormatoMasUtilizado formatoMasUtilizado = new FeiMxRelFormatoMasUtilizado();
				formatoMasUtilizado.setIdFormato(formatoMasUtilizadoDto.getIdFormato());
				formatoMasUtilizado.setFchHorUltimaActualizacion(new Timestamp(System.currentTimeMillis()));
				formatoMasUtilizado.setNumEjecutivo(formatoMasUtilizadoDto.getNumEjecutivo());
				formatoMasUtilizado.setUsrId(formatoMasUtilizadoDto.getUsrId());
				
				FeiMxRelFormatoMasUtilizado saved = formatosMasUtilizadosRepository.save(formatoMasUtilizado);
				if(saved == null || saved.getIdMasUtil() == 0) {
					error = EFormatsErrors.FAVORITE_SAVE_ERROR;
				}else {
					response.setId("" + saved.getIdMasUtil());
					response.setCodigo("001");
					response.setMensaje("Se guardó con éxito");
				}
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.FAVORITE_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		return RestUtils.getResponse(response, error);
	}

	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			updateFormatoMasUtilizado(FormatoMasUtilizadoDto formatoMasUtilizadoDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			if(formatoMasUtilizadoDto == null 
					|| formatoMasUtilizadoDto.getIdMasUtil() == 0L
					|| formatoMasUtilizadoDto.getIdFormato() == 0L) {
				error = EFormatsErrors.FAVORITE_UPDATE_INVALID;
			}else {
				FeiMxRelFormatoMasUtilizado formatoMasUtilizado = formatosMasUtilizadosRepository.findOne(formatoMasUtilizadoDto.getIdMasUtil());
				
				formatoMasUtilizado.setFchHorUltimaActualizacion(new Timestamp(System.currentTimeMillis()));
				if(formatoMasUtilizadoDto.getNumEjecutivo() != null) {
					formatoMasUtilizado.setNumEjecutivo(formatoMasUtilizadoDto.getNumEjecutivo());
				}
				if(formatoMasUtilizadoDto.getUsrId() != null) {
					formatoMasUtilizado.setUsrId(formatoMasUtilizadoDto.getUsrId());
				}
				
				FeiMxRelFormatoMasUtilizado saved = formatosMasUtilizadosRepository.save(formatoMasUtilizado);
				if(saved == null) {
					error = EFormatsErrors.FAVORITE_UPDATE_ERROR;
				}else {
					response.setId("" + saved.getIdMasUtil());
					response.setCodigo("002");
					response.setMensaje("Se actualizó con éxito");
				}
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.FAVORITE_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(response, error);
	}

	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			deleteFormatoMasUtilizado(FormatoMasUtilizadoDto FormatoMasUtilizadoDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			if(FormatoMasUtilizadoDto == null 
					|| FormatoMasUtilizadoDto.getIdMasUtil() == 0L) {
				error = EFormatsErrors.FAVORITE_DELETE_INVALID;
			}else {
				FeiMxRelFormatoMasUtilizado formatoMasUtilizado = formatosMasUtilizadosRepository.findOne(FormatoMasUtilizadoDto.getIdMasUtil());
				formatosMasUtilizadosRepository.delete(formatoMasUtilizado);
				if(formatosMasUtilizadosRepository.findOne(formatoMasUtilizado.getIdMasUtil()) == null) {
					response.setId("" + FormatoMasUtilizadoDto.getIdMasUtil());
					response.setCodigo("003");
					response.setMensaje("Se eliminó con éxito");
				}else {
					error = EFormatsErrors.FAVORITE_DELETE_ERROR;
				}
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.FAVORITE_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(response, error);
	}
	
}
