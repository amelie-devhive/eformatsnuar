package mx.isban.api.eformats.service;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FacultadDto;
import mx.isban.api.eformats.utils.Either;

public interface FacultadBusiness {

	ResponseEntity<Either<ArrayList<FacultadDto>, EFormatsError>> getFacultades(
			String numFactPath, String numFac, String cveFac);

	ResponseEntity<Either<ArrayList<FacultadDto>, EFormatsError>> getByTipoPerfil(@Valid long idPerfil);

	ResponseEntity<Either<ArrayList<FacultadDto>, EFormatsError>> getFacultadesExtraordinarias(@Valid long idTipoPerfil);

}
