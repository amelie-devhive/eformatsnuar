package mx.isban.api.eformats.service.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.DatosCatalogosDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.ResolverDto;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.service.DatosCatalogoBusiness;
import mx.isban.api.eformats.service.ResolverBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.RestUtils;
import mx.isban.api.eformats.utils.SslUtils;
import mx.isban.resolver.ws.RequestCorporativo;
import mx.isban.resolver.ws.RequestLocal;
import mx.isban.resolver.ws.ResolverConsultaServiceWS;
import mx.isban.resolver.ws.ResolverServices;
import mx.isban.resolver.ws.ResponseConsultaUsrLDAP;

@Service
public class ResolverBusinessImpl implements ResolverBusiness {

	private static final String WS_RESULT_OK = "OK01";
	private static final String WS_RESULT_ERROR = "NO SE ENCONTRARON DATOS DEL USUARIO";
	private static final String WS_CONSULTA_PROD = "1";
	private static final String CLAVE_CATALOGO_007 = "CAT-007";
	private static final java.net.URL WS_URL_PROD = makeUrl("https://wsresolver.mx.corp:443/ResolverWS/ResolverServices/ResolverServices.wsdl");
	private static final java.net.URL WS_URL_DEV = makeUrl("https://srvprtbdvlmx01.dev.mx.corp:8443/ResolverWS/ResolverServices/ResolverServices.wsdl");

	@Autowired
	private DatosCatalogoBusiness catalogoBusiness;
	
	public static java.net.URL makeUrl(String urlString) {
	    try {
	        return new java.net.URL(urlString);
	    } catch (java.net.MalformedURLException e) {
	        return null;
	    }
	}

	@Override
	public ResponseEntity<Either<ResolverDto, EFormatsError>> getPuestoInfo(
			ResolverDto resolverDto) {
		ResponseEntity<Either<ResolverDto, EFormatsError>> result = this.getUserInfo(resolverDto);
		ResolverDto resolverResponseDto = result.getBody().getSuccess();
		ResponseEntity<Either<ArrayList<DatosCatalogosDto>, EFormatsError>> resultListCatEither = catalogoBusiness.getCatalogo(CLAVE_CATALOGO_007);
		ArrayList<DatosCatalogosDto> resultListCat = resultListCatEither.getBody().getSuccess();
		for (DatosCatalogosDto datosCatalogosDto : resultListCat) {
			if( datosCatalogosDto.getNombreDato().trim().equals(resolverResponseDto.getIdPuesto().trim())){
				resolverResponseDto.setDescripcionPuesto(datosCatalogosDto.getDscAdicional());
				break;
			}
		}
		return RestUtils.getResponse(resolverResponseDto, null);
	}
	@Override
	public ResponseEntity<Either<ResolverDto, EFormatsError>> getCentroCostosInfo(
			ResolverDto resolverDto) {
		ResponseEntity<Either<ResolverDto, EFormatsError>> result = this.getUserInfo(resolverDto);
		ResolverDto resolverResponseDto = result.getBody().getSuccess();
//		ResponseEntity<Either<ArrayList<DatosCatalogosDto>, EFormatsError>> resultListCatEither = catalogoBusiness.getCatalogo(CLAVE_CATALOGO_007);
//		ArrayList<DatosCatalogosDto> resultListCat = resultListCatEither.getBody().getSuccess();
//		for (DatosCatalogosDto datosCatalogosDto : resultListCat) {
//			if( datosCatalogosDto.getNombreDato().trim().equals(resolverResponseDto.getIdPuesto().trim())){
//				resolverResponseDto.setDescripcionPuesto(datosCatalogosDto.getDscAdicional());
//				break;
//			}
//		}
		return RestUtils.getResponse(resolverResponseDto, null);
	}
	@Override
	public ResponseEntity<Either<ResolverDto, EFormatsError>> getUserInfo(
			ResolverDto resolverDto) {
		ResolverDto clienteDto = null;
		ResolverDto resolverResponseDto = new ResolverDto();
		
		try{
			
			SslUtils.disableCertAll();
			
			ResolverConsultaServiceWS resolverServiceWs = getResolverService( resolverDto.getAmbiente() );
			
			RequestCorporativo requestCorporativo = new RequestCorporativo();
	
			
			requestCorporativo.setClaveAplicativo(resolverDto.getClaveAplicativo());
			requestCorporativo.setUsuarioAplicativo(resolverDto.getUsuarioAplicativo());
			
			resolverResponseDto.setIdentificadorCorporativo(getValueResult(getCorporateUserFromLocalID(resolverServiceWs,resolverDto)));
			requestCorporativo.setIdentificadorCorporativo(resolverResponseDto.getIdentificadorCorporativo());
			
			if( resolverResponseDto.getIdentificadorCorporativo() == null ){
				
				requestCorporativo.setIdentificadorCorporativo(resolverDto.getIdentificadorLocal());
				resolverResponseDto.setIdentificadorLocal(getValueResult(resolverServiceWs.getLocalUserFromCorpID( requestCorporativo )));
				resolverResponseDto.setIdentificadorCorporativo(resolverDto.getIdentificadorLocal());
			}else{
				
				resolverResponseDto.setIdentificadorLocal(resolverDto.getIdentificadorLocal());
			}
			
			if( resolverResponseDto.getIdentificadorLocal() == null && resolverResponseDto.getIdentificadorCorporativo() == null){
				
				return RestUtils.getResponse(clienteDto, getMessageResult(WS_RESULT_ERROR));
			}
			
			resolverResponseDto.setNombreUsuario(getValueResult(resolverServiceWs.getCommonNameFromCorpID( requestCorporativo )));
			resolverResponseDto.setCorreo(getValueResult(resolverServiceWs.getEmailFromCorpID( requestCorporativo )));
			resolverResponseDto.setCentroCosto(getValueResult(resolverServiceWs.getLocationFromCorpID( requestCorporativo )));
			resolverResponseDto.setIdPuesto(getValueResult(resolverServiceWs.getPositionFromCorpID( requestCorporativo )));
		}catch(RuntimeException e ) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String resultadoConsulta = e.getMessage()+"\n";
			resultadoConsulta+= sw.toString();
			return RestUtils.getResponse(clienteDto, getMessageResult(resultadoConsulta));
		}
		return RestUtils.getResponse(resolverResponseDto, null);
	}
	private String getValueResult(final ResponseConsultaUsrLDAP responseConsultaUsrLDAP){
		
		if( responseConsultaUsrLDAP != null && responseConsultaUsrLDAP.getMensajeError() != null && responseConsultaUsrLDAP.getResultado().trim().equalsIgnoreCase(WS_RESULT_OK)){
			
			return responseConsultaUsrLDAP.getResultado().trim();
		}
		return null;
	}
	private EFormatsErrors getMessageResult(final String msjResult){
		EFormatsErrors.WS_RESOLVER_ERROR.getValue().setErrMsg(msjResult);
		return EFormatsErrors.WS_RESOLVER_ERROR;
	}
	
	private ResolverConsultaServiceWS getResolverService(String ambiente){
		if( ambiente.trim().equals(WS_CONSULTA_PROD)){
			
			return new ResolverServices(WS_URL_PROD).getResolverServicesPort();
		}
			
		return new ResolverServices(WS_URL_DEV).getResolverServicesPort();
		
	}
	private ResponseConsultaUsrLDAP getCorporateUserFromLocalID( ResolverConsultaServiceWS resolverServiceWs, ResolverDto resolverDto ){
		RequestLocal requestLocal = new RequestLocal();
		
		requestLocal.setClaveAplicativo(resolverDto.getClaveAplicativo());
		requestLocal.setIdentificadorLocal(resolverDto.getIdentificadorLocal());
		requestLocal.setUsuarioAplicativo(resolverDto.getUsuarioAplicativo());
		return resolverServiceWs.getCorporateUserFromLocalID( requestLocal );
	}
}
