package mx.isban.api.eformats.service;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.CatFormatoDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.utils.Either;

public interface CatFormatoBusiness {

	ResponseEntity<Either<ArrayList<CatFormatoDto>, EFormatsError>> getFormatoSugerido(String keyword);

}
