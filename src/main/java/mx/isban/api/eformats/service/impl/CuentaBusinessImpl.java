package mx.isban.api.eformats.service.impl;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.CuentaDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.repository.CuentasRepository;
import mx.isban.api.eformats.service.CuentaBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class CuentaBusinessImpl implements CuentaBusiness {

	private static final Logger LOGGER = LogManager.getLogger(CuentaBusiness.class);

//	@Autowired
	private static CuentasRepository cuentaRepository = new CuentasRepository();

	@Override
	public ResponseEntity<Either<CuentaDto, EFormatsError>> getCuenta(String numCuenta) {
		CuentaDto cuentaDto = null;
		EFormatsErrors error = null;
		
		try {
			if(numCuenta != null && !numCuenta.isEmpty()) {
				cuentaDto = cuentaRepository.buscarCuentaCheques(numCuenta);
				if(cuentaDto == null || cuentaDto.getNumCuenta() == null || cuentaDto.getNumCuenta().isEmpty()) {
					error = EFormatsErrors.CUENTA_NOT_FOUND_NUM_CTA;
				}
			}else {
				error = EFormatsErrors.CUENTA_NULL_NUM_CTA;
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.CUENTA_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(cuentaDto, error);
	}
	
	@Override
	public ResponseEntity<Either<CuentaDto, EFormatsError>> getCuentaByMovil(String numMovil) {
		CuentaDto cuentaDto = null;
		EFormatsErrors error = null;
		
		try {
			if(numMovil != null && !numMovil.isEmpty()) {
				cuentaDto = cuentaRepository.buscarCuentaConMovil(numMovil);
				if(cuentaDto == null || cuentaDto.getNumCuenta() == null || cuentaDto.getNumCuenta().isEmpty()) {
					error = EFormatsErrors.CUENTA_NOT_FOUND_MOVIL;
				}
			}else {
				error = EFormatsErrors.CUENTA_NULL_NUM_CTA;
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.CUENTA_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(cuentaDto, error);
	}

}
