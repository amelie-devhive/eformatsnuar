package mx.isban.api.eformats.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FormatoFavoritoDto;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.model.FeiMxRelFmtFav;
import mx.isban.api.eformats.repository.FavoritesRepository;
import mx.isban.api.eformats.service.FavoritesBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.EntityDtoConverter;
import mx.isban.api.eformats.utils.RestUtils;
import mx.isban.api.eformats.utils.ValidateDataTypeUtils;

@Service
public class FavoritesBusinessImpl implements FavoritesBusiness {

	private static final Logger LOGGER = LogManager.getLogger(FavoritesBusiness.class);

	@Autowired
	private FavoritesRepository favoritesRepository;

	@Override
	public ResponseEntity<Either<ArrayList<FormatoFavoritoDto>, EFormatsError>> 
			getFormatosFavoritos(List<Long> request) {
		EFormatsErrors error = null;
		ArrayList<FormatoFavoritoDto> favoritosDto = new ArrayList<>();
		Iterable<FeiMxRelFmtFav> result = null;
		try {
			if(request != null && request.isEmpty()) {
				result = favoritesRepository.findAll();
				if(result == null || !result.iterator().hasNext()) {
					error = EFormatsErrors.FAVORITE_NO_CONTENT;
				}
			}else {
				result = favoritesRepository.findAll(request);
				if(result == null || !result.iterator().hasNext()) {
					error = EFormatsErrors.FAVORITE_NOT_FOUND;
				}
			}
			if(result != null) {
				result.forEach(favorito -> favoritosDto.add(EntityDtoConverter.convertEntityToDto(favorito, FormatoFavoritoDto.class)));
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.FAVORITE_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(favoritosDto, error);
	}


	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			saveFavorito(FormatoFavoritoDto formatoFavoritoDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			if(formatoFavoritoDto == null 
					|| formatoFavoritoDto.getFchHorInicio() == null
					|| ValidateDataTypeUtils.isLongNullOrZero(formatoFavoritoDto.getIdFormato())
					|| ValidateDataTypeUtils.isStringNullOrEmpty(formatoFavoritoDto.getNumEjecutivo())
					|| ValidateDataTypeUtils.isStringNullOrEmpty(formatoFavoritoDto.getUsrId())) {
				error = EFormatsErrors.FAVORITE_SAVE_INVALID;
			}else {
				FeiMxRelFmtFav favorito = new FeiMxRelFmtFav();
				// FIXME falta el id formato en la tabla
//				favorito.set(formatoFavoritoDto.getIdFormato());
				favorito.setFchHorIni(formatoFavoritoDto.getFchHorInicio());
				favorito.setFchHorUltAct(new Timestamp(System.currentTimeMillis()));
				favorito.setNumEje(formatoFavoritoDto.getNumEjecutivo());
				favorito.setUsrId(formatoFavoritoDto.getUsrId());
				
				FeiMxRelFmtFav saved = favoritesRepository.save(favorito);
				if(saved == null || saved.getIdFav() == 0) {
					error = EFormatsErrors.FAVORITE_SAVE_ERROR;
				}else {
					response.setId("" + saved.getIdFav());
					response.setCodigo("001");
					response.setMensaje("Se guardó con éxito");
				}
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.FAVORITE_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		return RestUtils.getResponse(response, error);
	}

	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			updateFormatoFavorito(FormatoFavoritoDto formatoFavoritoDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			if(formatoFavoritoDto == null 
					|| formatoFavoritoDto.getIdFavorito() == null || formatoFavoritoDto.getIdFavorito() == 0L
					|| formatoFavoritoDto.getIdFormato() == null || formatoFavoritoDto.getIdFormato() == 0L) {
				error = EFormatsErrors.FAVORITE_UPDATE_INVALID;
			}else {
				FeiMxRelFmtFav formatoFavorito = favoritesRepository.findOne(formatoFavoritoDto.getIdFavorito());
				
				if(formatoFavoritoDto.getFchHorInicio() != null) {
					formatoFavorito.setFchHorIni(formatoFavoritoDto.getFchHorInicio());
				}
				formatoFavorito.setFchHorUltAct(new Timestamp(System.currentTimeMillis()));
				if(formatoFavoritoDto.getNumEjecutivo() != null) {
					formatoFavorito.setNumEje(formatoFavoritoDto.getNumEjecutivo());
				}
				if(formatoFavoritoDto.getUsrId() != null) {
					formatoFavorito.setUsrId(formatoFavoritoDto.getUsrId());
				}
				
				FeiMxRelFmtFav saved = favoritesRepository.save(formatoFavorito);
				if(saved == null) {
					error = EFormatsErrors.FAVORITE_UPDATE_ERROR;
				}else {
					response.setId("" + saved.getIdFav());
					response.setCodigo("002");
					response.setMensaje("Se actualizó con éxito");
				}
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.FAVORITE_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(response, error);
	}

	@Override
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			deleteFormat(FormatoFavoritoDto formatoFavoritoDto) {
		OperationResultResponseDto response = new OperationResultResponseDto();
		EFormatsErrors error = null;
		try {
			if(formatoFavoritoDto == null 
					|| formatoFavoritoDto.getIdFavorito() == null || formatoFavoritoDto.getIdFavorito() == 0L) {
				error = EFormatsErrors.FAVORITE_DELETE_INVALID;
			}else {
				FeiMxRelFmtFav favorito = favoritesRepository.findOne(formatoFavoritoDto.getIdFavorito());
				favoritesRepository.delete(favorito);
				if(favoritesRepository.findOne(favorito.getIdFav()) == null) {
					response.setId("" + formatoFavoritoDto.getIdFavorito());
					response.setCodigo("003");
					response.setMensaje("Se eliminó con éxito");
				}else {
					error = EFormatsErrors.FAVORITE_DELETE_ERROR;
				}
			}
		} catch (RuntimeException e) {
			error = EFormatsErrors.FAVORITE_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}

		return RestUtils.getResponse(response, error);
	}
	
}
