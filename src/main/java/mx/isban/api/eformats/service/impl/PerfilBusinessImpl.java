package mx.isban.api.eformats.service.impl;

import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.PerfilDto;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.model.FeiMxPrcCatPerfi;
import mx.isban.api.eformats.repository.PerfilRepository;
import mx.isban.api.eformats.service.DatosCatalogoBusiness;
import mx.isban.api.eformats.service.PerfilBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.EntityDtoConverter;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class PerfilBusinessImpl implements PerfilBusiness {


	private static final Logger LOGGER = LogManager.getLogger(DatosCatalogoBusiness.class);

	@Autowired
	private PerfilRepository perfilRepository;

	@Override
	public ResponseEntity<Either<ArrayList<PerfilDto>, EFormatsError>> getPerfilByTipoPerfil(Long idTipoPerfil) {
		ArrayList<PerfilDto> response = new ArrayList<>();
		EFormatsErrors error = null;
		try {
			Iterable<FeiMxPrcCatPerfi> result = null;
			if(idTipoPerfil == null) {
				result = perfilRepository.findAll();
			}else {
				result = perfilRepository.findByTipoPerfil(idTipoPerfil);
			}
			if(result.iterator().hasNext()) {
				result.forEach(format -> 
				response.add(EntityDtoConverter.convertEntityToDto(format, PerfilDto.class)));
			}else {
				error = EFormatsErrors.PERFIL_NO_CONTENT;
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.PERFIL_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		return RestUtils.getResponse(response, error);
	}

	
}
