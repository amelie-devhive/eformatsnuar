package mx.isban.api.eformats.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FormatoMasUtilizadoDto;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.utils.Either;

public interface FormatoMasUtilizadoBusiness {

	ResponseEntity<Either<ArrayList<FormatoMasUtilizadoDto>, EFormatsError>> getFormatoMasUtilizado(List<Long> request);

	ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> saveFormatoMasUtilizado(
			FormatoMasUtilizadoDto formatoMasUtilizadoDto);

	ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> updateFormatoMasUtilizado(
			FormatoMasUtilizadoDto formatoMasUtilizadoDto);

	ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> deleteFormatoMasUtilizado(
			FormatoMasUtilizadoDto formatoMasUtilizadoDto);

}
