package mx.isban.api.eformats.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FacultadDto;
import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.model.FeiMxPrcCatFact;
import mx.isban.api.eformats.repository.CatFacultadesRepository;
import mx.isban.api.eformats.service.FacultadBusiness;
import mx.isban.api.eformats.utils.Either;
import mx.isban.api.eformats.utils.EntityDtoConverter;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class FacultadBusinessImpl implements FacultadBusiness {

	private static final Logger LOGGER = LogManager.getLogger(FacultadBusiness.class);

	@Autowired
	private CatFacultadesRepository catFacultadesRepository;

	@Override
	public ResponseEntity<Either<ArrayList<FacultadDto>, EFormatsError>> getFacultades(
			String numFactPath, String numFac, String cveFac) {
		if(numFactPath != null && !numFactPath.isEmpty()) {
			numFac = numFactPath;
		}
		
		EFormatsErrors error = null;
		ArrayList<FacultadDto> facultadesDto = new ArrayList<>();
		List<FeiMxPrcCatFact> facultades = null;
		
		try {
			if(numFac != null && !numFac.isEmpty()) {
				if(cveFac != null && !cveFac.isEmpty()) {
					facultades = catFacultadesRepository.findByNumFacCveFac(numFac, cveFac);
				}else {
					facultades = catFacultadesRepository.findByNumFac(numFac);
				}
			}else {
				if(cveFac == null || cveFac.isEmpty()) {
					facultades = catFacultadesRepository.findAll();
				}else {
					error = EFormatsErrors.CAT_FACULTAD_NULL_NUM_FAC;
				}
			}
			if(facultades == null) {
				error = EFormatsErrors.CAT_DATOS_NO_CONTENT;
			}else {
				facultades.forEach(f -> facultadesDto.add(EntityDtoConverter.convertEntityToDto(f, FacultadDto.class)));
			}
		}catch (RuntimeException e) {
			error = EFormatsErrors.CAT_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		
		return RestUtils.getResponse(facultadesDto, error);
	}

	@Override
	public ResponseEntity<Either<ArrayList<FacultadDto>, EFormatsError>> getByTipoPerfil(@Valid long idPerfil) {
		EFormatsErrors error = null;
		ArrayList<FacultadDto> facultadesDto = new ArrayList<>();
		
		try {
			List<FeiMxPrcCatFact> facultades = catFacultadesRepository.findByIdPerfil(idPerfil);
			if(facultades == null) {
				error = EFormatsErrors.CAT_DATOS_NO_CONTENT;
			}else {
				facultades.forEach(f -> facultadesDto.add(EntityDtoConverter.convertEntityToDto(f, FacultadDto.class)));
			}
		}catch (Exception e) {
			error = EFormatsErrors.CAT_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		
		return RestUtils.getResponse(facultadesDto, error);
	}

	@Override
	public ResponseEntity<Either<ArrayList<FacultadDto>, EFormatsError>> getFacultadesExtraordinarias(@Valid long idTipoPerfil) {
		EFormatsErrors error = null;
		ArrayList<FacultadDto> facultadesDto = new ArrayList<>();
		
		try {
			List<FeiMxPrcCatFact> facultades = catFacultadesRepository.findExtraordinarias(idTipoPerfil);
			if(facultades == null) {
				error = EFormatsErrors.CAT_DATOS_NO_CONTENT;
			}else {
				facultades.forEach(f -> facultadesDto.add(EntityDtoConverter.convertEntityToDto(f, FacultadDto.class)));
			}
		}catch (Exception e) {
			error = EFormatsErrors.CAT_DB_UNKNOWN_ERR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		
		return RestUtils.getResponse(facultadesDto, error);
	}

}
