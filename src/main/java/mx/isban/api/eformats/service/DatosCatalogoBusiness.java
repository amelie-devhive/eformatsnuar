package mx.isban.api.eformats.service;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.DatosCatalogosDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.utils.Either;

public interface DatosCatalogoBusiness {

	ResponseEntity<Either<ArrayList<DatosCatalogosDto>, EFormatsError>> getCatalogo(String cveCat);

}
