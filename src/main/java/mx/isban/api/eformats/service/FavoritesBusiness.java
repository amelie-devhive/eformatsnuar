package mx.isban.api.eformats.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FormatoFavoritoDto;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.utils.Either;

public interface FavoritesBusiness {

	ResponseEntity<Either<ArrayList<FormatoFavoritoDto>, EFormatsError>> getFormatosFavoritos(List<Long> request);

	ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> saveFavorito(
			FormatoFavoritoDto formatoFavoritoDto);

	ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> updateFormatoFavorito(
			FormatoFavoritoDto formatoFavoritoDto);

	ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> deleteFormat(
			FormatoFavoritoDto formatoFavoritoDto);

}
