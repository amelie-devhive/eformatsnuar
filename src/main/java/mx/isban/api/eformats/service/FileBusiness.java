package mx.isban.api.eformats.service;

import org.springframework.http.ResponseEntity;

public interface FileBusiness {

	ResponseEntity<byte[]> getFacultades(String filename, boolean base64, boolean inline);

}
