package mx.isban.api.eformats.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
//import org.springframework.http.ContentDisposition;


import mx.isban.api.eformats.error.EFormatsErrors;
import mx.isban.api.eformats.service.FileBusiness;
import mx.isban.api.eformats.utils.RestUtils;

@Service
public class FileBusinessImpl implements FileBusiness {

	private static final Logger LOGGER = LogManager.getLogger(FileBusiness.class);

	@Value("${eformats.filedepot}")
	private String rootPath;
	
	@Override
	public ResponseEntity<byte[]> getFacultades(
			String filename, boolean base64, boolean inline) {
		EFormatsErrors error = null;
		byte[] fileContent = null;
		HttpHeaders headers = null;
		try {
			if(filename == null || filename.isEmpty()) {
				error = EFormatsErrors.FILE_INVALID_NAME;
			}else {
				Path path = Paths.get(rootPath, filename);
				fileContent = Files.readAllBytes(path);

				String inlineOrAttach = inline ? "inline" : "attachment";
				
				headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_PDF);
				headers.setContentDispositionFormData(inlineOrAttach, filename);
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				if(inline) {
//					headers.setContentDisposition(ContentDisposition.parse("inline"));
				}
			}
		}catch (RuntimeException | IOException e) {
			error = EFormatsErrors.FILE_UNKNOWN_ERROR;
			LOGGER.error(error.getValue().getHashErr() + " :: " + e.getMessage());
		}
		return RestUtils.getResponseFile(fileContent, error, headers);
	}

}
