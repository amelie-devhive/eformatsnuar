package mx.isban.api.eformats.service;

import org.springframework.http.ResponseEntity;

import mx.isban.api.eformats.dto.CuentaDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.utils.Either;

public interface CuentaBusiness {

	ResponseEntity<Either<CuentaDto, EFormatsError>> getCuenta(String numCuenta);

	ResponseEntity<Either<CuentaDto, EFormatsError>> getCuentaByMovil(String numMovil);
	
}
