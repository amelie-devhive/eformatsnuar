package mx.isban.api.eformats.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FormatDto;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.service.FormatBusiness;
import mx.isban.api.eformats.utils.Either;

@RestController
@RequestMapping(value= {"/format", "/formato"}, produces=MediaType.APPLICATION_JSON_VALUE)
public class FormatsController {

	@Autowired
	private FormatBusiness formatBusiness;

	@GetMapping
	@ResponseBody
	public ResponseEntity<Either<ArrayList<FormatDto>, EFormatsError>> getFormats(
			@RequestParam(value = "formatId", required=false, defaultValue="") List<Long> request) {
		return formatBusiness.getFormats(request);
	}


	@PostMapping
	@ResponseBody
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> saveFormat(
			@RequestBody FormatDto formatDto) {
		return formatBusiness.saveFormat(formatDto);
	}

	@PutMapping
	@ResponseBody
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> updateFormat(
			@RequestBody FormatDto formatDto) {
		return formatBusiness.updateFormat(formatDto);
	}

	@DeleteMapping
	@ResponseBody
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> deleteFormat(
			@RequestBody FormatDto formatDto) {
		return formatBusiness.deleteFormat(formatDto);
	}
}
