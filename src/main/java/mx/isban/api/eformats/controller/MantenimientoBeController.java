package mx.isban.api.eformats.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.BitacoraDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.service.BitacoraBusiness;
import mx.isban.api.eformats.utils.Either;

@Deprecated
@RestController
@RequestMapping("/mantenimientobe")
public class MantenimientoBeController {

	@Autowired
	private BitacoraBusiness bitacoraBusiness;

	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Either<ArrayList<BitacoraDto>, EFormatsError>> getTop(
			@RequestParam(value = "numEjecutivo", required = true, defaultValue = "") String numEjecutivo, 
			@RequestParam(value = "top", required = false, defaultValue = "5") int topCount) {
		return bitacoraBusiness.getTop(numEjecutivo, topCount);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>>  saveBitacora(@RequestBody BitacoraDto bitacoraDto) {
		return bitacoraBusiness.saveBitacora(bitacoraDto);
	}

}
