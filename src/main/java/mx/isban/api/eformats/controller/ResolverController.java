package mx.isban.api.eformats.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.ResolverDto;
import mx.isban.api.eformats.service.ResolverBusiness;
import mx.isban.api.eformats.utils.Either;

/**
* Clase que expone los servicios rest para la consulta de datos del usuario
* corporativo o local santander comunicandose a traves del WS resolver
*
* @author  Hector Copca
* @version 1.0
* @since   2018-10-20
*/
@RestController
@RequestMapping(value="/resolver", produces=MediaType.APPLICATION_JSON_VALUE)
public class ResolverController {
	
	@Autowired
	private ResolverBusiness resolverBusiness;

	/**
	* Servicio rest de tipo GET para la consulta de datos del usuario
	* corporativo o local santander
	*
	* @author  Hector Copca
	* @version 1.0
	* @since   2018-10-20
	*/
	@GetMapping(value = "/consultaDatosResolverGet/",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Either<ResolverDto, EFormatsError>> consultaDatosResolverGet(
			@RequestParam(value = "claveAplicativo", required = false) String claveAplicativo, 
			@RequestParam(value = "usuarioAplicativo", required = false) String usuarioAplicativo,
			@RequestParam(value = "identificador", required = true) String identificador,
			@RequestParam(value = "ambiente", required = true, defaultValue = "0") String ambiente) {
		
		ResolverDto resolverDto = new ResolverDto();
		resolverDto.setClaveAplicativo(claveAplicativo);
		resolverDto.setIdentificadorLocal(identificador);
		resolverDto.setUsuarioAplicativo(usuarioAplicativo);
		resolverDto.setAmbiente(ambiente);
		
		return resolverBusiness.getUserInfo(resolverDto);
	}
	/**
	 * Servicio rest de tipo GET para la consulta de puestos
	 * corporativo o local santander
	 *
	 * @author  Hector Copca
	 * @version 1.0
	 * @since   2018-10-30
	 */
	@GetMapping(value = "/consultaPuestosGet/",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Either<ResolverDto, EFormatsError>> consultaPuestos(
			@RequestParam(value = "identificador", required = true) String identificador,
			@RequestParam(value = "ambiente", required = true, defaultValue = "0") String ambiente) {
		
		ResolverDto resolverDto = new ResolverDto();
		resolverDto.setIdentificadorLocal(identificador);
		resolverDto.setAmbiente(ambiente);
		
		return resolverBusiness.getPuestoInfo(resolverDto);
	}
	/**
	 * Servicio rest de tipo GET para la consulta de centro de costos
	 * corporativo o local santander
	 *
	 * @author  Hector Copca
	 * @version 1.0
	 * @since   2018-10-30
	 */
	@GetMapping(value = "/consultaCentroCostosGet/",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Either<ResolverDto, EFormatsError>> consultaCentroCostos(
			@RequestParam(value = "identificador", required = true) String identificador,
			@RequestParam(value = "ambiente", required = true, defaultValue = "0") String ambiente) {
		
		ResolverDto resolverDto = new ResolverDto();
		resolverDto.setIdentificadorLocal(identificador);
		resolverDto.setAmbiente(ambiente);
		
		return resolverBusiness.getCentroCostosInfo(resolverDto);
	}
	/**
	* Servicio rest de tipo POST para la consulta de datos del usuario
	* corporativo o local santander
	*
	* @author  Hector Copca
	* @version 1.0
	* @since   2018-10-20
	*/
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Either<ResolverDto, EFormatsError>> consultaDatosResolverPost(
			@RequestBody ResolverDto resolverDto) {
		return resolverBusiness.getUserInfo(resolverDto);
	}
}
