package mx.isban.api.eformats.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.service.FileBusiness;

@RestController
@RequestMapping("/otf")
public class FileDownloaderController {

	@Autowired
	private FileBusiness fileBusiness;
	
	@GetMapping(path = {"/{filename}"}, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<byte[]> getFacultades(
			@PathVariable(value = "filename", required = true) String filename,
			@RequestParam(value = "base64", required=false, defaultValue="false") boolean base64,
			@RequestParam(value = "inline", required=false, defaultValue="false") boolean inline) {
		return fileBusiness.getFacultades(filename, base64, inline);
	}

}
