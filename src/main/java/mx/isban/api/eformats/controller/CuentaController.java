package mx.isban.api.eformats.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.CuentaDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.service.CuentaBusiness;
import mx.isban.api.eformats.utils.Either;

/**
 * BE-046 / BE-230	Número de cuenta
 * @author Amelie
 *
 */
@RestController
@RequestMapping(value="/cuenta", produces=MediaType.APPLICATION_JSON_VALUE)
public class CuentaController {

	@Autowired
	private CuentaBusiness cuentaBusiness;

	@GetMapping(path = {"/{numCuenta}", ""}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<Either<CuentaDto, EFormatsError>> getCuenta(
			@PathVariable(value = "numCuenta", required=false) String numCuenta) {
		return cuentaBusiness.getCuenta(numCuenta);
	}

}
