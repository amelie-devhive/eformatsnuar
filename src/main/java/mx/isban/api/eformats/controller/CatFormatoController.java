package mx.isban.api.eformats.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.CatFormatoDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.service.CatFormatoBusiness;
import mx.isban.api.eformats.utils.Either;

@RestController
@RequestMapping(value="/catFormato", produces=MediaType.APPLICATION_JSON_VALUE)
public class CatFormatoController {

	@Autowired
	private CatFormatoBusiness catFormatoBusiness;

	@GetMapping(path="/{keyword}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<Either<ArrayList<CatFormatoDto>, EFormatsError>> getFormatoSugerido(
			@Valid @PathVariable(value = "keyword", required = true) String keyword) {
		return catFormatoBusiness.getFormatoSugerido(keyword);
	}

}
