package mx.isban.api.eformats.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.ClienteDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.service.ClienteBusiness;
import mx.isban.api.eformats.utils.Either;

/**
 * BE-046 / BE-230	Código de cliente o BUC
 * @author Amelie
 *
 */
@RestController
@RequestMapping(value="/cliente", produces=MediaType.APPLICATION_JSON_VALUE)
public class ClienteController {

	@Autowired
	private ClienteBusiness clienteBusiness;

	@GetMapping(path = {"/{numClientePath}", ""}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<Either<ClienteDto, EFormatsError>> getClientesBuc(
			@PathVariable(value = "numClientePath", required=false) String numClientePath,
			@RequestParam(value = "numCliente", required=false) String numCliente) {
		return clienteBusiness.getClientesBuc(numClientePath, numCliente);
	}

}
