package mx.isban.api.eformats.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.PerfilDto;
import mx.isban.api.eformats.service.PerfilBusiness;
import mx.isban.api.eformats.utils.Either;

@RestController
@RequestMapping(value="/perfil", produces=MediaType.APPLICATION_JSON_VALUE)
public class PerfilController  {

	@Autowired
	private PerfilBusiness perfilBusiness;

	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<Either<ArrayList<PerfilDto>, EFormatsError>> getPerfilByTipoPerfil(
			@Valid @RequestParam(value = "idTipoPerfil", defaultValue = "") Long idTipoPerfil) {
		return perfilBusiness.getPerfilByTipoPerfil(idTipoPerfil);
	}

}
