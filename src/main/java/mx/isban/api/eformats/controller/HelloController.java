package mx.isban.api.eformats.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/hello", produces=MediaType.APPLICATION_JSON_VALUE)
public class HelloController {
	
	@Value("${spring.application.buildtime}")
	private Object timestamp;

	@CrossOrigin(origins = "http://ec2-13-56-168-103.us-west-1.compute.amazonaws.com")
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> sayHello()  {
		ResponseEntity<String> response = new ResponseEntity<String>(
				"{ \"saludo\" : \"hola mundo\", \"tag\" : \"" + timestamp + "\" }",
				HttpStatus.OK);
		return response;
	}
}
