package mx.isban.api.eformats.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.TipoPerfilDto;
import mx.isban.api.eformats.service.TipoPerfilBusiness;
import mx.isban.api.eformats.utils.Either;

@RestController
@RequestMapping(value="/tipoPerfil", produces=MediaType.APPLICATION_JSON_VALUE)
public class TipoPerfilController {

	@Autowired
	private TipoPerfilBusiness tipoPerfilBusiness;

	/**
	 * Consulta los tipos de perfiles de la DB
	 * @return
	 */
	@GetMapping
	@ResponseBody
	public ResponseEntity<Either<ArrayList<TipoPerfilDto>, EFormatsError>> getCatalogo() {
		return tipoPerfilBusiness.getTiposPerfil();
	}

}
