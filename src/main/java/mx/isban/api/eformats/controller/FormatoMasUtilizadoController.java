package mx.isban.api.eformats.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FormatoMasUtilizadoDto;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.service.FormatoMasUtilizadoBusiness;
import mx.isban.api.eformats.utils.Either;

@RestController
@RequestMapping(value="/formatoMasUtilizado", produces=MediaType.APPLICATION_JSON_VALUE)
public class FormatoMasUtilizadoController {

	@Autowired
	private FormatoMasUtilizadoBusiness formatoMasUtilizadoBusiness;

	@GetMapping
	@ResponseBody
	public ResponseEntity<Either<ArrayList<FormatoMasUtilizadoDto>, EFormatsError>> 
			getFormatoMasUtilizado(@RequestParam(value="idFavorito", required=false, defaultValue="") List<Long> request) {
		return formatoMasUtilizadoBusiness.getFormatoMasUtilizado(request);
	}


	@PostMapping
	@ResponseBody
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			saveFormatoMasUtilizado(@RequestBody FormatoMasUtilizadoDto formatoMasUtilizadoDto) {
		return formatoMasUtilizadoBusiness.saveFormatoMasUtilizado(formatoMasUtilizadoDto);
	}

	@PutMapping
	@ResponseBody
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			updateFormatoFavorito(@RequestBody FormatoMasUtilizadoDto formatoMasUtilizadoDto) {
		return formatoMasUtilizadoBusiness.updateFormatoMasUtilizado(formatoMasUtilizadoDto);
	}

	@DeleteMapping
	@ResponseBody
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			deleteFormat(@RequestBody FormatoMasUtilizadoDto formatoMasUtilizadoDto) {
		return formatoMasUtilizadoBusiness.deleteFormatoMasUtilizado(formatoMasUtilizadoDto);
	}
}
