package mx.isban.api.eformats.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FacultadDto;
import mx.isban.api.eformats.service.FacultadBusiness;
import mx.isban.api.eformats.utils.Either;

@RestController
@RequestMapping(value="/facultad", produces=MediaType.APPLICATION_JSON_VALUE)
public class FacultadController {

	@Autowired
	private FacultadBusiness facultadBusiness;

	@GetMapping(path = {"/{numFactPath}", ""}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<Either<ArrayList<FacultadDto>, EFormatsError>> getFacultades(
			@PathVariable(value = "numFactPath", required = false) String numFactPath,
			@RequestParam(value = "numFac", required=false, defaultValue="") String numFac,
			@RequestParam(value = "cveFac", required=false, defaultValue="") String cveFac) {
		return facultadBusiness.getFacultades(numFactPath, numFac, cveFac);
	}

	@GetMapping(path = {"/perfil/{idPerfil}", ""}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<Either<ArrayList<FacultadDto>, EFormatsError>> getFacultadByPerfil(
			@Valid @PathVariable(value = "idPerfil", required = true) long idPerfil) {
		return facultadBusiness.getByTipoPerfil(idPerfil);
	}

	@GetMapping(path = {"/extraordinaria/{idTipoPerfil}", ""}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<Either<ArrayList<FacultadDto>, EFormatsError>> getFacultadesExtraordinarias(
			@Valid @PathVariable(value = "idTipoPerfil", required = true) long idTipoPerfil) {
		return facultadBusiness.getFacultadesExtraordinarias(idTipoPerfil);
	}

}
