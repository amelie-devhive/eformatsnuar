package mx.isban.api.eformats.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.DatosCatalogosDto;
import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.service.DatosCatalogoBusiness;
import mx.isban.api.eformats.utils.Either;

@RestController
@RequestMapping(value="/catalogo", produces=MediaType.APPLICATION_JSON_VALUE)
public class DatosCatalogosController {

	@Autowired
	private DatosCatalogoBusiness catalogoBusiness;

	@GetMapping
	@ResponseBody
	public ResponseEntity<Either<ArrayList<DatosCatalogosDto>, EFormatsError>> getCatalogo(@RequestParam(value = "cveCat", required=true) String cveCat) {
		return catalogoBusiness.getCatalogo(cveCat);
	}
	
}
