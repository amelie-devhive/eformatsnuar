package mx.isban.api.eformats.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.api.eformats.dto.EFormatsError;
import mx.isban.api.eformats.dto.FormatoFavoritoDto;
import mx.isban.api.eformats.dto.OperationResultResponseDto;
import mx.isban.api.eformats.service.FavoritesBusiness;
import mx.isban.api.eformats.utils.Either;

@RestController
@RequestMapping("/favorito")
public class FavoritesController {

	@Autowired
	private FavoritesBusiness favoritesBusiness;

	@GetMapping
	@ResponseBody
	public ResponseEntity<Either<ArrayList<FormatoFavoritoDto>, EFormatsError>> 
			getFormatosFavoritos(@RequestParam(value="idFavorito", required=false, defaultValue="") List<Long> request) {
		return favoritesBusiness.getFormatosFavoritos(request);
	}


	@PostMapping
	@ResponseBody
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			saveFavorito(@RequestBody FormatoFavoritoDto formatoFavoritoDto) {
		return favoritesBusiness.saveFavorito(formatoFavoritoDto);
	}

	@PutMapping
	@ResponseBody
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			updateFormatoFavorito(@RequestBody FormatoFavoritoDto formatoFavoritoDto) {
		return favoritesBusiness.updateFormatoFavorito(formatoFavoritoDto);
	}

	@DeleteMapping
	@ResponseBody
	public ResponseEntity<Either<OperationResultResponseDto, EFormatsError>> 
			deleteFormat(@RequestBody FormatoFavoritoDto formatoFavoritoDto) {
		return favoritesBusiness.deleteFormat(formatoFavoritoDto);
	}
}
