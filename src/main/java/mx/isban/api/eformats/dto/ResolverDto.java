package mx.isban.api.eformats.dto;

import java.io.Serializable;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
public class ResolverDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String claveAplicativo;
	private String usuarioAplicativo;
	//usuarioLocal
	private String identificadorLocal;
	//usuarioCorporativo
	private String identificadorCorporativo;
	private String nombreUsuario;
	private String correo;
	//location
	private String centroCosto;
	//position
	private String idPuesto;
	private String ambiente;
	
	private String descripcionPuesto;
	
	public String getClaveAplicativo() {
		return claveAplicativo;
	}
	public void setClaveAplicativo(String claveAplicativo) {
		this.claveAplicativo = claveAplicativo;
	}
	public String getUsuarioAplicativo() {
		return usuarioAplicativo;
	}
	public void setUsuarioAplicativo(String usuarioAplicativo) {
		this.usuarioAplicativo = usuarioAplicativo;
	}
	public String getIdentificadorLocal() {
		return identificadorLocal;
	}
	public void setIdentificadorLocal(String identificadorLocal) {
		this.identificadorLocal = identificadorLocal;
	}
	public String getIdentificadorCorporativo() {
		return identificadorCorporativo;
	}
	public void setIdentificadorCorporativo(String identificadorCorporativo) {
		this.identificadorCorporativo = identificadorCorporativo;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(String centroCosto) {
		this.centroCosto = centroCosto;
	}
	public String getIdPuesto() {
		return idPuesto;
	}
	public void setIdPuesto(String idPuesto) {
		this.idPuesto = idPuesto;
	}
	public String getAmbiente() {
		return ambiente;
	}
	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}
	public String getDescripcionPuesto() {
		return descripcionPuesto;
	}
	public void setDescripcionPuesto(String descripcionPuesto) {
		this.descripcionPuesto = descripcionPuesto;
	}
}
