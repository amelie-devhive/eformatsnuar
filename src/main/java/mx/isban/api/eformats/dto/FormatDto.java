package mx.isban.api.eformats.dto;

import java.io.Serializable;

public class FormatDto implements Serializable {

	private static final long serialVersionUID = 5738725319193510495L;
	
	private Long formatId;
	private String formatName;
	
	public FormatDto() {
		// TODO Auto-generated constructor stub
	}
	
	public FormatDto(long formatId, String formatName) {
		this.formatId = formatId;
		this.formatName = formatName;
	}
	
	public Long getFormatId() {
		return formatId;
	}
	public void setFormatId(Long formatId) {
		this.formatId = formatId;
	}
	public String getFormatName() {
		return formatName;
	}
	public void setFormatName(String formatDescription) {
		this.formatName = formatDescription;
	}

	@Override
	public String toString() {
		return "FormatDto [formatId=" + formatId + ", formatName=" + formatName + "]";
	}

}
