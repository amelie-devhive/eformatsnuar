package mx.isban.api.eformats.dto;

import java.io.Serializable;

public class OperationResultResponseDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String codigo;
	private String mensaje;

	public OperationResultResponseDto() {
		
	}
	
	public OperationResultResponseDto(String id, String codigo, String mensaje) {
		this.id = id;
		this.codigo = codigo;
		this.mensaje = mensaje;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public String toString() {
		return "OperationResultResponseDto [id=" + id + ", codigo=" + codigo + ", mensaje=" + mensaje + "]";
	}

	
	
}
