package mx.isban.api.eformats.dto;

import java.io.Serializable;

public class CuentaDto implements Serializable{
	private static final long serialVersionUID = 9067324741648155003L;
	
	private String numCuenta;
	private String numCliente;
	private String nombreCliente;
	private String divisa;
	
	public CuentaDto() {
	}
	
	
	public CuentaDto(String numCuenta, String numCliente, String nombreCliente, String divisa) {
		this.numCuenta = numCuenta;
		this.numCliente = numCliente;
		this.nombreCliente = nombreCliente;
		this.divisa = divisa;
	}



	public String getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	public String getNumCliente() {
		return numCliente;
	}

	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getDivisa() {
		return divisa;
	}

	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	@Override
	public String toString() {
		return "CuentaDto [numCuenta=" + numCuenta + ", numCliente=" + numCliente + ", nombreCliente=" + nombreCliente
				+ ", divisa=" + divisa + "]";
	}


	
}
