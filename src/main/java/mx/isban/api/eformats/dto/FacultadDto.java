package mx.isban.api.eformats.dto;

import java.sql.Timestamp;


public class FacultadDto  {
	private Long idFacultad;
	private String claveFac;
	private String dsc;
	private String estatus;
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-ddThh:mm:ss.SSS+ZZZZ")
	private Timestamp fchHorUltimaActualizacion;
	private String numFac;
	private String usrId;

	public FacultadDto() {
		// default constructor
	}

	public long getIdFacultad() {
		return this.idFacultad;
	}

	public void setIdFacultad(long idFacultad) {
		this.idFacultad = idFacultad;
	}

	public String getClaveFac() {
		return this.claveFac;
	}

	public void setClaveFac(String claveFac) {
		this.claveFac = claveFac;
	}

	public String getDsc() {
		return this.dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	public String getEstatus() {
		return this.estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Timestamp getFchHorUltimaActualizacion() {
		if(this.fchHorUltimaActualizacion == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltimaActualizacion.clone();
	}

	public void setFchHorUltimaActualizacion(Timestamp fchHorUltimaActualizacion) {
		if(fchHorUltimaActualizacion == null){
			this.fchHorUltimaActualizacion = null;
		} else {
			this.fchHorUltimaActualizacion = (Timestamp) fchHorUltimaActualizacion.clone();
		}
	}

	public String getNumFac() {
		return this.numFac;
	}

	public void setNumFac(String numFac) {
		this.numFac = numFac;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	@Override
	public String toString() {
		return "FacultadDto [idFacultad=" + idFacultad + ", claveFac=" + claveFac + ", dsc=" + dsc + ", estatus="
				+ estatus + ", fchHorUltimaActualizacion=" + fchHorUltimaActualizacion + ", numFac=" + numFac
				+ ", usrId=" + usrId + "]";
	}
	
}