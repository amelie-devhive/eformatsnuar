package mx.isban.api.eformats.dto;

public class TopFormatosBitacora {

	private long idFormato;
	private int formatoCount;
	private String nombreFormato;
	private String numEjecutivo;
	private String claveFormato;
	
	public TopFormatosBitacora(){
		// default constructor
	}
	
	public TopFormatosBitacora(long idFormato, int formatoCount, 
			String numEjecutivo, String claveFormato, String nombreFormato) {
		this.idFormato = idFormato;
		this.formatoCount = formatoCount;
		this.numEjecutivo = numEjecutivo;
		this.claveFormato = claveFormato;
		this.nombreFormato = nombreFormato;
	}



	public long getIdFormato() {
		return idFormato;
	}


	public void setIdFormato(long idFormato) {
		this.idFormato = idFormato;
	}


	public int getFormatoCount() {
		return formatoCount;
	}


	public void setFormatoCount(int formatoCount) {
		this.formatoCount = formatoCount;
	}


	public String getNombreFormato() {
		return nombreFormato;
	}


	public void setNombreFormato(String nombreFormato) {
		this.nombreFormato = nombreFormato;
	}



	public String getNumEjecutivo() {
		return numEjecutivo;
	}



	public void setNumEjecutivo(String numEjecutivo) {
		this.numEjecutivo = numEjecutivo;
	}



	public String getClaveFormato() {
		return claveFormato;
	}



	public void setClaveFormato(String claveFormato) {
		this.claveFormato = claveFormato;
	}



	@Override
	public String toString() {
		return "TopFormatosBitacora [idFormato=" + idFormato + ", formatoCount=" + formatoCount + ", nombreFormato="
				+ nombreFormato + ", numEjecutivo=" + numEjecutivo + ", claveFormato=" + claveFormato + "]";
	}

	
}
