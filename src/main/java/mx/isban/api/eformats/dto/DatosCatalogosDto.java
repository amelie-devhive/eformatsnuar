package mx.isban.api.eformats.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import com.jsoniter.annotation.JsonProperty;

public class DatosCatalogosDto implements Serializable {
	private static final long serialVersionUID = 6197987909926851485L;
	
	@JsonProperty(from="idDat")
	private long idDato;
	@JsonProperty(from="dscAdic")
	private String dscAdicional;
	@JsonProperty(from="dscEstatDat")
	private String estatus;
	@JsonProperty(from="fchHorUltAct")
	private Timestamp fchHorUltimaActualizacion;
	@JsonProperty(from={"feiMxRelDatCat","idDat"})
	private long feiIdDato;
	@JsonProperty(from= {"feiMxRelCatSi", ""})
	private long idCat;
	@JsonProperty(from={"feiMxRelDatCat","idDat"})
	private long idDatoPadre;
	@JsonProperty(from="dscNomDat")
	private String nombreDato;
	@JsonProperty(from="usrId")
	private String usrId;
	
	public DatosCatalogosDto() {
		// default constructor
	}

	public long getIdDato() {
		return idDato;
	}

	public void setIdDato(long idDato) {
		this.idDato = idDato;
	}

	public String getDscAdicional() {
		return dscAdicional;
	}

	public void setDscAdicional(String dscAdicional) {
		this.dscAdicional = dscAdicional;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Timestamp getFchHorUltimaActualizacion() {
		if(fchHorUltimaActualizacion == null) {
			return null;
		}
		return (Timestamp) fchHorUltimaActualizacion.clone();
	}

	public void setFchHorUltimaActualizacion(Timestamp fchHorUltimaActualizacion) {
		if(fchHorUltimaActualizacion == null) {
			this.fchHorUltimaActualizacion = null;
		}else {
			this.fchHorUltimaActualizacion = (Timestamp) fchHorUltimaActualizacion.clone();
		}
	}

	public long getFeiIdDato() {
		return feiIdDato;
	}

	public void setFeiIdDato(long feiIdDato) {
		this.feiIdDato = feiIdDato;
	}

	public long getIdCat() {
		return idCat;
	}

	public void setIdCat(long idCat) {
		this.idCat = idCat;
	}

	public long getIdDatoPadre() {
		return idDatoPadre;
	}

	public void setIdDatoPadre(long idDatoPadre) {
		this.idDatoPadre = idDatoPadre;
	}

	public String getNombreDato() {
		return nombreDato;
	}

	public void setNombreDato(String nombreDato) {
		this.nombreDato = nombreDato;
	}

	public String getUsrId() {
		return usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	@Override
	public String toString() {
		return "DatosCatalogosDto [idDato=" + idDato + ", dscAdicional=" + dscAdicional + ", estatus=" + estatus
				+ ", fchHorUltimaActualizacion=" + fchHorUltimaActualizacion + ", feiIdDato=" + feiIdDato + ", idCat="
				+ idCat + ", idDatoPadre=" + idDatoPadre + ", nombreDato=" + nombreDato + ", usrId=" + usrId + "]";
	}

	
}
