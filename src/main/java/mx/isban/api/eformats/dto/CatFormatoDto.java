package mx.isban.api.eformats.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;

import com.jsoniter.annotation.JsonProperty;


/**
 * Clase DTO con los campos de la entidad de CatFormato
 */
public class CatFormatoDto implements Serializable {
	private static final long serialVersionUID = -2039945005148900565L;

	private long idFmt;
	@JsonProperty(from={"feiMxRelCatTipoFmt", "idTipoFmt"})
	private long idTipoFormato;
	private String valClaveFmt;
	private String desNomFmt;
	private String dscNomNego;
	private String dscAreaRes;
	private byte[] grlImgFmt;
	private String txtPlbraClave;
	private String usrId;
	private Timestamp fchHorUltAct;

	public CatFormatoDto() {
		//default constructor
	}

	public CatFormatoDto(Long idFormato) {
		this.idFmt = idFormato;
	}

	public long getIdFmt() {
		return this.idFmt;
	}

	public void setIdFmt(long idFmt) {
		this.idFmt = idFmt;
	}

	public String getDesNomFmt() {
		return this.desNomFmt;
	}

	public void setDesNomFmt(String desNomFmt) {
		this.desNomFmt = desNomFmt;
	}

	public String getDscAreaRes() {
		return this.dscAreaRes;
	}

	public void setDscAreaRes(String dscAreaRes) {
		this.dscAreaRes = dscAreaRes;
	}

	public String getDscNomNego() {
		return this.dscNomNego;
	}

	public void setDscNomNego(String dscNomNego) {
		this.dscNomNego = dscNomNego;
	}

	public Timestamp getFchHorUltAct() {
		if(this.fchHorUltAct == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltAct.clone();
	}

	public void setFchHorUltAct(Timestamp fchHorUltAct) {
		if(fchHorUltAct == null){
			this.fchHorUltAct = null;
		} else {
			this.fchHorUltAct = (Timestamp) fchHorUltAct.clone();
		}
	}

	public byte[] getGrlImgFmt() {
		if(grlImgFmt == null) {
			return new byte[] {};
		}
		return this.grlImgFmt.clone();
	}

	public void setGrlImgFmt(byte[] grlImgFmt) {
		if(grlImgFmt == null) {
			this.grlImgFmt = null;
		}else {
			this.grlImgFmt = grlImgFmt.clone();
		}
	}

	public String getTxtPlbraClave() {
		return this.txtPlbraClave;
	}

	public void setTxtPlbraClave(String txtPlbraClave) {
		this.txtPlbraClave = txtPlbraClave;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getValClaveFmt() {
		return this.valClaveFmt;
	}

	public void setValClaveFmt(String valClaveFmt) {
		this.valClaveFmt = valClaveFmt;
	}

	public long getIdTipoFormato() {
		return idTipoFormato;
	}

	public void setIdTipoFormato(long idTipoFormato) {
		this.idTipoFormato = idTipoFormato;
	}

	@Override
	public String toString() {
		return "CatFormatoDto [idFmt=" + idFmt + ", idTipoFormato=" + idTipoFormato + ", valClaveFmt=" + valClaveFmt
				+ ", desNomFmt=" + desNomFmt + ", dscNomNego=" + dscNomNego + ", dscAreaRes=" + dscAreaRes
				+ ", grlImgFmt=" + Arrays.toString(grlImgFmt) + ", txtPlbraClave=" + txtPlbraClave + ", usrId=" + usrId
				+ ", fchHorUltAct=" + fchHorUltAct + "]";
	}

}