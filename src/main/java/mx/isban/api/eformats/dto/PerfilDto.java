package mx.isban.api.eformats.dto;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * DTO para perfil
 * 
 */
public class PerfilDto implements Serializable, Cloneable {
	private static final long serialVersionUID = -3126197211958599548L;
	
	private long idPerfil;
	private long idTipoPerfil;
	private String clavePerfil;
	private String dsc;
	private String estatus;
	private Timestamp fchHorUltimaActualizacion;
	private String usrId;

	public PerfilDto() {
		// default constructor
	}
	
	public long getIdPerfil() {
		return this.idPerfil;
	}

	public void setIdPerfil(long idPerfil) {
		this.idPerfil = idPerfil;
	}

	public String getClavePerfil() {
		return this.clavePerfil;
	}

	public void setClavePerfil(String clavePerfil) {
		this.clavePerfil = clavePerfil;
	}

	public String getDsc() {
		return this.dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	public String getEstatus() {
		return this.estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Timestamp getFchHorUltimaActualizacion() {
		if(this.fchHorUltimaActualizacion == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltimaActualizacion.clone();
	}

	public void setFchHorUltimaActualizacion(Timestamp fchHorUltimaActualizacion) {
		if(fchHorUltimaActualizacion == null){
			this.fchHorUltimaActualizacion = null;
		} else {
			this.fchHorUltimaActualizacion = (Timestamp) fchHorUltimaActualizacion.clone();
		}
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public long getIdTipoPerfil() {
		return idTipoPerfil;
	}

	public void setIdTipoPerfil(long idTipoPerfil) {
		this.idTipoPerfil = idTipoPerfil;
	}

}