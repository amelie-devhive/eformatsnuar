package mx.isban.api.eformats.dto;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
public class WsResolverDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Value("${url.isban.wsdl.resolver}")
	private String urlIsbanWsdlResolver;
	private String claveAplicativo;
	private String usuarioAplicativo;
	private String identificadorLocal;
	private String identificadorCorporativo;
	private Integer servicioSeleccionado;
	
	public String getUrlIsbanWsdlResolver() {
		return urlIsbanWsdlResolver;
	}
	public void setUrlIsbanWsdlResolver(String urlIsbanWsdlResolver) {
		this.urlIsbanWsdlResolver = urlIsbanWsdlResolver;
	}
	public String getClaveAplicativo() {
		return claveAplicativo;
	}
	public void setClaveAplicativo(String claveAplicativo) {
		this.claveAplicativo = claveAplicativo;
	}
	public String getUsuarioAplicativo() {
		return usuarioAplicativo;
	}
	public void setUsuarioAplicativo(String usuarioAplicativo) {
		this.usuarioAplicativo = usuarioAplicativo;
	}
	public String getIdentificadorLocal() {
		return identificadorLocal;
	}
	public void setIdentificadorLocal(String identificadorLocal) {
		this.identificadorLocal = identificadorLocal;
	}
	public String getIdentificadorCorporativo() {
		return identificadorCorporativo;
	}
	public void setIdentificadorCorporativo(String identificadorCorporativo) {
		this.identificadorCorporativo = identificadorCorporativo;
	}
	public Integer getServicioSeleccionado() {
		return servicioSeleccionado;
	}
	public void setServicioSeleccionado(Integer servicioSeleccionado) {
		this.servicioSeleccionado = servicioSeleccionado;
	}
}
