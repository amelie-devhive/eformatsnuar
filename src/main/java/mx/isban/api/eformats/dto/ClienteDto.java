package mx.isban.api.eformats.dto;

import java.io.Serializable;

public class ClienteDto implements Serializable{
	private static final long serialVersionUID = 6777618724786860106L;
	
	private String numCliente;
	private String nombreCliente;
	
	public ClienteDto() {
	}
	
	
	public ClienteDto(String numCliente, String nombreCliente) {
		this.numCliente = numCliente;
		this.nombreCliente = nombreCliente;
	}


	public String getNumCliente() {
		return numCliente;
	}

	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	@Override
	public String toString() {
		return "ClienteDto [numCliente=" + numCliente + ", nombreCliente=" + nombreCliente + "]";
	}


	
}
