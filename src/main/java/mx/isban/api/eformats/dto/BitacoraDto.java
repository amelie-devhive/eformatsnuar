package mx.isban.api.eformats.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

public class BitacoraDto {

	private Long idBitacora;
	private Long idFormato;
	private String estatus;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy HH:mm:ss.S")
	private Timestamp fchHorInicio;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy HH:mm:ss.S")
	private Timestamp fchHorTerminado;
	
	private Timestamp fchHorUltimaActualizacion;
	private String numEjecutivo;
	private String numSucursal;
	private String usrId;

	
	public BitacoraDto() {
		//Default constructor for BitacoraDto
	}


	public Long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(Long idBitacora) {
		this.idBitacora = idBitacora;
	}
	
	public Long getIdFormato() {
		return idFormato;
	}
	public void setIdFormato(Long idFormato) {
		this.idFormato = idFormato;
	}


	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public Timestamp getFchHorInicio() {
		if( this.fchHorInicio != null ){
			return (Timestamp) fchHorInicio.clone();
		}
		return null;
	}

	public void setFchHorInicio(Timestamp fchHorInicio) {
		if( fchHorInicio != null ){
			this.fchHorInicio = (Timestamp) fchHorInicio.clone();
		}else{
			this.fchHorInicio = null;
		}
	}

	public Timestamp getFchHorTerminado() {
		if( this.fchHorTerminado != null ){
			return (Timestamp) fchHorTerminado.clone();
		}
		return null;
	}

	public void setFchHorTerminado(Timestamp fchHorTerminado) {
		if( fchHorTerminado != null ){
			this.fchHorTerminado = (Timestamp) fchHorTerminado.clone();
		}else{
			this.fchHorTerminado = null;
		}
	}


	public Timestamp getFchHorUltimaActualizacion() {
		if(fchHorUltimaActualizacion == null) {
			return null;
		}else {
			return (Timestamp) fchHorUltimaActualizacion.clone();
		}
	}


	public void setFchHorUltimaActualizacion(Timestamp fchHorUltimaActualizacion) {
		if(fchHorUltimaActualizacion == null) {
			this.fchHorUltimaActualizacion = null;
		}else {
			this.fchHorUltimaActualizacion = (Timestamp) fchHorUltimaActualizacion.clone();
		}
	}


	public String getNumEjecutivo() {
		return numEjecutivo;
	}


	public void setNumEjecutivo(String numEjecutivo) {
		this.numEjecutivo = numEjecutivo;
	}


	public String getNumSucursal() {
		return numSucursal;
	}


	public void setNumSucursal(String numSucursal) {
		this.numSucursal = numSucursal;
	}


	public String getUsrId() {
		return usrId;
	}


	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}


	@Override
	public String toString() {
		return "BitacoraDto [idBitacora=" + idBitacora + ", estatus=" + estatus + ", fchHorInicio=" + fchHorInicio
				+ ", fchHorTerminado=" + fchHorTerminado + ", fchHorUltimaActualizacion=" + fchHorUltimaActualizacion
				+ ", numEjecutivo=" + numEjecutivo + ", numSucursal=" + numSucursal + ", usrId=" + usrId + "]";
	}

	
}
