package mx.isban.api.eformats.dto;

import java.io.Serializable;
import java.sql.Timestamp;


public class FormatoFavoritoDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idFavorito;
	private Long idFormato;

	private String usrId;

	private Timestamp fchHorInicio;
	private Timestamp fchHorUltimaActualizacion;
	private String numEjecutivo;

	public FormatoFavoritoDto() {
		// default constructor
	}

	public Long getIdFavorito() {
		return this.idFavorito;
	}

	public void setIdFavorito(Long idFavorito) {
		this.idFavorito = idFavorito;
	}

	public Timestamp getFchHorInicio() {
		if(fchHorInicio == null) {
			return null;
		}else {
			return (Timestamp) fchHorInicio.clone();
		}
	}

	public void setFchHorInicio(Timestamp fchHorInicio) {
		if(fchHorInicio == null) {
			this.fchHorInicio = null;
		}else {
			this.fchHorInicio = (Timestamp) fchHorInicio.clone();
		}
	}

	public Timestamp getFchHorUltimaActualizacion() {
		if(fchHorUltimaActualizacion == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltimaActualizacion.clone();
	}

	public void setFchHorUltimaActualizacion(Timestamp fchHorUltimaActualizacion) {
		if(fchHorUltimaActualizacion == null) {
			this.fchHorUltimaActualizacion = null;
		}else {
			this.fchHorUltimaActualizacion = (Timestamp) fchHorUltimaActualizacion.clone();
		}
	}

	public Long getIdFormato() {
		return this.idFormato;
	}

	public void setIdFormato(Long idFormato) {
		this.idFormato = idFormato;
	}

	public String getNumEjecutivo() {
		return this.numEjecutivo;
	}

	public void setNumEjecutivo(String numEjecutivo) {
		this.numEjecutivo = numEjecutivo;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	@Override
	public String toString() {
		return "FormatoFavoritoDto [idFavorito=" + idFavorito + ", idFormato=" + idFormato + ", fchHorInicio="
				+ fchHorInicio + ", fchHorUltimaActualizacion=" + fchHorUltimaActualizacion + ", numEjecutivo="
				+ numEjecutivo + ", usrId=" + usrId + "]";
	}

	
	
}