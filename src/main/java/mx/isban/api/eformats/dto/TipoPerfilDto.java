package mx.isban.api.eformats.dto;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * DTO para tipo de perfil
 * 
 */
public class TipoPerfilDto implements Serializable{
	private static final long serialVersionUID = -8277229064903645617L;
	
	private long idTipoPerfil;
	private String estatus;
	private Timestamp fchHorUltimaActualizacion;
	private String nombre;
	private String usrId;

	public TipoPerfilDto() {
		// default constructor
	}

	public long getIdTipoPerfil() {
		return this.idTipoPerfil;
	}

	public void setIdTipoPerfil(long idTipoPerfil) {
		this.idTipoPerfil = idTipoPerfil;
	}

	public String getEstatus() {
		return this.estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Timestamp getFchHorUltimaActualizacion() {
		if(this.fchHorUltimaActualizacion == null) {
			return null;
		}
		return (Timestamp) this.fchHorUltimaActualizacion.clone();
	}

	public void setFchHorUltimaActualizacion(Timestamp fchHorUltimaActualizacion) {
		if(fchHorUltimaActualizacion == null){
			this.fchHorUltimaActualizacion = null;
		} else {
			this.fchHorUltimaActualizacion = (Timestamp) fchHorUltimaActualizacion.clone();
		}
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

}