package mx.isban.api.eformats.dto;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class EFormatsError implements Serializable{
	private static final long serialVersionUID = -6602542747531177496L;
	
	private String hashErr;
	private String codigo;
	private String errMsg;
	private HttpStatus httpStatus;
	
	public EFormatsError(String hashErr, String codigo, String errMsg, HttpStatus httpStatus) {
		this.hashErr = hashErr;
		this.codigo = codigo;
		this.errMsg = errMsg;
		this.httpStatus = httpStatus;
	}
	public EFormatsError() {
		
	}
	

	public String getHashErr() {
		return hashErr;
	}
	public String getCodigo() {
		return codigo;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	@Override
	public String toString() {
		return "EFormatsError [hashErr=" + hashErr + ", codigo=" + codigo + ", errMsg=" + errMsg + ", httpStatus="
				+ httpStatus + "]";
	}
}
