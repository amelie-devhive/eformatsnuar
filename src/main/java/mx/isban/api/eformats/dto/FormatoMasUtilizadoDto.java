package mx.isban.api.eformats.dto;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * The persistent class for the FEI_MX_REL_FMT_MAS_UTIL database table.
 * 
 */
public class FormatoMasUtilizadoDto implements Serializable {
	private static final long serialVersionUID = 6118020184347784556L;
	
	private long idMasUtil;
	private long idFormato;
	private String numEjecutivo;
	private long contador;
	private Timestamp fchHorUltimaActualizacion;
	private String usrId;

	public FormatoMasUtilizadoDto() {
		// default constructor
	}

	public long getIdMasUtil() {
		return idMasUtil;
	}

	public void setIdMasUtil(long idMasUtil) {
		this.idMasUtil = idMasUtil;
	}

	public long getIdFormato() {
		return idFormato;
	}

	public void setIdFormato(long idFormato) {
		this.idFormato = idFormato;
	}

	public String getNumEjecutivo() {
		return numEjecutivo;
	}

	public void setNumEjecutivo(String numEjecutivo) {
		this.numEjecutivo = numEjecutivo;
	}

	public long getContador() {
		return contador;
	}

	public void setContador(long contador) {
		this.contador = contador;
	}

	public Timestamp getFchHorUltimaActualizacion() {
		if(fchHorUltimaActualizacion == null) {
			return null;
		}
		return (Timestamp) fchHorUltimaActualizacion.clone();
	}

	public void setFchHorUltimaActualizacion(Timestamp fchHorUltimaActualizacion) {
		if(fchHorUltimaActualizacion == null) {
			this.fchHorUltimaActualizacion = null;
		}else {
			this.fchHorUltimaActualizacion = (Timestamp) fchHorUltimaActualizacion.clone();
		}
	}

	public String getUsrId() {
		return usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	@Override
	public String toString() {
		return "FormatoMasUtilizadoDto [idMasUtil=" + idMasUtil + ", idFormato=" + idFormato + ", numEjecutivo="
				+ numEjecutivo + ", contador=" + contador + ", fchHorUltimaActualizacion=" + fchHorUltimaActualizacion
				+ ", usrId=" + usrId + "]";
	}

	
}