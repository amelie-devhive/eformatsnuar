/**
 * 
 */
package mx.isban.api.eformats.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Amelie
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FormatBusinessTest {

	/**
	 * Test method for {@link mx.isban.api.eformats.service.impl.FormatBusinessImpl#getFormats(java.util.List)}.
	 */
	@Test
	public void testGetFormats() {
		System.out.println("Not yet implemented");
	}

	/**
	 * Test method for {@link mx.isban.api.eformats.service.impl.FormatBusinessImpl#saveFormat(mx.isban.api.eformats.dto.FormatDto)}.
	 */
	@Test
	public void testSaveFormat() {
		System.out.println("Not yet implemented");
	}

	/**
	 * Test method for {@link mx.isban.api.eformats.service.impl.FormatBusinessImpl#updateFormat(mx.isban.api.eformats.dto.FormatDto)}.
	 */
	@Test
	public void testUpdateFormat() {
		System.out.println("Not yet implemented");
	}

	/**
	 * Test method for {@link mx.isban.api.eformats.service.impl.FormatBusinessImpl#deleteFormat(mx.isban.api.eformats.dto.FormatDto)}.
	 */
	@Test
	public void testDeleteFormat() {
		System.out.println("Not yet implemented");
	}

}
