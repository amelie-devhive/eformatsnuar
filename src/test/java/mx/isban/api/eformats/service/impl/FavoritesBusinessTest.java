/**
 * 
 */
package mx.isban.api.eformats.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Amelie
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FavoritesBusinessTest {

	/**
	 * Test method for {@link mx.isban.api.eformats.service.impl.FavoritesBusinessImpl#getFormatosFavoritos(java.util.List)}.
	 */
	@Test
	public void testGetFormatosFavoritos() {
		System.out.println("Not yet implemented");
	}

	/**
	 * Test method for {@link mx.isban.api.eformats.service.impl.FavoritesBusinessImpl#saveFavorito(mx.isban.api.eformats.dto.FormatoFavoritoDto)}.
	 */
	@Test
	public void testSaveFavorito() {
		System.out.println("Not yet implemented");
	}

	/**
	 * Test method for {@link mx.isban.api.eformats.service.impl.FavoritesBusinessImpl#updateFormatoFavorito(mx.isban.api.eformats.dto.FormatoFavoritoDto)}.
	 */
	@Test
	public void testUpdateFormatoFavorito() {
		System.out.println("Not yet implemented");
	}

	/**
	 * Test method for {@link mx.isban.api.eformats.service.impl.FavoritesBusinessImpl#deleteFormat(mx.isban.api.eformats.dto.FormatoFavoritoDto)}.
	 */
	@Test
	public void testDeleteFormat() {
		System.out.println("Not yet implemented");
	}

}
